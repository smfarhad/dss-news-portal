<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{lang?}', 'HomeController@welcome')->name('welcome');
Route::get('/news/{slung}/{lang?}', 'HomeController@news')->name('news');
Route::get('/blog/{lang?}', 'BlogController@blog')->name('blog');
Route::get('/blog/{slung}/{lang?}', 'BlogController@singlePost')->name('blogPost');
Route::get('/tags/{slung}/{lang?}', 'HomeController@tags')->name('tags');
Route::get('/category/{slung}/{lang?}', 'HomeController@category')->name('category');
Route::get('/gallery/{lang?}', 'HomeController@gallery')->name('gallery');
Route::get('/gallery/{slung}/{lang?}', 'HomeController@imageGallery')->name('image-gallery');
Route::get('/video/{lang?}', 'HomeController@videoGallery')->name('video');
Route::get('/appointment/{lang?}', 'HomeController@appointment')->name('appointment');
Route::get('/contact/{lang?}', 'HomeController@contact')->name('contact');
Route::get('/search/{lang?}', 'HomeController@search')->name('search');
Route::get('/blogsearch/{lang?}', 'BlogController@search')->name('bsearch');

Auth::routes(['verify' => true]);


Route::get('/language/{lang?}', 'HomeController@chnageLanguage')->name('language');

// Email
Route::get('mail/send/{lang?}', 'MailController@send')->name('mail/send');

// login 
Route::get('login/{lang?}', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');

//logout 
Route::get('logout/{lang?}', 'Auth\LoginController@logout')->name('logout');
//Route::get('password/email/{lang?}', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');

//Register
Route::get('register/{lang?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->name('register.post');
Route::get('password/reset/{token?}/{lang?}', 'Auth\ResetPasswordController@showResetForm');
Route::get('/clear-cache/{lang?}', function() {
    $exitCode = Artisan::call('cache:clear');
     return 'what you want';
});

//Route::post('login', 'Auth\LoginController@login')->name('login.post');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout.post');
//Route::post('register', 'Auth\RegisterController@register')->name('register.post');
//Route::get('password/email/{lang?}', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::post('password/reset', 'Auth\ForgotPasswordController@reset');
//Route::post('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
 
Route::group(['prefix'=>'admin'], function (){    
        Route::group(['prefix'=>'{lang?}'], function (){       
                Route::get('/', 'Admin\HomeController@index')->name('home');
                Route::get('/profile', 'Admin\HomeController@profile')->name('profile');
                Route::get('/newspublished', 'Admin\NewsController@published')->name('newsPublished');
                Route::get('/rejectednews', 'Admin\NewsController@rejectedNews')->name('newsRejected');
                
                Route::resource('category', 'Admin\CategoryController');
                Route::resource('news', 'Admin\NewsController');
                Route::resource('tags', 'Admin\TagsController');
                Route::resource('blogcategory', 'Admin\BlogCategoryController');
                Route::resource('blogpost', 'Admin\BlogPostController');
                Route::resource('blogtags', 'Admin\BlogTagsController');
                Route::resource('notices', 'Admin\NoticeController');
                Route::resource('division', 'Admin\DivisionController');
                Route::resource('district', 'Admin\DistrictController');
                Route::resource('upozila', 'Admin\UpozilaController');
                Route::resource('module', 'Admin\ModuleController');
                Route::resource('user', 'Admin\UserController');
                Route::resource('usergroup', 'Admin\UserGroupController');
                Route::resource('userpermission', 'Admin\UserPermissionController');
                Route::resource('useractivities', 'Admin\UserActivitiesController');
                Route::resource('designation', 'Admin\DesignationController');
                
                Route::get('/userpermission_bulk_add', 'Admin\UserPermissionController@bulkAdd');
                Route::post('/userpermission_bulk_add', 'Admin\UserPermissionController@bulkAddPost')->name('admin_permission_bulkpost');
                Route::patch('/changePasswordAdmin/{id}', 'Admin\HomeController@changePasswordAdmin')->name('admin_change_pass');
               
                
                Route::get('/appointment', 'Admin\NoticeController@appointment')->name('admin_appointment');
                Route::post('/appointment', 'Admin\NoticeController@appointmentUpdate')->name('admin_appointment');
                
                Route::get('/settings', 'Admin\SettingsController@addBlock')->name('admin_addblock');
                Route::post('/settings', 'Admin\SettingsController@addBlockUpdate')->name('admin_addblock');
                
                Route::get('/aboutus', 'Admin\SettingsController@aboutUs')->name('aboutus');
                Route::post('/aboutus', 'Admin\SettingsController@aboutUsUpdate')->name('aboutus');
                
                Route::get('/newsajax', 'Admin\NewsController@ajaxCreate')->name('admin_ajax');
                Route::post('/newsajaxupload', 'Admin\NewsController@ajaxUpdate')->name('admin_upload');
                
                Route::get('/contact', 'Admin\ContactController@index')->name('admin_contact');
                Route::post('/contact', 'Admin\ContactController@update')->name('admin_contact');
                
                Route::get('/ajaxshow/{id}', 'Admin\NewsController@ajaxShow')->name('admin_news_show');
                
                //crooper js
                Route::get('/crooperjs', 'Admin\HomeController@CrooperJsGet')->name('crooper');
                Route::post('/crooperjs', 'Admin\HomeController@crooperJsPost')->name('crooper');
                
        });
});

Auth::routes(['verify' => true]);