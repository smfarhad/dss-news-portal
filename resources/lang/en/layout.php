<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author farhad
 */
return [
    'menu' => 'News',
    'home' => 'Home',
    'blog' => 'Blog',
    'megamenu' => 'Mega Menu',
    'gallery' => 'Gallery',
    'appointment' => 'Appointment',
    'contact' => 'Contact Us',
    'login' => 'LogIn',
    'featured stories' => 'Featured Stories',
    'latest_news' => 'Latest News',
    'topstoris' => 'Top Storis',
    'read_more'=>'Read more news',
    'amazingarticles' => 'Amazing Article',
    'advertisment' => 'Advertisment',
    'populerarticle' => 'Popular Article',
    'recentlyvideos' => ' Recent Videos ',
    'hotteststoris' => 'Hottest Storis ',
    'mosttalkedaboutstories' => ' Most Talked About Stories',
    'latestphoto' => ' Latest Photo',    
    'copyright' => 'সমাজসেবা অধিদফতর-গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
    'tag-cloud' => 'Tag',
    'notpost' => 'No post found',
    'view-more-articles' =>'View more articles',
    'bangla' =>'বাংলা',
    'english' =>'ইংরেজি',
    'watch-video' =>'Watch video',
    'image-gallery' =>'Photo Gallery',
    'video-gallery' => 'Video Gallery',
    'some-cool' => 'Some cool video',
    'latest_blog'=>'Latest Article',
    'popular_blog'=>'Top Article',
    'search' =>'Search',
    'photographer' =>'Photographer',
    
];
