<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author farhad
 */
return [
    'menu' => 'খবর',
    'home'=>'প্রথম পাতা', 
    'blog'=>'ব্লগ',
    'megamenu'=>'মেগা মেনু',
    'gallery'=>'গ্যাল্যারি',
    'appointment'=>'অ্যাপয়েন্টমেন্ট',
    'contact'=>'যোগাযোগ',
    'login'=>'লগ ইন',
    'featured stories'=>'স্পটলাইট',
    'latest_news' => 'সাম্প্রতিক খবর',
    'topstoris'=>'শীর্ষ খবর',
     'read_more'=>'আরও দেখুন',
    'amazingarticles'=>'সর্বাধিক পঠিত',
    'advertisment'=>'বিজ্ঞাপন',
    'populerarticle'=>'আলোচিত খবর',
    'recentlyvideos'=>'সর্বশেষ ভিডিও ',
    'hotteststoris'=>'জনপ্রিয়  খবর',
    'mosttalkedaboutstories'=>'সর্বাধিক আলোচিত খবর ',
    'latestphoto'=>'সাম্প্রতিক ছবি ',
    'copyright'=>'সমাজসেবা অধিদফতর-গণপ্রজাতন্ত্রী বাংলাদেশ সরকার ',
    'tag-cloud'=>'ট্যাগ',
    'notpost' => 'কোন পোস্ট খুঁজে পাওয়া যায় নি',
    'view-more-articles' =>'আরও  দেখুন',
    'bangla' =>'Bangla',
    'english' =>'English',
    'watch-video' =>'ভিডিও দেখুন',
    'image-gallery' =>'ফটো গ্যালারি',
    'video-gallery' =>'ভিডিও গ্যালারি ',
     'some-cool' => 'কিছু অসাধারণ ভিডিও',
     'latest_blog'=>'সাম্প্রতিক আর্টিকেল',
     'popular_blog'=>'শীর্ষ   আর্টিকেল',
     'search' =>'অনুসন্ধান',
     'photographer' =>'ফটোগ্রাফার',
    
    ];