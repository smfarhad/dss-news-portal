@extends('layouts.master')
@section('content')
<!-- BEGIN #content -->
<main id="content">

    <!-- BEGIN .container -->
    <div class="container">
        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block" style="border-color: #E01B1D;">
                    <h2> @lang('layout.menu')</h2>
                </div>
                <div class="ot-content-block">
                    <div class="ot-content-block ot-article-list-large">
                        @if($main->count()>0)
                        <div class="otg otg-items-2 otg-h-30 otg-v-30">
                            @foreach($main as $row)
                            <div class="otg-item">
                                <div class="item">
                                    <div class="item-header">
                                        <a href="/category/@if( Session::get('lang') == 'bd'){{$row->category_slung_bd}}@else{{$row->category_slung_en}}@endif/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="{{$row->name}}" /></a>
                                    </div>
                                    <div class="item-content">
                                        <div class="item-categories">
                                            <a href="/category/@if( Session::get('lang') == 'bd'){{$row->category_slung_bd}}@else{{$row->category_slung_en}}@endif/{{Session::get('lang')}}" style="color: #E01B1D;"> @if( Session::get('lang') == 'bd') {{$row->category_name_bd}} @else {{$row->category_name_en}} @endif</a>
                                        </div>
                                        <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}"> @if( Session::get('lang') == 'en') {{$row->name_en}} @else {{$row->name}} @endif</a></h3>
                                        <p> @if( Session::get('lang') == 'en') 
                                                     {!! str_limit(strip_tags($row->description_en), 50,'...') !!} 
                                                @else
                                                     {!! str_limit(strip_tags($row->description), 50,'...') !!}
                                                @endif</p>

                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        @else

                        <div class="otg otg-items-1 otg-h-30 otg-v-30">
                            <div class="otg-item">
                                <div class="item">
                                    <div class="item-content">

                                        <h3>@lang('layout.notpost') </h3>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="pagination">
                        {{ $main->links() }}          
                    </div>

                </div>
            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar -->
                @include('sidebar.sidebar-gallery')
                <!-- END .sidebar -->
            </div>
        </div>

        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>

@endsection