@extends('layouts.master')
@section('content')

<!-- BEGIN #content -->
<main id="content">

    <!-- BEGIN .container -->
    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">

                <div class="ot-title-block">
                    <h2>@lang('gallery.tittle') </h2>
                </div>

                <div class="ot-content-block">

                    <div class="photo-block-grid photo-grid-large">
                        <div class="otg otg-items-3 otg-h-30 otg-v-30">

                            @foreach($main as $row)
                            <div class="otg-item">
                                <div class="item">
                                    <a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                        <i class="material-icons">&#xE412;</i>
                                        <img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" />
                                        <p class="text-right text-small"> @if(!empty($row->photographer))@lang('layout.photographer')   :  {{$row->photographer}} @endif</p>
                                    </a>
                                    <div class="item-content">
                                        <h3><a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</a></h3>
                                        <div class="item-meta">                                             
                                            <span class="item-meta-item"><i class="material-icons">&#xE412;</i>@if(json_decode($row->image_gallery)>0) {{count(json_decode($row->image_gallery))}} @else 0  @endif @lang('gallery.photocount')</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endforeach 

                        </div>
                    </div>

                     <div class="pagination">
                         {{ $main->links() }}          
                    </div>
                </div>
            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar -->
             @include('sidebar.sidebar-gallery')
                <!-- END .sidebar -->
           
        </div>
    </div>

    <!-- END .container -->
</div>

<!-- BEGIN #content -->
</main>

@endsection