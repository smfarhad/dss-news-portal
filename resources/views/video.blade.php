@extends('layouts.master')
@section('content')

<!-- BEGIN #content -->
<main id="content">

    <!-- BEGIN .container -->
    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block">
                    <h2>@lang('gallery.videotittle')</h2>
                </div>
                <div class="ot-content-block">
                    <div class="photo-block-grid photo-grid-large">
                        <div class="otg otg-items-3 otg-h-30 otg-v-30">
                            @foreach($main as $row)
                               @if(!empty($row->video))
                            <div class="otg-item">
                                <div class="item">
                                 
                                        <i class="material-icons">&#xE412;</i>
                                        <div class="embed-responsive embed-responsive-16by9">	
                                            {!! $row->video !!} 
                                        </div>
                                   
                                    <div class="item-content">
                                        <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}"> 
                                              @if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif
                                            </a></h3>
                                        <!-- <div class="item-meta">
                                                <span class="item-meta-item"><i class="material-icons">&#xE412;</i> 10 @lang('gallery.photocount')</span>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                                @endif
                            @endforeach 
                        </div>
                    </div>
                    <div class="pagination">
                        {{ $main->links() }}             
                    </div>
                </div>
            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar -->
                @include('sidebar.sidebar-gallery')
                <!-- END .sidebar -->

            </div>
        </div>

        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>

@endsection