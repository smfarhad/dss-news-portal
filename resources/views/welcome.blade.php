@extends('layouts.master')
@section('content')
<div style=" margin: auto;  width: 50%;  padding: 10px;">
<marquee style="" width="100%"   height="30%">
@foreach($marquee as  $row)
<a style="color: #2797d1" href="/news/{{$row->slung}}" target="_blank"> @if(Session::get('lang') == 'bd'){{$row->name}} @else  {{$row->name_en}}@endif</a> |
@endforeach
</marquee> 
</div><!-- BEGIN #content -->

<main id="content">
    <!-- BEGIN .container -->
    <div class="container">
        <div class="otg otg-h-30">
            <div class="otg-item otg-u-2">
                <div class="ot-title-block">
                    <h2>@lang('layout.menu')</h2>
                </div>
                <div class="ot-content-block">
                    <div class="article-front-list">
                        @if(count($news) > 0)
                        @php($i = 0)
                        @foreach ($news as $row)
                        @if ($i == 0)
                        <div class="item">
                            <a href="/news/{{$row->slung}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a> <p class="text-right text-small"> </p>
                            <h3>
                               <a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif</a>
                            </h3>
                        </div>
                        @else
                        <div class="item">
                            <h3>
                                <a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif</a>
                            </h3>
                        </div>
                        @endif
                        @php($i++)
                        @if($i==6)
                        @break
                        @endif
                        @endforeach
                        @php($i=0)
                        @endif

                    </div>

                    <a href="/category/{{Session::get('lang')}}" class="more-stories-button"> @lang('layout.read_more') </a>

                </div>

                <div class="ot-title-block">
                    <h2> @lang('layout.latest_news') </h2>
                </div>

                <div class="ot-content-block">
                    <div class="article-front-list">
                        @if(count($news) > 0)
                        @php($i = 0)
                        @foreach ($news as $row)
                        @if ($i == 0)
                        <div class="item">
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a>
                           
                        </div>
                        @else
                        <div class="item">
                            <h3>
                                <a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif </a>
                            </h3>
                        </div>
                        @endif
                        @php($i++)
                        @if($i==5)
                        @break
                        @endif
                        @endforeach
                        @php($i=0)
                        @endif

                    </div>

                    <a href="/" class="more-stories-button"> @lang('layout.read_more') </a>

                </div>

            </div>
            <div class="otg-item otg-u-4">

                <div class="ot-title-block">
                    <h2>@lang('layout.featured stories')</h2>
                </div>
                <div class="ot-content-block">
                    <div class="article-front-grid">
                        @php ($i = 0)
                        @if (count($news) > 0)
                        @foreach ($news as $row)
                        @if ($i == 0)
                        <div class="item item-overlay-blur" data-blur-strength="30"> 
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header"><img src="{{'/storage/images/news/'.$row->image}}" alt="" /></a><p class="text-right text-small"> </p>
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                <strong> @if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif</strong>
                            </a>
                        </div>
                        @else
                        <div class="item item-overlay-blur">
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a>
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                <strong>  @if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif<p class="text-right text-small"> </p></strong>
                                <canvas></canvas>
                            </a>
                        </div>
                        @endif
                        @php($i++)
                        @if($i==9)
                        @break
                        @endif
                        @endforeach
                        @endif  
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- 
      à¦¸à¦°à§�à¦¬à¦¾à¦§à¦¿à¦• à¦ªà¦ à¦¿à¦¤ 
    -->
    <div class="container-lightstrip" style="background-color: #f0f0f0;">
        <div class="container">
            <div class="otg otg-h-30">
                <div class="otg-item otg-u-6">
                    <div class="ot-title-block text-me-center">
                        <h2><a href="#">@lang('layout.amazingarticles')</a> </h2>
                    </div>
                    <div class="ot-content-block hd-article-block-grid">
                        <div class="otg otg-items-4 otg-h-30 otg-v-30">
                            @if (count($news) > 0)
                            @php($i=0)
                            @foreach ($news as $row)
                            @if ($row->parent_id == 0)
                                <div class="otg-item">
                                    <div class="item">
                                        <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                           <img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /><p class="text-right text-small"> </p>
                                        </a>
                                        <div class="item-content">
                                            <div class="item-categories">
                                               <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}">{{$row->parent_name}}</a>
                                            </div>
                                            <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en')  {{ $row->name_en }} @else  {{ $row->name }} @endif</a></h3>
                                            <div class="item-meta">
                                                <span class="item-meta-item"><i class="material-icons">&#xE192;</i> &nbsp; {{ date('F j, Y', strtotime($row->created_at)) }} </span>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif 
                            @php($i++)
                            @if($i==4)
                            @break
                            @endif  
                            @endforeach
                            @endif  
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <!--<hr>-->
        <div class="otg otg-items-4 otg-h-30">
            @if (count($categories) > 0)
            @php($i=0)
            @php($j=0)
            @php($k=0)
            @php($l=0)
            @php($limit = 5)
            @foreach ($categories as $row)
            @if ($row->parent_id == 0)
            <div class="otg-item">
                <!--<div class="ot-title-block" style="border-color: #DE4242;">-->
                <div class="ot-title-block">
                    <h2>@if(Session::get('lang') == 'en'){{$row->name_en}} @else {{$row->name_bd}} @endif</h2>
                </div>
                <div class="ot-content-block">
                    <div class="article-front-list">
                        @foreach ($news as $news_row)
                            @if ($row->id == $news_row->parent_category )
                                @if($row->id == 1 && $i <= $limit)   
                                    <div class="item">
                                    @if($i == 0)
                                        <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$news_row->image}}" alt="1" /></a>
                                        <h3>
                                          <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif   </a>
                                        </h3>
                                    @else 
                                    <h3>
                                        <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif </a>
                                    </h3>
                                    @endif
                                    @php($i++)
                                    </div> 
                                @endif
            
                                @if($row->id == 2 && $j <= $limit)      
                                    <div class="item">
                                    @if($j == 0)
                                       <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$news_row->image}}" alt="2" /></a><p class="text-right text-small"> @if(!empty($news_row->photographer))@lang('layout.photographer')   :  {{$news_row->photographer}} @endif</p>
                                        <h3>
                                            <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif</a>
                                        </h3>
                                    @else 
                                        <h3>
                                            <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif</a>
                                        </h3>
                                    @endif 
                                    @php($j++) 
                                    </div> 
                                @endif
                                @if($row->id == 3 && $k <= $limit)   
                                    <div class="item">
                                        @if($k == 0)
                                            <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$news_row->image}}" alt="3" /></a><p class="text-right text-small"> @if(!empty($news_row->photographer))@lang('layout.photographer')   :  {{$news_row->photographer}} @endif</p>
                                            <h3>
                                               <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif</a>
                                            </h3>
                                        @else 
                                            <h3>
                                                <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $news_row->name_en }} @else  {{ $news_row->name }} @endif </a>
                                            </h3>
                                        @endif
                                        @php($k++) 
                                    </div> 
                                @endif
                                @if($row->id == 4 && $l <= $limit)   
                                <div class="item">
                                    @if($l == 0)
                                        <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$news_row->image}}" alt="4" /></a><p class="text-right text-small"> @if(!empty($news_row->photographer))@lang('layout.photographer')   :  {{$news_row->photographer}} @endif</p>
                                        <h3>
                                            <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$news_row->name_en}} @else  {{$news_row->name}} @endif </a>
                                        </h3>
                                    @else 
                                        <h3>
                                            <a href="/news/{{$news_row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$news_row->name_en}} @else  {{$news_row->name}} @endif </a>
                                        </h3>
                                    @endif
                                    @php($l++)
                                </div>
                                @endif
                            @endif                         
                        @endforeach                         
                    </div>
                    <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}" class="more-stories-button"> @lang('layout.read_more') </a>
                </div>
            </div>
            @endif 
            @endforeach
            @endif  
        </div>
        <hr>

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">

                <div class="ot-title-block">
                    <h2> @lang('layout.recentlyvideos')</h2>
                </div>

                <div class="ot-content-block video-block-grid">
                    <div class="otg otg-items-3 otg-h-30 otg-v-30">
                        @php($i=0)
                        @foreach ($news as $row)
                                @if(!empty($row->video))
                            <div class="otg-item">
                                <div class="item">
                                        {!!$row->video!!}
                                    <div class="item-content">
                                        <div class="item-categories">
                                            <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}"> {{$row->parent_name}}</a>
                                        </div>
                                        <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif </a></h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($i==8)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                     
                    </div>
                </div>

            </div>
            <div class="otg-item otg-u-2">

                <div class="ot-title-block">
                    <h2> @lang('layout.latestphoto')</h2>
                </div>

                <div class="ot-content-block photo-block-grid">
                    <div class="otg otg-items-2 otg-h-30 otg-v-30">
                        @php($i=0)
                        @foreach($news as $row)
                        <div class="otg-item">
                            <div class="item">
                                <a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                    <i class="material-icons">&#xE412;</i>
                                    <img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /><p class="text-right text-small"> </p>
                                </a>
                                <div class="item-content">
                                    <h3><a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{$row->name_en}} @else  {{$row->name}} @endif  </a></h3>
                                    <div class="item-meta">
                                        <span class="item-meta-item"><i class="material-icons">&#xE412;</i> @if(json_decode($row->image_gallery)>0){{count(json_decode($row->image_gallery))}}  @else  0 @endif  photos</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($i==5)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                      
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="container-darkstrip" style="background-color: #232323;">
        <img class="container-darkstrip-bg" src="/assets/sitetheme/images/photos/image-21.jpg" alt="" /><p class="text-right text-small"> </p>
        <div class="container">
            <div class="otg otg-h-30">
                <div class="otg-item otg-u-6">
                        <div class="ot-title-block text-me-center">
                            <h2>@lang('layout.amazingarticles')</h2>
                        </div>

                    <div class="ot-content-block hd-article-block-grid">
                        <div class="otg otg-items-3 otg-h-30 otg-v-30">
                            @php($i=0)
                            @foreach($news as $row)
                            <div class="otg-item">
                                <div class="item">
                                    <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                        <img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" />
                                    </a><p class="text-right text-small"> </p>
                                    <div class="item-content">
                                        <div class="item-categories">
                                            <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}">{{$row->parent_name}}</a>
                                        </div>
                                        <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $row->name_en }} @else  {{ $row->name }} @endif </a></h3>
                                    </div>
                                </div>
                            </div>
                            @if($i==2)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block">
                    <h2>@lang('layout.topstoris')</h2>
                </div>
                <div class="ot-content-block ot-article-list-large">
                    <div class="otg otg-items-2 otg-h-30 otg-v-30">
                        @php($i=0)
                        @foreach($news as $row)
                        <div class="otg-item">
                            <div class="item">
                                <div class="item-header">
                                    <a href="/news/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a><p class="text-right text-small"> </p>
                                </div>
                                <div class="item-content">
                                    <div class="item-categories">
                                        <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}" style="color: #E01B1D;">{{$row->parent_name}}</a>
                                    </div>
                                    <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $row->name_en }} @else  {{ $row->name }} @endif </a></h3>
                                    <p>
                                        @if(Session::get('lang') == 'en') {{ substr(strip_tags($row->description_en), 0, 100)  }} . . .  @else {{ substr(strip_tags($row->description), 0, 100)  }} . . .  @endif
                                        
                                    </p>
                                </div>
                            </div>
                        </div>
                        @if($i==5)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                        
                    </div>
                </div>

            </div>
            <div class="otg-item otg-u-2">

                <div class="ot-title-block">
                    <h2>  @lang('layout.populerarticle')</h2>
                </div>

                <div class="ot-content-block">
                    <div class="ot-article-list-small">
                        @php($i=0)
                        @foreach($news as $row)
                        <div class="item">
                            <div class="item-header">
                                <a href="/news/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a><p class="text-right text-small"> </p>
                            </div>
                            <div class="item-content">
                                <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $row->name_en }} @else  {{ $row->name }} @endif </a></h3>
                                <div class="item-meta">
                                    <a href="blog.html"><i class="material-icons">access_time</i> 3 hours ago</a>
                                </div>
                            </div>
                        </div>
                        @if($i==5)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                        

                    </div>
                </div>

                <div class="ot-title-block">
                    <h2>@lang('layout.hotteststoris')</h2>
                </div>

                <div class="ot-content-block">
                    <div class="ot-article-list-small">
                        @php($i=0)
                        @foreach($news as $row)
                        <div class="item">
                            <div class="item-header">
                                <a href="/news/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="" /></a><p class="text-right text-small"> </p>
                            </div>
                            <div class="item-content">
                                <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $row->name_en }} @else  {{ $row->name }} @endif </a></h3>
                                {{--<div class="item-meta">
                                    <a href="#"><i class="material-icons">access_time</i> 3 hours ago</a>
                                </div>--}}
                            </div>
                        </div>
                        @if($i==4)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                        
                        
                    </div>
                </div>

            </div>
        </div>

        <hr>

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-6">

                <div class="ot-title-block text-me-center">
                    <h2> @lang('layout.mosttalkedaboutstories')   </h2>
                </div>

                <div class="ot-content-block ot-article-block-list">
                    <div class="otg otg-items-4 otg-h-30 otg-v-30">
                       @php($i=0)
                        @foreach($news as $row)
                        <div class="otg-item">
                            <div class="item">
                                <h3>
                                    <a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'en') {{ $row->name_en }} @else  {{ $row->name }} @endif</a>
                                </h3>
                                {{-- <div class="item-meta">
                                    <a href="#"><i class="material-icons">access_time</i> 3 hours ago</a>
                                    <a href="/news/{{$row->slung}}/{{Session::get('lang')}}#comments"><i class="material-icons">chat_bubble_outline</i> 30</a>
                                </div> --}}
                            </div>
                        </div>
                        @if($i==15)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
               
                    </div>
                </div>
            </div>
        </div>
        <!-- END .container -->
    </div>
    <!-- BEGIN #content -->
</main>
@endsection