<aside class="sidebar">
    <!-- BEGIN .widget -->

    <div class="widget">
        <form class="search-form"  action="{{route('bsearch',Session::get('lang'))}}" method="get">
            <label>
                <span class="screen-reader-text">Search for:</span>
                <input type="search" class="search-field" placeholder="Search …" value="" name="search" title="Search for:">
            </label>
            <input type="submit" class="search-submit screen-reader-text" value="@lang('layout.search')">
        </form>
        <!-- END .widget -->
    </div>
 
    <!-- BEGIN .widget -->
    <div class="widget">
        <h3>@lang('layout.popular_blog')</h3>
        <div class="widget-content ot-w-article-list">
            @php($i=0)
            @foreach($popularBlogPost as $row)
            <div class="item">
                <div class="item-header">
                    <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/blog_thumb/'.$row->image}}" alt="" /></a>
                </div>
                <div class="item-content">
                    <h4><a href="/blog/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</a></h4>
                    <span class="item-meta">
                        <span class="item-meta-item"><i class="material-icons">access_time</i>{{ date('F j, Y', strtotime($row->created_at)) }}</span>
                    </span>
                </div>
            </div>
            @if($i==2)
            @break;
            @endif
            @php($i++)
            @endforeach 

        </div>
        <!-- END .widget -->
    </div>

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3><span>@lang('layout.tag-cloud')</span></h3>

        <div class="tagcloud">
            @foreach($blogtags as $row)
            @if(Session::get('lang') =='en')
            <a href="/tags/{{$row->slung}}/{{Session::get('lang')}}">{{$row->name_en}}</a>
            @else
             <a href="/tags/{{$row->slung}}/{{Session::get('lang')}}">{{$row->name}}</a>
            @endif
            @endforeach
        </div>

        <!-- END .widget -->
    </div>

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3><span> @lang('layout.latest_blog')</span></h3>
        <div class="ot-widget-article-list">
            @php($i=0)
            @foreach($latestPost as $row)
                    <div class="item">
                        <div class="item-header">
                            <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/blog_thumb/'.$row->image}}" alt="" /></a>
                        </div>
                        <div class="item-content">
                            <h4><a href="/blog/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</a></h4>
                        </div>
                    </div>
            @if($i==3)
            @break;
            @endif
            @php($i++)
            @endforeach 	

        </div>
        <a href="/category/{{Session::get('lang')}}" class="ot-widget-button">@lang('layout.view-more-articles')</a>
        <!-- END .widget -->
    </div>
    <!-- END .widget -->
</div>
</aside>