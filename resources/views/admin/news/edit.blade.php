@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ucfirst( $title)}}
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
            <li><a href="#">সম্পাদনা </a></li>
            <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা দেখুন
                                    </button>
                                </a>
                                <a href="{{route($title.'.create','bd')}}" class="btn  btn-warning btn-flat" type="button"><i class="fa fa-fw fa-plus"></i>  নতুন {{$title_bd}} যোগ করুন  </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                        <form  class='form-horizontal' method="POST" action="{{ route($title.'.update',['lang'=>'bd', 'id'=>$show->id]) }}" enctype="multipart/form-data">
                            <input name="_method" type="hidden" value="PATCH">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">বাংলায় শিরোনাম</label>
                                    <div class="col-sm-7">
                                        <input name="name" value="{{ $show->name }}" class="form-control" id="name" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name_en" class="col-sm-2 control-label">ইংরেজিতে শিরোনাম</label>
                                    <div class="col-sm-7">
                                        <input name="name_en" value="{{ $show->name_en }}" class="form-control" id="name_en" placeholder="" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="imageUpload" class="col-sm-2 control-label">চিত্র আপলোড</label>
                                    <div class="col-sm-5">
                                        <input  value="{{ $show->image }}"  type="hidden" name="image_upload_edit" id="imageUploadEn">
                                        <input type="file" name="feature_image_upload" id="imageUpload">
                                        <p class="help-block">Please upload an image here</p>
                                    </div>
                                    <div class="col-sm-5">
                                        <p id="image_p" class="help-block"><img src="{{'/storage/images/news_thumb/'.$show->image}}" alt="{{$show->name}}" style="width:200px;"></p>    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="galleryImageUpload" class="col-sm-2 control-label"> গ্যালারি চিত্র আপলোড </label>
                                    <div class="col-sm-5">
                                        <input  value="{{ $show->image_gallery }}"  type="hidden" name="gallery_image_upload_edit" id="imageUploadEn">
                                        <input type="file" name="gallery_image_upload[]" id="galleryImageUpload"  class="galleryImageUpload">
                                        <p class="help-block">Please upload an image here</p>
                                    </div>
                                    <div class="col-sm-5">
                                        <p id="gallery_p" class="gallery_p help-block"></p>
                                    </div>
                                    <div class="col-sm-5"></div>
                                    
                                </div>
                                 
                                <div id="multiImagediv"></div>
                                <div id="gallarayUpload" class="form-group">
                                    <div class="col-sm-2"></div><div class="col-sm-3">Add More Image to the Gallery<button class="btn btn-primary btn-flat addMoreGallary pull-right" type="button"> <i class="fa fa-plus" aria-hidden="true"></i></button></div>
                                </div>
                                
                                <div class="col-sm-7"></div>  
                                <div class="col-sm-5">
                                        <p id="image_p" class="help-block">
                                            @if(json_decode($show->image_gallery)>0)    
                                            @foreach(json_decode($show->image_gallery) as $image)
                                            <img src="{{'/storage/images/news_thumb/'.$image}}" alt="{{$show->name}}" style="width:60px;">
                                            @endforeach
                                            @endif
                                        </p> 
                                </div>
                                <div class="form-group">
                                    <label for="photographer" class="col-sm-2 control-label">ফটোগ্রাফার</label>
                                    <div class="col-sm-5">
                                        <input name="photographer" value="{{$show->photographer}}" class="form-control" id="photographer" placeholder="" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="video" class="col-sm-2 control-label"> ভিডিও আপলোড </label>
                                    <div class="col-sm-5">
                                        <textarea name="video" id="video" class="form-control" rows="4" placeholder="Enter ...">{{ $show->video }}</textarea>
                                        <p class="help-block">Please paste only youtube embaded video here</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="video" class="col-sm-2 control-label">  </label>

                                    <div class="col-md-5 col-sm-5">
                                        <p style="width: 200px;">{!! $show->video !!}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="category" class="col-sm-2 control-label"> শ্রেণী  </label>
                                    <div class="col-sm-5">
                                        <select name="category" id="category" class="form-control select2m" required>
                                            <option value="">নির্বাচন করুন....</option>
                                            @if (count($main) > 0)
                                            @foreach($main as $row)
                                            @if ($row->parent_id == 0)
                                            <option value="{{$row->id}}" @if($row->id == $show->category) selected @endif>{{$row->name_bd}}</option>
                                            @foreach($main as $sub_row)
                                            @if ($sub_row->parent_id == $row->id)
                                            <option value="{{$sub_row->id}}" @if($sub_row->id == $show->category) selected @endif>- {{$sub_row->name_bd}}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                            @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="featured" class="col-sm-2 control-label">আলোচিত সংবাদ </label>
                                    <div class="col-sm-5">
                                        <label class="radio-inline"><input type="radio" name="featured" value="1" @if($show->featured  == 1) checked @endif>হাঁ</label>
                                        <label class="radio-inline"><input type="radio" name="featured" value="0"  @if($show->featured  != 1) checked @endif>না</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="coolVideo" class="col-sm-2 control-label">অসাধারণ ভিডিও </label>
                                    <div class="col-sm-5">
                                        <label class="radio-inline"><input type="radio" name="coolVideo" value="1" @if($show->cool_video  == 1) checked @endif>হাঁ</label>
                                        <label class="radio-inline"><input type="radio" name="coolVideo" value="0" @if($show->cool_video  != 1) checked @endif>না</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tags" class="col-sm-2 control-label"> ট্যাগ  </label>
                                    <div class="col-sm-5">
                                        <select name="tags" id="tags" class="form-control select2m" required>
                                            <option value="">নির্বাচন করুন....</option>
                                            @if (count($tags) > 0)
                                            @foreach($tags as $row)
                                            <option value="{{$row->id}}" @if($show->tag == $row->id) selected @endif>{{$row->name}}</option>
                                            @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">বাংলায় বিস্তারিত </label>
                                    <div class="col-sm-10">
                                        <div id="descriptionp"></div>
                                        <textarea name="description" id="description" class="textarea">{{$show->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description_en" class="col-sm-2 control-label">ইংরেজিতে বিস্তারিত </label>
                                    <div class="col-sm-10">
                                        <div id="description_enp"></div>
                                        <textarea name="description_en" id="description_en" class="textarea">{{$show->description_en}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group" @if (Auth::user()->group_id == 3) style="display:none" @endif>
                                     <label for="tags" class="col-sm-2 control-label"> স্ট্যাটাস </label>
                                    <div class="col-sm-5">
                                        <select name="status" id="status" class="form-control select2m" required>
                                            <option value="">নির্বাচন করুন....</option>
                                            @if (count($status) > 0)
                                            @foreach($status as $row)
                                            <option value="{{$row->id}}" @if($show->status == $row->id) selected @endif>{{$row->name_alt}}</option>
                                            @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                @if(Auth::user()->group_id == 3)
                                <input id="status" type="hidden" name="status" value="1">

                                <button id="draft" data-status="1" class="btn btn-default btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    ড্রাফট হিসেবে  কার্যকর করুন
                                </button>
                                @endif
                                <button id="save" data-status="2" class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
