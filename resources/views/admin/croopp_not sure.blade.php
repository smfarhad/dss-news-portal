@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$title_bd}}
            <small>যোগ করুন</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
            <li><a href="#">নতুন  যোগ </a></li>
            <li class="active">{{$title_bd}}</li>
        </ol>
    </section>
            <div class="image-wrapper" id="image-4-wrapper">
                <img id="image-2" src="/storage/images/croop/testo.jpg">
                <form>
                    <div>
                        <label for="width">width:</label> 
                        <input id="width" type="text">
                    </div>
                    <div>
                        <label for="height">height:</label>  
                        <input id="height" type="text">
                    </div>
                    <div>
                        <label for="x">x:</label>  
                        <input id="x" type="text">
                    </div>
                    <div>
                        <label for="y">y:</label>  
                        <input id="y" type="text">
                    </div>
                    <input id="cancel" type="reset" value="cancel">
                    <input id="update" type="button" value="Apply">
                </form>
            </div>        
@endsection