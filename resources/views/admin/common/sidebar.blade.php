<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{'/storage/images/profilethumb/'.Auth::user()->profile_picture}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p> {{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">প্রধান ন্যাভিগেশন</li>
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'CategoryController' && $row->read_access == 1 )
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>বার্তা ক্যাটাগরি</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('category.index','bd')}}"><i class="fa fa-circle-o"></i>  তালিকা </a></li>
                    <li><a href="{{route('category.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'NewsController' && $row->read_access == 1 )
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>খবর</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('news.index','bd')}}"><i class="fa fa-circle-o"></i>  অপ্রকাশিত   খবর তালিকা </a></li>
                     <li><a href="{{route('newsPublished','bd')}}"><i class="fa fa-circle-o"></i> প্রকাশিত খবর তালিকা </a></li>
                      <li><a href="{{route('newsRejected','bd')}}"><i class="fa fa-circle-o"></i> বাতিল খবর তালিকা </a></li>
                    <li><a href="{{route('news.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'TagsController' && $row->read_access == 1 )
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>ট্যাগ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('tags.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('tags.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>

                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'NoticeController' && $row->read_access == 1 )        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>  
                    <span>এপয়েন্টমেন্ট</span>

                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin_appointment','bd')}}"><i class="fa fa-circle-o"></i> পৃষ্ঠা বিবরণ </a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span>বিজ্ঞপ্তি</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('notices.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                            <li><a href="{{route('notices.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin_appointment','bd')}}"><i class="fa fa-circle-o"></i> পৃষ্ঠা বিবরণ </a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span>বিজ্ঞপ্তি</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('notices.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                            <li><a href="{{route('notices.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif
            @endforeach
            
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'ContactController' && $row->read_access == 1 )        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>  
                    <span> আমাদের সম্পর্কে </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('aboutus','bd')}}"><i class="fa fa-circle-o"></i> পৃষ্ঠা বিবরণ </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'ContactController' && $row->read_access == 1 )        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i>  
                    <span> যোগাযোগ </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin_contact','bd')}}"><i class="fa fa-circle-o"></i> পৃষ্ঠা বিবরণ </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'BlogCategoryController' && $row->read_access == 1 )        
            <li class="header"> ব্লগ বিভাগ </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>ব্লগ শ্রেণী</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('blogcategory.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('blogcategory.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>

                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'BlogTagsController' && $row->read_access == 1 )        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>ব্লগ ট্যাগ </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('blogtags.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('blogtags.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'BlogPostController' && $row->read_access == 1 )     
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>ব্লগ পোস্ট </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('blogpost.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('blogpost.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
             <li class="header"> <i class="fa fa-gear"></i> সেটিংস  </li>
             @foreach(session()->get('permission') as $row)
             @if($row->module_name == 'SettingsController' && $row->read_access == 1 )    
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> বিজ্ঞাপন সেটিংস </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin_addblock','bd')}}"><i class="fa fa-circle-o"></i>  সম্পাদনা করুন </a></li>
                </ul>
             </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'NotModuleController' && $row->read_access == 1 )    
            <li class="header"> <i class="fa fa-gear"></i> সেটিংস  </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> মডিউল </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('module.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('module.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'UserController' && $row->read_access == 1 )    
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> ইউজার  </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('user.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('user.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
                @foreach(session()->get('permission') as $row)
                @if($row->module_name == 'DivisionController' && $row->read_access == 1 )    
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span> বিভাগ  </span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('division.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                        <li><a href="{{route('division.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                    </ul>
                </li>
                @endif
                @endforeach
                @foreach(session()->get('permission') as $row)
                @if($row->module_name == 'DivisionController' && $row->read_access == 1 )    
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span> জেলা  </span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('district.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                        <li><a href="{{route('district.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                    </ul>
                </li>
                @endif
                @endforeach
                 @foreach(session()->get('permission') as $row)
                @if($row->module_name == 'DivisionController' && $row->read_access == 1 )    
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span> উপজেলা  </span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('upozila.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                        <li><a href="{{route('upozila.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                    </ul>
                </li>
                @endif
                @endforeach
                @foreach(session()->get('permission') as $row)
                @if($row->module_name == 'DesignationController' && $row->read_access == 1 )    
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span> জব টাইটেল  </span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('designation.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                        <li><a href="{{route('designation.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                    </ul>
                </li>
                @endif
                @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'UserGroupController' && $row->read_access == 1 )    
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> ইউজার  গ্রুপ  </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('usergroup.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('usergroup.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            
             @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'UserGroupController' && $row->read_access == 2 )    
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> ইউসার একটিভিটিস </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('useractivities.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('useractivities.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
            @foreach(session()->get('permission') as $row)
            @if($row->module_name == 'UserPermissionController' && $row->read_access == 1 )    
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span> ইউসার পারমিশন </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('userpermission.index','bd')}}"><i class="fa fa-circle-o"></i> তালিকা </a></li>
                    <li><a href="{{route('userpermission.create','bd')}}"><i class="fa fa-circle-o"></i> নতুন যোগ করুন </a></li>
                </ul>
            </li>
            @endif
            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
