@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ucfirst( $title)}}
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
                 <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
                <li><a href="#">সম্পাদনা </a></li>
                <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6 text-left text-bold">
                                <h3>Add Settings</h3>
                            </div>
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                             <form  class='form-horizontal' method="POST" action="{{ route('admin_addblock','bd') }}" enctype="multipart/form-data">
                                
                                 @csrf
                            <div class="box-body">
                                        <div class="radio">
                                            <label>
                                              <input type="radio" name="addblock" id="optionsRadios1" value="1" @if($show->phone ==1) checked @endif>
                                             এমবেডেড বিজ্ঞাপন কোড
                                            </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="addblock" id="optionsRadios2" value="2"  @if($show->phone ==2) checked @endif>
                                           বিজ্ঞাপন ইমেজ ও লিংক
                                          </label>
                                        </div>
                               
                                <div  @if($show->phone !=1) style="display: none;" @endif   id="embedCode" class="form-group">  
                                    <label for="name" class="col-sm-2 control-label"> এমবেডেড বিজ্ঞাপন কোড</label>
                                    <div class="col-sm-5">
                                        <textarea rows="5" name="embedCode" class="form-control" id="name_bd" placeholder="" type="text" required>{{$show->name}}</textarea>
                                    </div>
                                </div>
                                
                                <div @if($show->phone !=2) style="display: none;" @endif id="imageAdd">  
                                    <div  id="embedImage"  class="form-group">
                                        <label for="fileInput" class="col-sm-2 control-label"> বিজ্ঞাপন ইমেজ</label>
                                        <div class="col-sm-3">
                                            <input  name="adsImage_edit" id="fileInput" value="{{$show->name_en}}" type="hidden">
                                            <input  name="adsImage" id="fileInput" type="file">
                                        </div>
                                        <div class="col-sm-5">
                                            <p class="help-block"><img src="{{$add_img_thumb.$show->name_en}}" alt="add Image Not Found"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="link" class="col-sm-2 control-label">  বিজ্ঞাপন লিংক</label>
                                        <div class="col-sm-5">
                                            <input value="{{$show->email}}" name="link" class="form-control" id="link" type="text" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                       কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
