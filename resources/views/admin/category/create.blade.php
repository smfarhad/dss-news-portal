@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$title_bd}}
            <small>যোগ করুন</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
            <li><a href="#">নতুন  যোগ </a></li>
            <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6 text-right text-bold">
                                <h3>নতুন প্রধান ক্যাটাগরি</h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা  দেখুন
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form id="tryitForm" role="form" data-toggle="validator"  class='form-horizontal' method="post" action="{{ route($title.'.store',['lang'=>'bd']) }}">
                                 @csrf
                            <div class="box-body">
                                <input name="parent_id" value="0" class="form-control" id="parent_id" placeholder="" type="hidden" required>                
                                <div class="form-group">
                                    <label for="nameBd" class="col-sm-5 control-label">বাংলা নাম</label>
                                    <div class="col-sm-7">
                                        <input name="name_bd" value="{{ old('name_bd') }}" class="form-control" id="nameBd" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name_en" class="col-sm-5 control-label">ইংরেজি নাম</label>
                                    <div class="col-sm-7">
                                        <input name="name_en" value="{{ old('name_en') }}" class="form-control" id="name_en" placeholder="" type="text" required>
                                    </div>
                                </div>
  
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6 text-right text-bold">
                                <h3>নতুন  সাব  ক্যাটাগরি</h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা  দেখুন
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form id="tryitForm" role="form" data-toggle="validator"  class='form-horizontal' method="post" action="{{ route($title.'.store',['lang'=>'bd']) }}">
                                 @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="head" class="col-sm-5 control-label">প্রধান {{$title_bd}} নাম </label>
                                    <div class="col-sm-7">
                                        <select name="parent_id" class="form-control select2m">
                                            <option value="">নির্বাচন করুন....</option>
                                            @if (count($main) > 0)
                                            @foreach($main as $row)
                                            <option value="{{$row->id}}">{{$row->name_bd}}</option>
                                            @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nameBd" class="col-sm-5 control-label">বাংলা নাম</label>
                                    <div class="col-sm-7">
                                        <input name="name_bd" value="{{ old('name_bd') }}" class="form-control" id="nameBd" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name_en" class="col-sm-5 control-label">ইংরেজি নাম</label>
                                    <div class="col-sm-7">
                                        <input name="name_en" value="{{ old('name_en') }}" class="form-control" id="name_en" placeholder="" type="text" required>
                                    </div>
                                </div>
  
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
