<!DOCTYPE html>
<html>
    <head>
        <title>Responsive Crop. Advanced demos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <link href="/assets/admintheme/bower_components/rcrooper/dist/rcrop.min.css" media="screen" rel="stylesheet" type="text/css">
        <style>
            body{margin: 0; padding: 0px}
            main{
                min-height:500px;
                display: block
            }
            pre{
                overflow: auto;
            }
            .demo{
                padding: 20px;
            }
            .image-wrapper{
                max-width: 600px;
                min-width: 200px;
            }
            img{
                max-width: 100%;
            }

            #image-4-wrapper .rcrop-outer-wrapper{
                opacity: .75;
            }
            #image-4-wrapper .rcrop-outer{
                background: #000
            }
            #image-4-wrapper .rcrop-croparea-inner{
                border: 1px dashed #fff;
            }

            #image-4-wrapper .rcrop-handler-corner{
                width:12px;
                height: 12px;
                background: none;
                border : 0 solid #51aeff;
            }
            #image-4-wrapper .rcrop-handler-top-left{
                border-top-width: 4px;
                border-left-width: 4px;
                top:-2px;
                left:-2px
            }
            #image-4-wrapper .rcrop-handler-top-right{
                border-top-width: 4px;
                border-right-width: 4px;
                top:-2px;
                right:-2px
            }
            #image-4-wrapper .rcrop-handler-bottom-right{
                border-bottom-width: 4px;
                border-right-width: 4px;
                bottom:-2px;
                right:-2px
            }
            #image-4-wrapper .rcrop-handler-bottom-left{
                border-bottom-width: 4px;
                border-left-width: 4px;
                bottom:-2px;
                left:-2px
            }
            #image-4-wrapper .rcrop-handler-border{
                display: none;
            }

            #image-4-wrapper .clayfy-touch-device.clayfy-handler{
                background: none;
                border : 0 solid #51aeff;
                border-bottom-width: 6px;
                border-right-width: 6px;
            }

            label{
                display: inline-block;
                width: 60px;
                margin-top: 10px;
            }
            #update{
                margin: 10px 0 0 60px ;
                padding: 10px 20px;
            }

            #cropped-original, #cropped-resized{
                padding: 20px;
                border: 4px solid #ddd;
                min-height: 60px;
                margin-top: 20px;
            }
            #cropped-original img, #cropped-resized img{
                margin: 5px;
            }

            #image-2-wrapper .rcrop-outer-wrapper{
                opacity: .75;
            }
            #image-2wrapper .rcrop-outer{
                background: #000
            }
            #image-2-wrapper .rcrop-croparea-inner{
                border: 1px dashed #fff;
            }

            #image-2-wrapper .rcrop-handler-corner{
                width:12px;
                height: 12px;
                background: none;
                border : 0 solid #51aeff;
            }
            #image-2-wrapper .rcrop-handler-top-left{
                border-top-width: 4px;
                border-left-width: 4px;
                top:-2px;
                left:-2px
            }
            #image-2-wrapper .rcrop-handler-top-right{
                border-top-width: 4px;
                border-right-width: 4px;
                top:-2px;
                right:-2px
            }
            #image-2-wrapper .rcrop-handler-bottom-right{
                border-bottom-width: 4px;
                border-right-width: 4px;
                bottom:-2px;
                right:-2px
            }
            #image-2-wrapper .rcrop-handler-bottom-left{
                border-bottom-width: 4px;
                border-left-width: 4px;
                bottom:-2px;
                left:-2px
            }
            #image-2-wrapper .rcrop-handler-border{
                display: none;
            }

            #image-2-wrapper .clayfy-touch-device.clayfy-handler{
                background: none;
                border : 0 solid #51aeff;
                border-bottom-width: 6px;
                border-right-width: 6px;
            }
        </style>
    </head>
    <body>
        <main>
            <div class="image-wrapper" id="image-4-wrapper">
                <img id="image-2" src="/storage/images/croop/testo.jpg">
                <form>
                    <div>
                        <label for="width">width:</label> 
                        <input id="width" type="text">
                    </div>
                    <div>
                        <label for="height">height:</label>  
                        <input id="height" type="text">
                    </div>
                    <div>
                        <label for="x">x:</label>  
                        <input id="x" type="text">
                    </div>
                    <div>
                        <label for="y">y:</label>  
                        <input id="y" type="text">
                    </div>
                    <input id="cancel" type="reset" value="cancel">
                    <input id="update" type="button" value="Apply">
                </form>
            </div>
        </main>
        <script src="/assets/admintheme/bower_components/rcrooper/libs/jquery.js" ></script>
        <script src="/assets/admintheme/bower_components/rcrooper/dist/rcrop.min.js" ></script>
        <script>
            $(document).ready(function () {
                var $image2 = $('#image-2'),
                        $update = $('#update'),
                        inputs = {
                            x: $('#x'),
                            y: $('#y'),
                            width: $('#width'),
                            height: $('#height')
                        },
                        fill = function () {
                            var values = $image2.rcrop('getValues');
                            for (var coord in inputs) {
                                inputs[coord].val(values[coord]);
                            }
                        }
                $image2.rcrop();
                $image2.on('rcrop-changed rcrop-ready', fill);
                $update.click(function () {
                    $image2.rcrop('resize', inputs.width.val(), inputs.height.val(), inputs.x.val(), inputs.y.val());
                    fill();
                })
            });
        </script>
    </body>
</html>