@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$title_bd}}
            <small>যোগ করুন</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
            <li><a href="#">নতুন  যোগ </a></li>
            <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা  দেখুন
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                        <form id="tryitForm" role="form" data-toggle="validator"  class='form-horizontal' method="post" action="{{ route($title.'.store',['lang'=>'bd']) }}" enctype="multipart/form-data">
                                 @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> শিরোনাম</label>
                                    <div class="col-sm-5">
                                        <input name="name" value="{{ old('name') }}" class="form-control" id="name" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fileUpload" class="col-sm-2 control-label">চিত্র আপলোড</label>
                                    <div class="col-sm-5">
                                        <input type="file" name="file_upload" id="fileUpload">
                                        <p class="help-block">Please upload an image here</p>
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> প্রকাশের তারিখ </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input data-date-format="yyyy-mm-dd" name="published" value="{{ old('name') }}" class="form-control pull-right" id="datepicker" placeholder="" type="text" required>
                                          </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                     কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
