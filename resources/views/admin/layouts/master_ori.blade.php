<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> {{$title_bd}} | বার্তা-  @if(Auth::user()->group_id == 1) এডমিন @elseif(Auth::user()->group_id == 2) প্রকাশক @else সম্পাদক @endif</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/assets/admintheme/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/assets/admintheme/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/assets/admintheme/bower_components/Ionicons/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="/assets/admintheme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- wysihtml5 editor -->
        <link rel="stylesheet" href="/assets/admintheme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="/assets/admintheme/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/assets/admintheme/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/assets/admintheme/admin.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/assets/admintheme/dist/css/skins/_all-skins.min.css">
        <link href="/assets/admintheme/bower_components/rcrooper/dist/rcrop.min.css" media="screen" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        @if(Session::get('lang') == 'bd')
        <style>
            *{font-family: SolaimanLipi; }
        </style>
        @endif
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('admin.common.header')
            <!-- Left side column. contains the logo and sidebar -->
            @include('admin.common.sidebar')
            <!-- Content Wrapper. Contains page content -->
            @yield('content')
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    ©কারিগরি সহযোগিতায়ঃ <a href="http://www.4beats.net/">4বিটস লিমিটেড</a>
                </div>
                <strong>

                    <p><a href="http://dss.gov.bd/" target="_blank">সমাজসেবা অধিদফতর |</a>
                        <a href="http://www.msw.gov.bd/" target="_blank">সমাজকল্যাণ মন্ত্রণালয়</a></p>

                </strong>
            </footer>


        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="/assets/admintheme/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/assets/admintheme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="/assets/admintheme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/assets/admintheme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="/assets/admintheme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="/assets/admintheme/bower_components/fastclick/lib/fastclick.js"></script>

        <!-- wysihtml5 editor -->
        <script src="/assets/admintheme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="/assets/admintheme/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- bootstrap datepicker -->
        <script src="/assets/admintheme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/assets/admintheme/bower_components/ckeditor/ckeditor.js"></script>
        <!-- AdminLTE App -->
        <script src="/assets/admintheme/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="/assets/admintheme/dist/js/demo.js"></script>
        <!-- responsive croop -->
        <script src="/assets/admintheme/bower_components/rcrooper/dist/rcrop.min.js" ></script>
        <!-- Site script -->
        <script src="/assets/admintheme/dist/js/admin.js"></script>
        <script>
        $(document).ready(function () {
  
                $('#imageUpload').on('change', function(){                  
                        var $image2 = $('#image-2'),
                        $update = $('#update'),
                        inputs = {  x: $('#x'),
                                        y: $('#y'),
                                        width: $('#width'),
                                        height: $('#height')
                                     },
                fill = function () {
                        var values = $image2.rcrop('getValues');
                        for (var coord in inputs) {
                            inputs[coord].val(values[coord]);
                        }
                }
                $image2.rcrop();
                $image2.on('rcrop-changed rcrop-ready', fill);
                $update.click(function () {
                $image2.rcrop('resize', inputs.width.val(), inputs.height.val(), inputs.x.val(), inputs.y.val());
                fill();

                })
                    
                        $('#image-2').show();
                                var preview = document.querySelector('#image-2');
                                var file    = document.querySelector('input[type=file]').files[0];
                                //var file    = document.querySelector('#imageUpload').files[0];
                                var reader  = new FileReader();

                                reader.onloadend = function () {
                                        preview.src = reader.result;
                                }
                                
                                if (file) {
                                        reader.readAsDataURL(file);
                                } else {
                                        preview.src = "fff";
                                }
                });
        });
        </script>

        <!-- page script -->
        <script>
            $(function () {

                 setTimeout( function(){ 
                     var stylesheet = document.createElement('link');
                            stylesheet.href = '/assets/admintheme/dist/css/testtest.css';
                            stylesheet.rel = 'stylesheet';
                            stylesheet.type = 'text/css';
                            document.getElementsByTagName('head')[0].appendChild(stylesheet);
                 }, 3000 );

                
                $('#example2').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': true,
                    'ordering': true,
                    "order": [[1, "desc"]],
                    'info': true,
                    'autoWidth': true
                });

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                })
            })
        </script>
    </body>
</html>
