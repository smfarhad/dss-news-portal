@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ucfirst( $title)}}
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
                 <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
                <li><a href="#">সম্পাদনা </a></li>
                <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা দেখুন
                                    </button>
                                </a>
                                <a href="{{route($title.'.create','bd')}}" class="btn  btn-warning btn-flat" type="button"><i class="fa fa-fw fa-plus"></i>  নতুন {{$title_bd}} যোগ করুন  </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                             <form  class='form-horizontal' method="POST" action="{{ route($title.'.update',['lang'=>'bd', 'id'=>$show->id]) }}">
                                <input name="_method" type="hidden" value="PATCH">
                                 @csrf
                            <div class="box-body">
                
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> প্রোফাইল ছবি  </label>
                                    <div class="col-sm-9">
                                        <img src="{{'/storage/images/profilethumb/'.$show->profile_picture}}" style="height: 100px;"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="imageUpload" class="col-sm-2 control-label" > পরিবর্তন করুন চিত্র পরিবর্তন করুন</label>
                                    <div class="col-sm-5">
                                        <input  value="{{ $show->profile_picture }}"  type="hidden" name="image_upload_edit" id="imageUploadEn">
                                        <input type="file" name="feature_image_upload" id="imageUpload">
                                        <p class="help-block">Please upload an image here</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> নাম  </label>
                                    <div class="col-sm-7">
                                        <input name="name" value="{{ Auth::user()->name }}" class="form-control" id="name" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">ই-মেইল</label>
                                    <div class="col-sm-7">
                                        <input name="email" value="{{ Auth::user()->email }}" class="form-control" id="email" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mobile" class="col-sm-2 control-label">মোবাইল</label>
                                    <div class="col-sm-7">
                                        <input name="mobile" value="{{ Auth::user()->mobile }}" class="form-control" id="mobile" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="office" class="col-sm-2 control-label"> অফিস </label>
                                    <div class="col-sm-7">
                                        <select name="office" id="office" class="form-control select2m" required>
                                             <option value="">নির্বাচন করুন....</option>
                                                @if (count($office) > 0)
                                                  @foreach($office as $row)
                                                       <option value="{{$row->id}}" @if($show->office == $row->id) selected @endif>{{$row->name_bd}}</option>
                                                  @endforeach
                                                @endif
                                        </select> 
                                    </div>
                                </div>    
                              
                                <div class="form-group">
                                    <label for="job_title" class="col-sm-2 control-label">  জব টাইটেল </label>
                                    <div class="col-sm-7">
                                        <select name="job_title" id="office" class="form-control select2m" required>
                                             <option value="">নির্বাচন করুন....</option>
                                                @if (count($designation) > 0)
                                                  @foreach($designation as $row)
                                                       <option value="{{$row->id}}" @if($show->job_title == $row->id) selected @endif>{{$row->name}}</option>
                                                  @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="division" class="col-sm-2 control-label">  বিভাগ </label>
                                        <div class="col-sm-7">
                                            <select name="division" id="division" class="form-control select2m" required>
                                             <option value="">নির্বাচন করুন....</option>
                                                @if (count($division) > 0)
                                                  @foreach($division as $row)
                                                       <option value="{{$row->id}}" @if($show->division == $row->id) selected @endif>{{$row->name_bd}}</option>
                                                  @endforeach
                                                @endif
                                        </select>
                                        </div>
                                    </div>
                                    
                                       <div class="form-group">
                                        <label for="district" class="col-sm-2 control-label">  জেলা </label>
                                        <div class="col-sm-7">
                                            <select name="district" id="district" class="form-control select2m" required>
                                             <option value="">নির্বাচন করুন....</option>
                                                @if (count($district) > 0)
                                                  @foreach($district as $row)
                                                       <option value="{{$row->id}}" @if($show->district == $row->id) selected @endif>{{$row->name_bd}}</option>
                                                  @endforeach
                                                @endif
                                        </select>
                                        </div>
                                </div>
                                 <div class="form-group">
                                        <label for="thana" class="col-sm-2 control-label">  থানা </label>
                                        <div class="col-sm-7">
                                            <select name="thana" id="thana" class="form-control select2m" required>
                                             <option value="">নির্বাচন করুন....</option>
                                                @if (count($thana) > 0)
                                                  @foreach($thana as $row)
                                                       <option value="{{$row->id}}" @if($show->thana == $row->id) selected @endif>{{$row->name_bd}}</option>
                                                  @endforeach
                                                @endif
                                        </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="user-group" class="col-sm-2 control-label"> অবস্থা </label>
                                    <div class="col-sm-5">
                                       <select name="status" id="groupID" class="form-control select2m">
                                            <option value="">নির্বাচন করুন....</option>
                                               <option @if($show->status ==1) selected @endif  value="1"> Active</option>         
                                               <option @if($show->status ==2) selected @endif  value="2">In Active</option>         
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user-group" class="col-sm-2 control-label"> ব্যবহারকারীর ধরন</label>
                                    <div class="col-sm-5">
                                       <select name="groupID" id="groupID" class="form-control select2m">
                                            <option value="">নির্বাচন করুন....</option>
                                                @if (count($group) > 0)
                                                @foreach($group as $row)
                                                        <option @if($row->id == $show->group_id) selected @endif  value="{{$row->id}}">{{$row->name}}</option>                   
                                                @endforeach
                                                @endif
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                       কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                
                                <h3 class="box-title">
                                     পাসওয়ার্ড পরিবর্তন করুন 
                                </h3>
                            </div>
                            <div class="col-md-6 text-left text-bold">
                              <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                             <form  class='form-horizontal' method="POST" action="{{ route('admin_change_pass',['lang'=>'bd', 'id'=>$show->id]) }}">                                                                                      
                                <input name="_method" type="hidden" value="PATCH">
                                 @csrf
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="password" class="col-sm-3 control-label">পাসওয়ার্ড </label>
                                        <div class="col-sm-5">
                                            <input name="password" value="" class="form-control" id="password" placeholder="পাসওয়ার্ড" type="password" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation" class="col-sm-3 control-label">পাসওয়ার্ড নিশ্চিত করুন </label>
                                        <div class="col-sm-5">
                                            <input name="password_confirmation" value="" class="form-control" id="cpassword" placeholder="পাসওয়ার্ড নিশ্চিত করুন" type="password" required>
                                        </div>
                                    </div>
                                </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                       কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
