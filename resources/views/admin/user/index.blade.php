@extends('admin.layouts.master')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{title_case($title_bd)}} তালিকা
        <small> </small>
      </h1>
      <ol class="breadcrumb">
                <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
                <li><a href="#"> তালিকা  </a></li>
                <li class="active">{{$title_bd}}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
       
          <div class="box  box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8"><h3 class="box-title"> {!!Session::get('success')!!}</h3></div>
                    <div class="col-md-4 text-right"> 
                        <a href="{{route($title.'.create','bd')}}" class="btn  btn-primary btn-flat" type="button"><i class="fa fa-fw fa-plus"></i> নতুন {{$title_bd}} যোগ করুন  </a>
                    </div>
                </div>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  {!! $th !!}
                </tr>
                </thead>
                <tbody>
                    @php($i =1)
                    @foreach($main as $row)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$row->name}} </td>
                        <td>{{$row->email}}</td>
                        <td>{{$row->group_name}} </td>
                        <td>@if($row->status ==1)  Active @else In Active @endif  </td>
                        <td>
                                @if($permission['read']==1)<a href="{{route($title.'.show',['lang'=>'bd', $title=>$row->id])}}" class="btn  btn-primary btn-xs" type="button" title="View Details" data-toggle="tooltip" data-placement="top"><i class="fa fa-fw fa-eye"></i></a>@endif
                                @if($permission['edit']==1)<a href="{{route($title.'.edit',['lang'=>'bd', $title=>$row->id])}}" class="btn  btn-success btn-xs" type="button"  title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-fw fa-pencil"></i></a>@endif
                                @if($permission['delete']==1)<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-danger-{{$row->id}}" data-toggle="tooltip" data-placement="top" title="Delete">@endif
                                    <i class="fa fa-fw fa-trash"></i>
                                  </button>
                                <div class="modal modal-danger fade" id="modal-danger-{{$row->id}}">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                       আপনি কি এটি মুছে ফেলতে চান?              
                                                        </h4>
                                        </div>
                                        
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">না</button>
                                                
                                                <form  action="{{ route($title.'.destroy', ['lang'=>'bd', $title=>$row->id ])}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-outline" type="submit"><i class="fa fa-fw fa-trash"></i> হাঁ</button>
                                                </form>
                                        </div>
                                      </div>
                                      <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                  </div>
                      </td>
                      
                    </tr>
                    @php($i++)
                    @endforeach
                
                </tbody>
                <tfoot>
                <tr>
                     {!! $th !!}
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection