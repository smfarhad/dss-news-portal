@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ucfirst( $title)}}
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
                 <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
                <li><a href="#">সম্পাদনা </a></li>
                <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা দেখুন
                                    </button>
                                </a>
                                <a href="{{route($title.'.create','bd')}}" class="btn  btn-warning btn-flat" type="button"><i class="fa fa-fw fa-plus"></i>  নতুন {{$title_bd}} যোগ করুন  </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                             <form  class='form-horizontal' method="POST" action="{{ route($title.'.update',['lang'=>'bd', 'id'=>$show->id]) }}">
                                <input name="_method" type="hidden" value="PATCH">
                                 @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="user-group" class="col-sm-3 control-label"> ব্যবহারকারীর ধরন</label>
                                    <div class="col-sm-5">
                                       <select name="userGroup" id="bangla_post_id" class="form-control select2m">
                                            <option value="">নির্বাচন করুন....</option>
                                                @if (count($userGroup) > 0)
                                                @foreach($userGroup as $row)
                                                
                                                        <option @if ($row->id == $show->group_id) selected  @endif   value="{{$row->id}}">{{$row->name}}</option>
                                                      
                                                @endforeach
                                                @endif
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user-group" class="col-sm-3 control-label"> মডিউল </label>
                                    <div class="col-sm-5">
                                       <select name="module" id="bangla_post_id" class="form-control select2m">
                                            <option value="">নির্বাচন করুন....</option>
                                                @if (count($module) > 0)
                                                @foreach($module as $row)
                                                    
                                                        <option @if ($row->id == $show->module_id) selected  @endif   value="{{$row->id}}">{{$row->name}}</option>
                                                                           
                                                @endforeach
                                                @endif
                                        </select> 
                                    </div>
                                </div>
                               <div class="form-group">
                                    <label for="user-group" class="col-sm-3 control-label"> মডিউল </label>
                                    <div class="col-sm-7">
                                         <label style="heigh:50px; margin-left:50px;" for="read" class=" control-label">Read</label>
                                        <label><input name="read" type="checkbox" value=1 class="minimal" @if ($show->read_access == 1) checked  @endif ></label>
                                        
                                        <label style="heigh:50px; margin-left:40px;" for="write" value="1" class=" control-label">Write</label>
                                        <label><input name="write" type="checkbox" value=1 class="minimal" @if ($show->write_access == 1) checked  @endif></label>
                                        
                                        <label style="heigh:50px; margin-left:50px;" for="edit" class=" control-label">Edit</label>
                                        <label><input name="edit" type="checkbox" value=1 class="minimal" @if ($show->edit_access == 1) checked  @endif></label>
                                        
                                        <label style="heigh:50px; margin-left:40px;" for="delete" class=" control-label">Delete</label>
                                        <label><input name="delete" type="checkbox" value=1 class="minimal" @if ($show->delete_access == 1) checked  @endif></label>
                                    </div>
                                </div>
                  
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                       কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
