@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title_bd }} <small> বিস্তারিত </small>
        </h1>
        <ol class="breadcrumb">
                <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
                <li><a href="#">বিস্তারিত  </a></li>
                <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">{!!Session::get('success')!!}</h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                                <a href="{{ route($title.'.index',['lang'=>'bd']) }}">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা দেখুন
                                    </button>
                                </a> &nbsp;
                                <a class="text-bold" href="{{ route($title.'.edit',['lang'=>'bd', 'category'=>$show->id] ) }}">
                                    <button class="btn btn-warning btn-flat" type="button">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> সম্পাদন করুন
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                        <div class="col-md-12">
                                            <label for="name" class="col-sm-3 control-label"> মডিউল </label><div class="col-sm-5"> {{ $show->module_name }} </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="name" class="col-sm-3 control-label"> ইউজার দল </label><div class="col-sm-5"> {{ $show->group_name }} </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="name" class="col-sm-3 control-label"> দেখার পারমিশন </label><div class="col-sm-5"> @if($show->read_access ==1)   হাঁ  @else  না @endif  </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="name" class="col-sm-3 control-label">লেখার পারমিশন </label><div class="col-sm-5"> @if($show->write_access ==1)   হাঁ  @else  না @endif </div>
                                        </div>
                                        <div class="col-md-12">
                                                    <label for="name" class="col-sm-3 control-label">পরিবর্তন পারমিশন </label><div class="col-sm-5">@if($show->edit_access ==1)   হাঁ  @else  না @endif  </div>
                                                </div>
                                        <div class="col-md-12">
                                                <label for="name" class="col-sm-3 control-label">মুছে ফেলার পারমিশন </label><div class="col-sm-5">@if($show->delete_access ==1)   হাঁ  @else  না @endif  </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>

@endsection
