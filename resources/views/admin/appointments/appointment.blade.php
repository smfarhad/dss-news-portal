@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$title_bd}}
            <small>যোগ করুন</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> ড্যাশবোর্ড</a></li>
            <li><a href="#">নতুন  যোগ </a></li>
            <li class="active">{{$title_bd}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="box-title">
                                    @if($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    {!!Session::get('success')!!}
                                </h3>
                            </div>
                            <div class="col-md-6 text-right text-bold">
                               <!-- <a href=" ">
                                    <button class="btn btn-primary btn-flat" type="button">
                                        <i class="fa fa-eye" aria-hidden="true"></i> তালিকা  দেখুন
                                    </button>
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- /.box-header -->
                        <!-- form start -->

                        <form id="tryitForm" role="form" data-toggle="validator"  class='form-horizontal' method="post" action="{{ route('admin_appointment',['lang'=>'bd']) }}" enctype="multipart/form-data">
                                 @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> শিরোনাম বাংলা </label>
                                    <div class="col-sm-10">
                                        <input name="name" value="{{ $show->name }}" class="form-control" id="name" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"> শিরোনাম ইংরেজি </label>
                                    <div class="col-sm-10">
                                        <input name="name_en" value="{{ $show->name_en }}" class="form-control" id="name" placeholder="" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="description" class="col-sm-2 control-label">বিস্তারিত  বাংলা </label>
                                        <div class="col-sm-10">
                                                <textarea name="description" id="description" class="textarea" 
                                                            accesskey=""
                                                            placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $show->description }}</textarea>
                                        </div>
                                </div>
                                
                                
                                <div class="form-group">
                                        <label for="description" class="col-sm-2 control-label">বিস্তারিত  ইংরেজি</label>
                                        <div class="col-sm-10">
                                                <textarea name="description_en" id="description" class="textarea" 
                                                            accesskey=""placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $show->description_en }}</textarea>
                                        </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-right">
                                <button class="btn btn-primary btn-flat" type="submit">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    কার্যকর করুন
                                </button>
                            </div>
                            <!-- /.box-footer -->
                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
