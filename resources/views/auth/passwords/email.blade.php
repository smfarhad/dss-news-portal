@extends('auth.layout')
@section('content')
       @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
    <form method="POST" action="{{ route('password.email') }}">
    @csrf
      <div class="form-group has-feedback">
          <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  placeholder="Email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
      </div>
      
                <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <button type="submit" class="pull-right btn btn-primary btn-block btn-flat">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>         
                </div>         
        <!-- /.col -->
      
    </form>
    <a class="btn btn-link" href="{{ route('register',['lang'=>'bd']) }}">
                                      Register a new membership 
                                </a>
    <a class="btn btn-link" href="{{ route('login',['lang'=>'bd']) }}">
                                   Login
                                </a>
@endsection