@extends('auth.layout')

@section('content')
                <div class="card-header">{{ __('Reset Password') }}</div>

                        <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        
                        <div class="form-group has-feedback">
                                <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  placeholder="Email" value="{{ old('email') }}" required autofocus>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback text-red" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                                <div class="form-group has-feedback">
                                     <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                                     <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                 @if ($errors->has('password'))
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('password') }}</strong>
                                         </span>
                                     @endif
                                 </div>
                                
                                <div class="form-group has-feedback">
                                 <input id="password-confirm" name="password_confirmation" type="password" class="form-control" placeholder="Retype Password" required>
                                   <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                               </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
@endsection
