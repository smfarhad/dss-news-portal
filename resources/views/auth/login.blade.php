@extends('auth.layout')
@section('content')
<form action="{{ route('login')}}" method="post">
    @csrf
    <div class="form-group has-feedback">
        <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  placeholder="Email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
        <span class="invalid-feedback text-red" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
        <span class="invalid-feedback text-red" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="form-check checkbox icheck">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                       <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>  
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
    </div>
</form>
<!--
<a class="btn btn-link" href="{{ route('register',Session::get('lang')) }}">
    Register a new membership 
</a>
<a class="btn btn-link" href="{{ route('password.request') }}">
    {{ __('Forgot Your Password?') }}
</a>

-->
@endsection