@extends('layouts.master')
@section('content')
<!-- BEGIN #content -->
<main id="content">

    <!-- BEGIN .container -->
    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-6">

                <div class="ot-title-block">
                    <h2>@if(Session::get('lang') =='en'){{$page->name_en}} @else{{$page->name}}@endif </h2>
                </div>

                <div class="ot-content-block">
                    <div>
                        @if(Session::get('lang') =='en'){{$page->description_en}} @else {!!$page->description!!}@endif
                        <p style="margin-bottom: 50px;"></p>
                    </div>

                        <table style="width:100%">
                          <tr>
                            <th>ক্রমিক</th>
                            <th>শিরোনাম</th> 
                            <th> প্রকাশের তারিখ </th>
                            <th> ডাউনলোড </th>
                          </tr>
                          @foreach($notices as $row)
                            <tr>
                              <td>{{$row->id}}</td>
                              <td>{{$row->title}} </td> 
                              <td> {{$row->published }}</td>
                              <td>
                                  
                                      <a href="{{Storage::url('images/file_upload/'.$row->download)}}" class="btn btn-social btn-success">
                                        <button class="btn"><i class="fa fa-download"></i> Download</button>
                                      </a>
                                
                              </td>
                            </tr>
                          @endforeach
                        </table> 


                </div>

            </div>

        </div>

        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>
@endsection		