@extends('layouts.master')
@section('content')
<main id="content">
    <!-- BEGIN .container -->
    <div class="container">
        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block">
                    <h2> @lang('layout.menu') </h2> 
                </div>
                <div class="ot-content-block">
                    <div itemscope itemtype="http://schema.org/Blog">
                        <div class="article-head">
                            <div class="articl-heade-media">
                                <img itemprop="image" src="{{'/storage/images/news/'.$details->image}}" alt="" /><p class="text-right text-small"> @if(!empty($details->photographer))@lang('layout.photographer')   :  {{$details->photographer}} @endif</p>
                            </div>  
                            <h1 itemprop="headline">@if( Session::get('lang') == 'en') {{$details->name_en}} @else {{$details->name}} @endif</h1>
                            <meta itemprop="datePublished" content="2016-04-01" />
                            <meta itemprop="dateModified" content="2016-04-01" />
                            <div class="article-head-meta">
                                <span class="meta-item text-small"><i class="material-icons">access_time</i>{{ date('F j, Y', strtotime($details->created_at)) }}  |
                                  News Reporter :  {{ $details->created_by_name }} 
                                @if( Session::get('lang') == 'en')
                                | Job Title : {!! $details->designation_name_en !!} 
                                @else 
                                 |  Job Title :  {{ $details->designation_name_bd }} 
                                @endif
                               @if( Session::get('lang') == 'en')
                                 |  Office :  {{ $details->office_name_en }} 
                                @else 
                                   |   Office :  {{ $details->office_name_bd }} 
                                @endif
                               </span>
                               
                                
                            </div>
                        </div>
                        <div class="shortcode-content" itemprop="mainEntityOfPage">
                                @if( Session::get('lang') == 'en') {!!$details->description_en!!} @else {!!$details->description!!} @endif
                        </div> 

                        <div class="article-foot-tags">
                            <strong><i class="material-icons">share</i>Share article</strong>
                            <div class="article-share-bttns">
                                <div class="fb-share-button" data-href="{{url()->full()}}" data-layout="box_count" data-size="large" data-mobile-iframe="true">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fhttp%2Fbeatsltd-001-site1.1tempurl.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
     
                <div class="ot-content-block">
                    <div class="fb-comments" data-href="{{url()->full()}}" data-numposts="10"></div>
                </div>

            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar -->
                @include('sidebar.sidebar-gallery')
                <!-- END .sidebar -->
            </div>
        </div>
        <!-- END .container -->
    </div>
    <!-- BEGIN #content -->
</main>
@endsection