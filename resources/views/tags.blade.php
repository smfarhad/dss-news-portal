@extends('layouts.master')
@section('content')
<!-- BEGIN #content -->
<main id="content">
    <!-- BEGIN .container -->
    <div class="container">
        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block">
                    <h2> @if(Session::get('lang') =='en'){{$title}} @else {{$title_bd}}@endif </h2>
                </div>
                <div class="ot-content-block">
                    <div class="article-front-grid">
                        @if(count($main) > 0)
                        @php($i = 0)
                        @foreach($main as $row)
                        @if($i == 0)
                        <div class="item item-overlay-blur item-overlay-inverse" data-blur-strength="30">
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header"><img src="{{'/storage/images/news/'.$row->image}}" alt="{{$row->name}}" /></a>
                            
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                <strong>  @if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif <p class="text-right text-small"> @if(!empty($row->photographer))@lang('layout.photographer')   :  {{$row->photographer}} @endif</p> </strong>
                                <canvas></canvas>
                            </a>
                            
                        </div>
                        @else
                        <div class="item item-overlay-blur">
                                <a href="/news/{{$row->slung}}/{{Session::get('lang')}}l" class="item-header"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="{{$row->name}}" /></a></a>
                            <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                <strong>@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</strong>
                                <canvas></canvas>
                            </a>
                        </div>
                        @endif
                        @php($i++)
                        @endforeach
                        @else 
                        <h3> @lang('layout.notpost') </h3>
                        @endif
                    </div>
                    <div class="pagination">
                        {{ $main->links() }}             
                    </div>
                </div>
            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar start-->
                @include('sidebar.sidebar-gallery')
                <!-- BEGIN .sidebar End -->
            </div>
        </div>
        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>

@endsection