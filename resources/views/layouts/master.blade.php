<!DOCTYPE HTML>
<!-- BEGIN html -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- BEGIN head -->
    <head>
        <title> @if(isset($title)) {{$title}} @else {{$title}} @endif | Dssbulletin</title>
        <!-- Meta Tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="/assets/sitetheme/images/favicon.png" type="image/x-icon" />
        <!-- Stylesheets -->
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Material+Icons%7CRoboto:400,500,700&amp;subset=latin,latin-ext" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/reset.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/otgrid.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/shortcodes.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/main-stylesheet.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/ot-lightbox.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/custom-styles.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/responsive.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/_ot-demo.min.css" />
        <link type="text/css" rel="stylesheet" href="/assets/sitetheme/css/styles.css" />
        @if(Session::get('lang') == 'bd')
        <style>
            @font-face {
                    font-family:"SolaimanLipi";
                        src:url("/assets/sitetheme/fonts/solaiman-lipi/SolaimanLipi.eot?") 
                        format("eot"),url("/assets/sitetheme/fonts/solaiman-lipi/SolaimanLipi.woff") 
                        format("woff"),url("/assets/sitetheme/fonts/solaiman-lipi/SolaimanLipi.ttf") 
                        format("truetype"),url("/assets/sitetheme/fonts/solaiman-lipi/SolaimanLipi.svg#SolaimanLipi") 
                        format("svg");
                        font-weight:normal;
                        font-style:normal;
                }
            body, h1, h2, h3, h4, h5, h6, p, a{  font-family: SolaimanLipi; color: #000;}
            #main-menu-wrapper .otm > ul > li > a, 
            #second-menu-wrapper .otm .sub-menu > li > a,
            #second-menu-wrapper .otm .sub-menu > li:hover > a,
            #second-menu-wrapper .otm > ul > li > a,
            #second-menu-wrapper .otm > ul > li:hover > a{
                font-size:19px;
            }
            #hyper-menu > li > a {
                        font-size: 24px;
                }
               .hyper-menu-langs a, .header-right-side li a{
                    font-size: 20px;
                }
                 .hyper-menu-langs a{
                    font-size: 18px;
                }
                .ot-article-list-large .item-content p, a, .hd-article-block-grid .item-meta{
                        color:#000;
                        font-size: 18px;
                }
        </style>
        @endif
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--[if lte IE 8]>
        <link type="text/css" rel="stylesheet" href="css/ie-ancient.min.css" />
        <![endif]-->
    </head>
    <!-- BEGIN body -->
    <body class="ot_debug ot-menu-will-follow">
        <div class="hentry">
            <span class="entry-title" style="display: none;">Home</span>
            <span class="vcard" style="display: none;">
                <span class="fn"><a href="" title="Posts by Orange Themes" rel="author">Orange Themes</a></span>
            </span>
            <span class="updated" style="display:none;">2016-04-18T08:17:28+00:00</span>
        </div>
        <!-- BEGIN #boxed -->
        <div id="boxed">
            <!-- BEGIN #header -->
            <header id="header">
                <!-- BEGIN .container -->
                <div class="container">
                    <a href="{{ route('welcome',Session::get('lang')) }}" id="header-logo" style="padding:0px;">
                        <!--<img style="padding:12px;" src="/assets/sitetheme/images/logo.png"  alt="Reporter" /> -->
                        <img style="margin-top:15px; margin-bottom:15px; width: auto;" src="/assets/sitetheme/images/gov.logo.png" alt="logo" />
                        <img style="margin-top:15px; margin-bottom:15px; margin-left: 60px;  width: auto;" src="/assets/sitetheme/images/somagkollanlogo.png" alt="logo" />
                    </a>                    
                    <ul id="hyper-menu" style="margin-top:10px;margin-left:30px">
                        <li>
                            <a class="custom-color" href="#"><i class="material-icons">menu</i> @lang('layout.menu')</a>
                            <ul class="hyper-menu-inner">
                                <li>
                                    <canvas id="hypermenu-canvas" width="1920" height="700"></canvas>
                                    <div class="container">
                                        <div class="otg otg-items-4 otg-h-30 otg-v-30">
                                            @if (count($categories) > 0)
                                            @foreach ($categories as $row)
                                            @if ($row->parent_id == 0)
                                            <div class="menu-widget-column otg-item">
                                                <div class="widget">
                                                    <h3><span>
                                                            @if(Session::get('lang') == 'bd')
                                                            {{$row->name_bd}}
                                                            @else 
                                                            {{$row->name_en}}
                                                            @endif
                                                        </span></h3>
                                                    <div class="ot-widget-article-list">
                                                        @foreach ($categories as $sub_row)
                                                        @if ($sub_row->parent_id == $row->id)
                                                        <div class="item">             
                                                            @if(Session::get('lang') == 'bd')
                                                            <a href="/category/{{$sub_row->slung_bd}}/{{Session::get('lang')}}">{{$sub_row->name_bd}}</a>
                                                            @else 
                                                            <a href="/category/{{$sub_row->slung_en}}/en">{{$sub_row->name_en}}</a>
                                                            @endif
                                                        </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                    @if(Session::get('lang') == 'bd')
                                                    <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}"  class="ot-widget-button">@lang('layout.view-more-articles')</a>
                                                    @else 
                                                    <a href="/category/{{$row->slung_en}}/{{Session::get('lang')}}"  class="ot-widget-button">@lang('layout.view-more-articles')</a>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                            @else
                                            I don't have any records! {{ count($categories) }}
                                            @endif
                                        </div>
                                        <div class="hyper-menu-footer">
                                            <div class="left hyper-menu-langs">
                                                @if(Session::get('lang') == 'bd')  custom-color
                                                <a class="custom-color change-language active" style="line-height: 20px;margin:0px;"  href="{{ route('login',['lang'=>Session::get('lang')]) }}" data-flag = "bd" >@lang('layout.bangla')</a>                                                
                                                @else 
                                                <a class="custom-color change-language" style="line-height: 20px;margin:0px;"  href="{{ route('login',['lang'=>Session::get('lang')]) }}" data-flag = "bd" >@lang('layout.bangla')</a>   
                                                @endif
                                                @if(Session::get('lang') == 'en') 
                                                <a class="custom-color change-language active" style="line-height: 20px;margin:0px;" href="{{ route('login',['lang'=>Session::get('lang')]) }}" data-flag = "en" >@lang('layout.english')</a>                                           
                                                @else 
                                                <a class="custom-color change-language" style="line-height: 20px;margin:0px;"  href="{{ route('login',Session::get('lang',['lang'=>Session::get('lang')])) }}" data-flag = "en" >@lang('layout.english')</a>   
                                                @endif
                                            </div>
                                            <div class="right hyper-menu-socials">
                                                <a href="https://www.facebook.com/dss.gov.bd" target="_blank"><i class="fa fa-facebook"></i>Facebook us</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul style="margin-top:10px;" class="right header-right-side load-responsive"> 
                        <li class="with-split"><a class="custom-color"  href="{{route('login')}}/{{Session::get('lang')}}">@lang('layout.login')</a></li>
<!--                        <li><a class="custom-color" href="https://www.facebook.com/dss.gov.bd" target="_blank"><i class="fa fa-facebook"></i></a></li>-->
                    </ul>
                    <div class="right hyper-menu-langs" style="padding-top: 25px; display: flex; align-items:center;">
                        
                                @if(Session::get('lang') == 'en')  
                                        <a  @if(Session::get('lang') == 'bd') class="active custom-color" @endif class="custom-color" style="color:#2f323b;line-height: 20px;margin:0px;margin-left: 20px;" href="#" data-flag="bd">
                                        @lang('layout.bangla')</a>
                                @endif
                                @if(Session::get('lang') == 'bd')  
                                        <a @if(Session::get('lang') == 'en') class="active custom-color" @endif class="custom-color" style="color:#2f323b;line-height: 20px;margin:0px; margin-left: 10px; margin-right: 15px; " href="#" data-flag="en">
                                        @lang('layout.english')</a>
                                @endif
                    </div>
                    <!-- END .container -->
                </div>
                <div id="main-menu-wrapper">
                    <!-- BEGIN .container -->
                    <div class="container">
                        <nav class="otm otm-follow">
                            <ul class="load-responsive">
                                <li><a href="{{ route('welcome',Session::get('lang')) }}">@lang('layout.home')</a></li>
                                <li><a href="{{ route('blog',Session::get('lang')) }}">@lang('layout.blog')</a></li>
                                <li><a href="#"><span>@lang('layout.megamenu')</span></a>
                                    <ul class="sub-menu ot-mega-menu">
                                        <li class="menu-widgets">
                                            <div class="otg otg-items-4 otg-h-30 otg-v-30">
                                                <!-- BEGIN .menu-widget-column -->
                                                <div class="menu-widget-column otg-item">
                                                    <div class="widget">
                                                        <h3><span>@lang('layout.some-cool')</span></h3>
                                                        <div class="shortcode-content">
                                                            @php($i = 0)
                                                            @foreach($cool_video as $row) 
                                                            @if($i == 1)
                                                            @break
                                                            @endif
                                                            <a href="/video/{{Session::get('lang')}}" class="ot-short-video-preview">
                                                                <span class="ot-short-video-header">
                                                                    {{--$row->video--}}
                                                                    <img src="/assets/sitetheme/images/video-icon.png">
                                                                </span>
                                                                <span class="ot-short-video-content">
                                                                    <strong>    @if(Session::get('lang') == 'bd') {{$row->name}}    @else  {{$row->name_en}}  @endif </strong>
                                                                    <span class="button">@lang('layout.watch-video')</span>
                                                                </span>
                                                            </a>
                                                            @php($i++)
                                                            @endforeach
                                                        </div>
                                                    </div>

                                                    <!-- END .menu-widget-column -->
                                                </div>
                                                <!-- BEGIN .menu-widget-column -->
                                                <div class="menu-widget-column otg-item">
                                                    <div class="widget">
                                                        <h3><span>@lang('layout.tag-cloud')</span></h3>
                                                        <div class="tagcloud">
                                                            @foreach($tags as $row)
                                                            @if(Session::get('lang') == 'bd')
                                                            <a href="/tags/{{$row->slung}}/{{Session::get('lang')}}">{{$row->name}}</a>
                                                            @else 
                                                            <a href="/tags/{{$row->slung}}/{{Session::get('lang')}}"> {{$row->name_en}}</a>
                                                            @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- END .menu-widget-column -->
                                                </div>
                                                <!-- BEGIN .menu-widget-column -->
                                                <div class="menu-widget-column otg-item">
                                                    <div class="widget">
                                                        <h3><span>@lang('layout.populerarticle')</span></h3>
                                                        <div class="ot-widget-article-list">
                                                            @php($i = 0)
                                                            @foreach($popular_news as $row) 
                                                            @if($i == 4)
                                                            @break
                                                            @endif
                                                            <div class="item">
                                                                <div class="item-header">
                                                                    <a href="/news/{{$row->slung}}/{{Session::get('lang')}}"><img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="{{$row->name}}" /></a>
                                                                </div>
                                                                <div class="item-content">
                                                                    <h4><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</a></h4>
                                                                </div>
                                                            </div>
                                                            @php($i++)
                                                            @endforeach
                                                        </div>
                                                        <a href="/category/{{Session::get('lang')}}" class="ot-widget-button">@lang('layout.view-more-articles')</a>
                                                    </div>
                                                    <!-- END .menu-widget-column -->
                                                </div>
                                                <!-- BEGIN .menu-widget-column -->
                                                <div class="menu-widget-column otg-item">

                                                    <div class="widget">
                                                        <h3><span>@if(Session::get('lang') == 'bd') {{ $about_us->name }} @else {{ $about_us->name_en }} @endif </span></h3>
                                                        <div >
                                                            @if(Session::get('lang') == 'bd') {!! $about_us->description !!} @else  {!! $about_us->description_en !!} @endif
                                                        </div>
                                                    </div>

                                                    <!-- END .menu-widget-column -->
                                                </div>

                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#"><span>@lang('layout.gallery')</span></a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('gallery',Session::get('lang')) }}">@lang('layout.image-gallery')</a></li>
                                        <li><a href="{{ route('video',Session::get('lang')) }}">@lang('layout.video-gallery')</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('appointment',Session::get('lang')) }}">@lang('layout.appointment')</a></li>
                                <li><a href="{{ route('contact',Session::get('lang')) }}">@lang('layout.contact')</a></li>
                            </ul>
                        </nav>
                        <a href="#" class="right search-header-bull"><i class="material-icons">search</i></a>
                        <!-- END .container -->
                    </div>
                </div>
                <div id="second-menu-wrapper">
                    <!-- BEGIN .container -->
                    <div class="container">
                        <nav class="otm">
                            <ul class="load-responsive">
                                @if (count($categories) > 0)
                                @foreach ($categories as $row)
                                @if ($row->parent_id == 0)
                                @if(Session::get('lang') == 'bd')
                                <li> <a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}"> {{$row->name_bd}}</a></li>
                                @else 
                                <li> <a href="/category/{{$row->slung_en}}/en">{{$row->name_en}}</a></li>
                                @endif
                                @endif
                                @endforeach
                                @endif
                            </ul>
                        </nav>
                        <!-- END .container -->
                    </div>
                </div>
                <div id="search-overlay">
                    <div id="search-overlay-inner">
                        <form action="{{ route('search',Session::get('lang')) }}" method="get">
                            <input name="search" type="text" placeholder="Search something.." />
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <strong class="category-listing-title"><span>Articles worth reading</span></strong>
                        <div class="ot-content-block hd-article-block-grid">
                            <div class="otg otg-items-3 otg-h-30 otg-v-30">
                                @foreach($popular_news as $row)
                                <div class="otg-item">
                                    <div class="item">
                                        <a href="/news/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                            <img src="{{'/storage/images/news/'.$row->image}}" alt="@if(Session::get('lang') == 'bd') {{ $row->name }} @else {{ $row->name_en }} @endif" />
                                        </a>
                                        <div class="item-content">
                                            
                                            <h3><a href="/news/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') == 'bd') {{ $row->name }} @else {{ $row->name_en }} @endif</a></h3>
                                            
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END #header -->
            </header>
            @yield('content')
            <!-- BEGIN #footer -->
            <footer id="footer">
                <!-- BEGIN .container -->
                <div class="container">
                    <div class="footer-top-block">
                        <div class="left hyper-menu-langs">
                            <a @if(Session::get('lang') == 'bd') class="change-language active" @else class="change-language"   @endif data-flag = "bd" href="/bd">@lang('layout.bangla')</a>
                            <a @if(Session::get('lang') == 'en') class="change-language active" @else class="change-language"  @endif data-flag = "en" href="/en" >@lang('layout.english')</a>
                        </div>
                        <div class="right hyper-menu-socials">
                            <!--<a href="https://www.facebook.com/dss.gov.bd" target="_blank"><i class="fa fa-facebook"></i>Facebook us</a>-->
                        </div>
                    </div>
                    <hr>
                    <div class="footer-menu-blocks otg otg-items-4 otg-h-20">
                        @php($i=0)
                        @foreach($categories as $row)
                        @if($row->parent_id == 0)
                        <div class="otg-item">
                            @if(Session::get('lang') == 'bd')
                            <h3><a href="/category/{{$row->slung_bd}}/{{Session::get('lang')}}"> {{$row->name_bd}}</a></h3>
                            @else 
                            <h3><a href="/category/{{$row->slung_en}}/{{Session::get('lang')}}"> {{$row->name_en}}</a></h3>
                            @endif
                            <ul>
                                @foreach($categories as $row_sub)
                                @if($row->id ==  $row_sub->parent_id)
                                @if(Session::get('lang') == 'bd')
                                <li><a href="/category/{{$row_sub->slung_bd}}/{{Session::get('lang')}}"> {{$row_sub->name_bd}}</a></li>
                                @else 
                                <li><a href="/category/{{$row_sub->slung_en}}/{{Session::get('lang')}}"> {{$row_sub->name_en}}</a></li>
                                @endif
                                @endif
                                @endforeach 
                            </ul>
                        </div>
                        @endif
                        @if($i==15)
                        @break;
                        @endif
                        @php($i++)
                        @endforeach 
                    </div>
                    <hr>
                    <div class="copyright-strip">
                        <div class="left">
                            <p>©<a href="http://dss.gov.bd/" target="_blank">সমাজসেবা অধিদফতর |</a>
                                <a href="http://www.msw.gov.bd/" target="_blank">সমাজকল্যাণ মন্ত্রণালয় 2019- {{date("Y")}}</a> </p>
                        </div>
                        <div class="right">
                            <p><a href="http://4beats.net/">কারিগরি সহযোগিতায়ঃ 4বিটস লিমিটেড </a></p>
                            <ul>
                                <li><a href="/"> @lang('layout.home')</a></li>
                                <li><a href="/blog/{{Session::get('lang')}}"> @lang('layout.blog')</a></li>
                                <li><a href="/gallery/{{Session::get('lang')}}"> @lang('layout.gallery')</a></li>
                                <li><a href="/appointment/{{Session::get('lang')}}"> @lang('layout.appointment')</a></li>
                                <li><a href="/contact/{{Session::get('lang')}}"> @lang('layout.contact')</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- END .container -->
                </div>
                <!-- END #footer -->
            </footer>
            <!-- END #boxed -->
        </div>
        <!-- Scripts -->
        <script type="text/javascript" src="/assets/sitetheme/jscript/jquery-latest.min.js"></script>
        <script type="text/javascript" src="/assets/sitetheme/jscript/html2canvas.min.js"></script>
        <script>
//var _otmLogo = 'Reporter';
var _otmLogo = '<img src="/assets/sitetheme/images/logo.png" alt="Reporter" />';
var _otmSearch = "blog.html";
        </script>
        <script type="text/javascript" src="/assets/sitetheme/jscript/otmenu.min.js"></script>
        <script type="text/javascript" src="/assets/sitetheme/jscript/shortcode-scripts.min.js"></script>
        <script type="text/javascript" src="/assets/sitetheme/jscript/theme-scripts.min.js"></script>
        <script type="text/javascript" src="/assets/sitetheme/jscript/ot-lightbox.min.js"></script>
        <!-- Demo Only -->
        <script type="text/javascript" src="/assets/sitetheme/jscript/_ot-demo.min.js"></script>
        <script type="text/javascript" src="/assets/sitetheme/js/scripts.js"></script>
        <div id="fb-root"></div>
        <script>
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1765005913812518&autoLogAppEvents=1';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
        </script>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1765005913812518&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
                </script>
    </body>
    <!-- END html -->
</html>