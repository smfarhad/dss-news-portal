@extends('layouts.master')
@section('content')
<!-- BEGIN #content -->
<main id="content">
    <!-- BEGIN .container -->
    <div class="container">
        <div class="otg otg-h-30">
            <div class="otg-item otg-u-4">
                <div class="ot-title-block">
                    <h2>  @if(Session::get('lang')== 'bd') {{$title_bd}}  @else {{$title}}  @endif</h2>
                </div>
                <div class="ot-content-block">
                    <div class="article-front-grid">
                        @php ($i = 0)
                        @if ($blogPost->count() > 0)
                        @foreach ($blogPost as $row)
                        @if ($i == 0)
                        <div class="item item-overlay-blur" data-blur-strength="30"
                             <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}" class="item-header"><img style="width:100%;" src="{{'/storage/images/blog/'.$row->image}}" alt="" /></a>
                            
                                <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                    <strong>  @if( Session::get('lang') == 'en') {{$row->name_en}} @else {{$row->name}} @endif</strong>
                                    <canvas></canvas>
                                </a>
                            
                        </div>
                        @else
                        <div class="item item-overlay-blur">
                                <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}" class="item-header"><img src="{{'/storage/images/blog_thumb/'.$row->image}}" alt="" /></a>
                                <a href="/blog/{{$row->slung}}/{{Session::get('lang')}}" class=item-overlay>
                                    <strong>  @if( Session::get('lang') == 'en') {{$row->name_en}} @else {{$row->name}} @endif  </strong>
                                    <canvas></canvas> 
                                </a>
             
                        </div>
                        @endif
                        @php($i++)
                        @if($i==9)
                        @break
                        @endif
                        @endforeach
                        @else
                         <div class="otg otg-items-1 otg-h-30 otg-v-30">
                            <div class="otg-item">
                                <div class="item">
                                    <div class="item-content">

                                        <h3>@lang('layout.notpost') </h3>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="pagination">
                         {{ $blogPost->links() }}          
                    </div>
                </div>
            </div>
            <div class="otg-item otg-u-2">
                <!-- BEGIN .sidebar -->
                <aside class="sidebar">
                  @include('sidebar.blog-sidebar')
                </aside>
            </div>
        </div>
        <!-- END .container -->
    </div>
    <!-- BEGIN #content -->
</main>

@endsection