@extends('layouts.master')
@section('content')
<main id="content">
    <div class="ot-single-photo-block">
        <div class="container">
            <div class="ot-single-photo-block-inner">
                       <img src="{{'/storage/images/news/'.$details->image}}" alt="{{$details->name}}"  style="width:100%; height: 600px"/>
            </div>
            <div class="photo-gallery-thumbs">
                <button class="photo-gallery-nav-left"><i class="material-icons">keyboard_arrow_left</i></button>
                <button class="photo-gallery-nav-right"><i class="material-icons">keyboard_arrow_right</i></button>
                <div class="photo-gallery-thumbs-inner" data-thumbs-start-from="0">
                    <a href="#" class="item active" data-image="{{'/storage/images/news/'.$details->image}}"
                                                        data-title=""
                                                        data-description="">
                                                   <img src="{{'/storage/images/news_thumb/'.$details->image}}" alt="{{$details->image}}" />                                            
                                                </a>
                        @php($i=0)
                       @if(json_decode($details->image_gallery)>0)
                                @foreach(json_decode($details->image_gallery) as $image)
                                                <a href="#" class="item" 
                                                        data-image="{{'/storage/images/news/'.$image}}"
                                                        data-title=""
                                                        data-description="">
                                                   <img src="{{'/storage/images/news_thumb/'.$image}}" alt="$image" />                                            
                                                </a>
                                        @php($i++)
                                @endforeach 
                        @endif
                       
                </div>
            </div> 
            <div class="photo-gallery-description">
                <div class="ot-content-block">
                <p class="text-right"> @if(!empty($details->photographer))@lang('layout.photographer')   :  {{$details->photographer}} @endif</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN .container -->
    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-6">

                <div class="ot-title-block">
                    <h2>Similar Galleries</h2>
                </div>

                <div class="ot-content-block">

                    <div class="photo-block-grid photo-grid-large">
                            <div class="otg otg-items-4 otg-h-30 otg-v-30">

                                    @php($i=0)
                                    @foreach($news as $row)
                                           @if($i < 4)
                                            <div class="otg-item">
                                                <div class="item">
                                                    <a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}" class="item-header">
                                                        <i class="material-icons">&#xE412;</i>
                                                        <img src="{{'/storage/images/news_thumb/'.$row->image}}" alt="{{$row->name}}" />
                                                        <p class="text-right text-small"> @if(!empty($details->photographer))@lang('layout.photographer')   :  {{$details->photographer}} @endif</p>
                                                    </a>
                                                    <div class="item-content">
                                                        <h3><a href="/gallery/{{$row->slung}}/{{Session::get('lang')}}">@if(Session::get('lang') =='en'){{$row->name_en}} @else {{$row->name}}@endif</a></h3>
                                                        <div class="item-meta">
                                                                <span class="item-meta-item"><i class="material-icons">&#xE412;</i>  
                                                                    @if(json_decode($row->image_gallery)>0){{count(json_decode($row->image_gallery))}}  @else  0 @endif photos
                                                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           @php($i++)
                                           @else
                                           @break
                                           @endif

                                         @endforeach 

                            </div>
                    </div>

                </div>

            </div>
        </div>

        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>
@endsection
