@extends('layouts.master')
@section('content')
<!-- BEGIN #content -->
<main id="content">
<!--
    <div class="full-width-map ot-content-block">
        <iframe src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=333+E+34th+St,+New+York,+NY&aq=1&oq=333&sll=37.269174,-119.306607&sspn=16.742323,33.815918&ie=UTF8&hq=&hnear=333+E+34th+St,+New+York,+10016&t=m&z=14&ll=40.744403,-73.974467&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
-->
    <!-- BEGIN .container -->
    <div class="container">

        <div class="otg otg-h-30">
            <div class="otg-item otg-u-3">

                <div class="ot-title-block">
                    <h2>@if(Session::get('lang') =='en'){{$page->name_en}} @else{{$page->name}}@endif</h2>
                </div>

                <div class="ot-content-block">

                    <div class="shortcode-content">

                      @if(Session::get('lang') =='en'){{$page->description_en}} @else{{$page->description}}@endif

                        <hr>

                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-map-marker"></i>@if(Session::get('lang') =='en'){{$page->address}} @else{{$page->address}}@endif</li>
                            <li><i class="fa-li fa fa-envelope"></i><a href="#">@if(Session::get('lang') =='en'){{$page->email}} @else{{$page->email}}@endif</a></li>
                            <li><i class="fa-li fa fa-phone"></i>@if(Session::get('lang') =='en'){{$page->phone}} @else{{$page->phone}}@endif</li>
                        </ul>

                        <hr>

                        <p>
                                <a class="ot-shortcode-social-icon social-facebook soc-bef hover-color-facebook" href="https://www.facebook.com/dss.gov.bd" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                        <strong>Facebook</strong>
                                        <span>Like Page</span>
                                </a>

                        </p>

                    </div>

                </div>

            </div>
            <div class="otg-item otg-u-3">

                <div class="ot-title-block">
                    <h2>Send us a message</h2>
                </div>

                <div class="ot-content-block">
                    <div class="comment-contact-form">
                        <div id="respond">
                          <!--<form action="/mail/send" method="POST"> -->
                           <form action="#" method="GET"> 
                                <!--
                                                                                                                        <div class="alert-message ot-shortcode-alert-message alert-green">
                                                                                                                                <strong>Success! This a success message</strong>
                                                                                                                        </div>
                                -->
                                <!--
                                                                                                                        <div class="alert-message ot-shortcode-alert-message alert-red">
                                                                                                                                <strong>Error! This an error message</strong>
                                                                                                                        </div>
                                                                                                                        <div class="alert-message ot-shortcode-alert-message">
                                                                                                                                <strong>Warning! This a warning message</strong>
                                                                                                                        </div>
                                -->

                                <p class="contact-form-user">
                                    <label class="label-input">
                                        <span>Nickname<i class="required">*</i></span>
                                        <input type="text" placeholder="Nickname" name="nickname" value="">
                                    </label>
                                </p>
                                <p class="contact-form-email">
                                    <label class="label-input">
                                        <span>E-mail<i class="required">*</i></span>
                                        <input type="email" placeholder="E-mail" name="email" value="">
                                    </label>
                                </p>
                                <p class="contact-form-comment">
                                    <label class="label-input">
                                        <span>Comment text<i class="required">*</i></span>
                                        <textarea name="comment" placeholder="Comment text"></textarea>
                                    </label>
                                </p>
                                <p class="form-submit">
                                    <button name="submit" type="submit" id="submit" class="submit">Send us a message</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- END .container -->
    </div>

    <!-- BEGIN #content -->
</main>
@endsection		