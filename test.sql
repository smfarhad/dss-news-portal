select`posts`.*, `c1`.`id` as `parent_category_id`, `c1`.`name_bd` as `category_name_bd`, `c1`.`name_en` as `category_name_en`, `c1`.`slung_en` as `category_slung_en`, `c1`.`slung_bd` as `category_slung_bd`, `c2`.`id` as `main_category_id`, `c2`.`name_bd` as `main_category_name`, `c2`.`slung_bd` as `main_slung_bd`, `c2`.`slung_en` as `main_slung_en` 
from `posts` 
inner join `categories` as `c1` on `posts`.`category` = `c1`.`id` 
inner join `categories` as `c2` on `c2`.`id` = `c1`.`parent_id` 
where `posts`.`name` like ? and `posts`.`status` = ? order by `posts`.`id` desc