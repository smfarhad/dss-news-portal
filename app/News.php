<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
      use Notifiable;
    // table name
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = ['id','status','name','name_en','video','cool_video','photographer','featured','slung','image', 'image_gallery','description','description_en','flags','language', 'category','created_by','created_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];
}
