<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * contact
     *
     * @return void
     */
    public $contact;

    public function __construct($contact) {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $demo = new \stdClass();
        $demo->demo_one = 'Demo One Value';
        $demo->demo_two = 'Demo Two Value';
        $demo->sender = 'SenderUserName';
        $demo->receiver = 'ReceiverUserName';
        return $this->from('farhad1556@gmail.com')
                        ->view('mails.contact')
//                        ->text('mails.demo_plain')
                        ->with(
                                [
                                    'demo' =>$demo,
                                    'testVarOne' => '1',
                                    'testVarTwo' => '2',
                        ])
                        ->attach(public_path('/images') . '/demo.jpg', [
                            'as' => 'demo.jpg',
                            'mime' => 'image/jpeg',
        ]);
    }

}
