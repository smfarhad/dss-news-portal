<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
      use Notifiable;
    // table name
    protected $table = 'blog_tags';
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'name_en', 'slung', 'slung_en'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
      protected $hidden = ['created_at','updated_at'];
}
