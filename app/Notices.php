<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Notices extends Model
{
      use Notifiable;
    // table name
    protected $table = 'notices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [ 'id', 'title', 'published', 'download'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}
