<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Notices;
use App\News;
use App\Tags;
use App\BlogCategory;
use App\BlogPost;
use App\BlogTags;
use App\Pages;
use Session;
use Illuminate\Support\Facades\App;

class BlogController extends Controller {

    public $pagination;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->pagination = 7;
    }

    public function blog($lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Blog';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'ব্লগ';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $blog = BlogPost::select('blog_posts.*', 'c1.parent_id as parent_category', 'c2.slung_bd', 'c2.slung_en', 'c2.name_bd as parent_name', 'c1.name_bd as category_name')
                ->leftJoin('blog_categories as c1', 'c1.id', '=', 'blog_posts.category')
                ->leftJoin('blog_categories as c2', 'c2.id', '=', 'c1.parent_id')
                ->where('blog_posts.status', '=', 3)
                ->orderBy('blog_posts.id', 'DESC')
                ->paginate($this->pagination);

        $data['blogPost'] = $blog;
        $data['blogCategory'] = BlogCategory::get();
        $data['blogtags'] = BlogTags::get();
        $data['popularBlogPost'] = $this->popularPost();
        $data ['latestPost'] = $this->latestPost();
        $data['title_bd'] = 'ব্লগ';

        return view('blog', $data);
    }

    public function singlePost($slung, $lang = 'bd') {
        $data = $this->pageComponent();
        $details = BlogPost::where('slung', '=', $slung)->get();
        if (count($details) > 0) {
            $details = $details->first();

            if ($lang == 'en') {
                $data['title'] = $details->name;
                Session::put('lang', $lang);
            } else {
                $data['title'] = $details->name;
                Session::put('lang', 'bd');
            }
            App::setLocale($lang);

            $data['blogCategory'] = BlogCategory::get();
            $data['blogtags'] = BlogTags::get();
            $data['popularBlogPost'] = $this->popularPost();
            $data['latestPost'] = $this->latestPost();
            $data['details'] = $details;
            $this->increasePopular($details->id, $details->popular);
            $data['title_bd'] = 'ব্লগ';
            $data['title'] = 'Blog';
            return view('single-blog', $data);
        } else {
            return redirect()->to('/' . Session::get('lang'));
        }
    }

    public function latestPost($lang = 'bd') {
        $news = BlogPost::where('status', '=', 3)
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
        return $news;
    }

    public function pageComponent() {

        $categories = Category::get();
        $tags = Tags::get();
        $popular_news = News::where('posts.status', '=', 3)
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
        $aboutUs = Pages::where('slung', 'about_us')->first();

        return $data = [
            'categories' => $categories,
            'tags' => $tags,
            'cool_video' => $this->coolVideo(),
            'popular_news' => $popular_news,
            'about_us' => $aboutUs,
            'ads' => $this->addBlock()];
    }

    public function popularPost($lang = 'bd') {
        $blogPost = BlogPost::where('blog_posts.status', '=', 3)
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
        return $blogPost;
    }

    public function addBlock() {
        $show = Pages::where('slung', 'adds_settings')->first();
        if ($show->phone == 1) {
            return $show->name;
        } else {
            return "<a href=" . $show->email . "><img src=/storage/images/adsblock_thumb/" . $show->name_en . "></a>";
        }
    }

    public function chnageLanguage($lang = 'bd') {
        App::setLocale($lang);
        Session::put('lang', $lang);
        return Session::get('lang');
    }

    public function coolVideo() {
        return News::select('posts.*')
                        ->where('posts.cool_video', '=', 1)
                        ->where('posts.status', '=', 3)
                        ->orderBy('posts.id', 'DESC')
                        ->get();
    }

    public function increasePopular($id, $popular) {
        $presentView = $popular + 1;
        BlogPost::where('id', $id)->update(['popular' => $presentView]);
    }

    public function search($lang = 'bd', Request $request) {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Blog Search';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'ব্লগ সার্চ';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $blog = BlogPost::select('blog_posts.*', 'c1.parent_id as parent_category', 'c2.slung_bd', 'c2.slung_en', 'c2.name_bd as parent_name', 'c1.name_bd as category_name')
                ->leftJoin('blog_categories as c1', 'c1.id', '=', 'blog_posts.category')
                ->leftJoin('blog_categories as c2', 'c2.id', '=', 'c1.parent_id')
                ->where('blog_posts.name', 'like', "%$request->search%")
                ->orWhere('blog_posts.name_en', 'like', "%$request->search%")
                //->where('blog_posts.description', 'like', "%$request->search%")
                //->orWhere('blog_posts.description_en',  'like',"%$request->search%")
                ->where('blog_posts.status', '=', 3)
                ->orderBy('blog_posts.id', 'DESC')
                ->paginate($this->pagination);
        $data['blogPost'] = $blog;
        $data['blogCategory'] = BlogCategory::get();
        $data['blogtags'] = BlogTags::get();
        $data['popularBlogPost'] = $this->popularPost();
        $data ['latestPost'] = $this->latestPost();
        $data['title_bd'] = 'ব্লগ';
        return view('blog', $data);
    }

}
