<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller {

    public function send() {
        
        $demo = new \stdClass();
        $demo->demo_one = 'Demo One Value';
        $demo->demo_two = 'Demo Two Value';
        $demo->sender = 'SenderUserName';
        $demo->receiver = 'ReceiverUserName';

        Mail::to("farhad1556@gmail.com")->send(new ContactEmail($demo));
    }

}
