<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Notices;
use App\News;
use App\Pages;
use App\Tags;
use Session;
use Illuminate\Support\Facades\App;

class HomeController extends Controller {

    public $pagination;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
        $this->pagination = 7;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome($lang = 'bd') {
       
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Home';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'খবর';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $data['news'] = News::select('posts.*', 'c1.parent_id as parent_category', 'c1.name_bd as category_name')
                                        ->join('categories as c1', 'posts.category', '=', 'c1.id')
                                        ->where('posts.status', '=', 3)
                                        ->orderBy('posts.id', 'DESC')
                                        ->get();

        $data['marquee'] =  $data['featured_news'];
        return view('welcome', $data);
    }

    public function news($slung, $lang = 'bd') {

        $local = App::getLocale();
        App::setLocale($lang);
        Session::put('lang', $lang);

        $data = $this->pageComponent();
         $details = News::join('users as u', 'posts.created_by', '=', 'u.id')
                                           ->join('designation as d', 'u.job_title', '=', 'd.id')
                                            ->join('offices as o', 'u.office', '=', 'o.id')
                                             ->select('posts.*', 'u.name as created_by_name',  'u.job_title', 'd.name_en as designation_name_en', 'd.name_bd as designation_name_bd',
                                                        'o.name_en as office_name_en', 'o.name_bd as office_name_bd')
                                                    ->where('slung', '=', $slung)->get();
       if($details->count()>0){
        $details = $details->first();
        if (Session::get('lang') == 'bd') {
            $data['title'] =  $details->name;
        } else {
            $data['title'] = $details->name;
            
            //$details = News::where('slung', '=', $slung)->first();
                $details = News::join('users as u', 'posts.created_by', '=', 'u.id')
                                           ->join('designation as d', 'u.job_title', '=', 'd.id')
                                            ->join('offices as o', 'u.office', '=', 'o.id')
                                             ->select('posts.*', 'u.name as created_by_name',  'u.job_title', 'd.name_en as designation_name_en', 'd.name_bd as designation_name_bd',
                                                        'o.name_en as office_name_en', 'o.name_bd as office_name_bd')
                                                    ->where('slung', '=', $slung)->first();
        }
     
        $this->increasePopular($details->id, $details->popular);
        $data ['details'] = $details;
        $data ['latestPost'] =  $this->latestPost();

        return view('single-news', $data);
          }else{
                return redirect()->to('/'.Session::get('lang'));
       }
    }

    public function tags($slung, $lang = 'bd') {
      $data = $this->pageComponent();
        if ($lang == 'en') {
                $data['title'] = 'tags';
                Session::put('lang', $lang);
        } else {
                $data['title'] = 'ট্যাগ';
                Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $main = News::select('posts.*', 'c.parent_id as parent_category', 'c.slung_bd', 't.name as tabs', 'c.name_bd as category_name', 'posts.id')
                ->join('categories as c', 'posts.category', '=', 'c.id')
                ->join('tags as t', 'posts.tag', '=', 't.id')
                
                ->where('t.slung', $slung)
                ->where('posts.status', '=', 3)
                ->orWhere('t.slung_en', $slung)
                ->orderBy('posts.id', 'DESC')
                ->paginate($this->pagination);

  
        $data['title'] = 'tags';
        $data['title_bd'] = 'ট্যাগ';
        $data['main'] = $main;
        $data['img_path'] = '/storage/images/news/';
        $data['thumb_path'] = '/storage/images/news_thumb/';
        return view('tags', $data);
    }

    public function gallery($lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            Session::put('lang', $lang);
            $data['title'] = 'Gallery';
        } else {
            $data['title'] = 'গ্যালারি';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $data['main'] = News::select('posts.*', 'c.parent_id as parent_category', 'c.slung_bd', 't.name as tabs', 'c.name_bd as category_name', 'posts.id')
                ->join('categories as c', 'posts.category', '=', 'c.id')
                ->join('tags as t', 'posts.tag', '=', 't.id')
                ->where('posts.status', '=', 3)
                ->orderBy('posts.id', 'DESC')
                ->paginate($this->pagination);

        $data['img_path'] = '/storage/images/news/';
        $data['thumb_path'] = '/storage/images/news_thumb/';
        return view('gallery', $data);
    }

    public function imageGallery($slung, $lang = 'bd') {

        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Gallery';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'গ্যালারি';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $data['details'] = News::where('slung', $slung)->where('posts.status', '=', 3)->first();
        $data['news'] = News::select('posts.*', 'c1.parent_id as parent_category', 'c1.name_bd as category_name')
                ->join('categories as c1', 'posts.category', '=', 'c1.id')
                ->where('posts.status', '=', 3)
                ->orderBy('posts.id', 'DESC')
                ->get();
        $data['slung'] = $slung;
        return view('gallery-single', $data);
    }

    public function videoGallery($lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Video Gallery';
            Session::put('lang', $lang);
        } else {
           $data['title'] = 'ভিডিও গ্যালারি';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $news = News::where('posts.status', '=', 3)->orderBy('posts.id', 'DESC')->paginate(10);
        $data['main'] = $news;
        $data['img_path'] = '/storage/images/news/';
        $data['thumb_path'] = '/storage/images/news_thumb/';
        return view('video', $data);
    }

    public function appointment($lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Appointment';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'এপয়েন্টমেন্ট';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $data['page'] = Pages::where("slung", "appointments")->first();
        $data['notices'] = Notices::get();
        return view('appointment', $data);
    }

    public function contact($lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Appointment';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'এপয়েন্টমেন্ট';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);

        $data['page'] = Pages::where("slung", "contact")->first();
        return view('contact', $data);
    }

    public function category($slung, $lang = 'bd') {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Category';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'ক্যাটাগরি';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $category = Category::where('slung_bd', $slung)->orWhere('slung_en', $slung)->get();
        if ($category->count() > 0) {
            $parent_id = $category->first()->parent_id;
            if ($parent_id == 0) {
                $news = News::select('posts.*', 'c1.id as parent_category_id', 'c1.name_bd as category_name_bd', 'c1.name_en as category_name_en', 'c1.slung_en as category_slung_en', 'c1.slung_bd as category_slung_bd', 'c2.id as main_category_id', 'c2.name_bd as main_category_name', 'c2.slung_bd as main_slung_bd', 'c2.slung_en as main_slung_en')
                        ->join('categories as c1', 'posts.category', '=', 'c1.id')
                        ->join('categories as c2', 'c2.id', '=', 'c1.parent_id')
                        ->where('c2.slung_en', $slung)
                        ->orWhere('c2.slung_bd', $slung)
                         ->where('posts.status', '=', 3)
                        ->orderBy('posts.id', 'DESC')
                        ->paginate($this->pagination);
            } else {
                $news = News::select('posts.*', 'c1.id as parent_category_id', 'c1.name_bd as category_name_bd', 'c1.name_en as category_name_en', 'c1.slung_en as category_slung_en', 'c1.slung_bd as category_slung_bd')
                        ->join('categories as c1', 'posts.category', '=', 'c1.id')
                        ->where('c1.slung_en', $slung)
                        ->orWhere('c1.slung_bd', $slung)
                        ->where('posts.status', '=', 3)
                        ->orderBy('posts.id', 'DESC')
                        ->paginate($this->pagination);
            }
        } else {
            $news = [];
        }
        if ($slung == 'bd' || $slung == 'en') {
            $news = News::select('posts.*', 'c1.id as parent_category_id', 'c1.name_bd as category_name_bd', 'c1.name_en as category_name_en', 'c1.slung_en as category_slung_en', 'c1.slung_bd as category_slung_bd', 'c2.id as main_category_id', 'c2.name_bd as main_category_name', 'c2.slung_bd as main_slung_bd', 'c2.slung_en as main_slung_en')
                    ->join('categories as c1', 'posts.category', '=', 'c1.id')
                    ->join('categories as c2', 'c2.id', '=', 'c1.parent_id')
                    ->where('posts.status', '=', 3)
                    ->orderBy('posts.id', 'DESC')
                    ->paginate($this->pagination);
        }
        if ($lang == 'bd') {
                if($news->count() >0){
                        $data['title'] = $news[0]->category_name_bd;
                }else{
                        $data['title'] = 'কোন পোস্ট খুঁজে পাওয়া যায়নি';
                }
               
        } else {
              if($news->count() >0){
                        $data['title'] = $news[0]->category_name_bd;
                }else{
                        $data['title'] = 'No Post Fount';
                }
                
        }
       
        $data['img_path'] = '/storage/images/news/';
        $data['thumb_path'] = '/storage/images/news_thumb/';
        $data['main'] = $news;
        return view('category', $data);
    }

    public function latestPost($lang = 'bd') {
        $news = News::orderBy('id', 'DESC')
                ->where('posts.status', '=', 3)
                ->orderBy('posts.id', 'DESC')
                ->limit(10)
                ->get();
        return $news;
    }

    public function pageComponent() {        
        $data = [  'categories' => Category::get(),
                        'cool_video' => $this->coolVideo(),
                        'tags' => Tags::get(),
                        'popular_news' => $this->popularPost(),
                        'featured_news' => $this->featuredNews(),
                        'ads' =>  $this->addBlock(),
                        'about_us' => Pages::where('slung', 'about_us')->first()
                    ];
        return $data;
    }

    public function popularPost() {
        return News::where('posts.status', '=', 3)
                                        ->limit(10)
                                        ->orderBy('posts.popular', 'DESC')
                                        ->get();
    }
    
    public function featuredNews() {
        return News::where('posts.status', '=', 3)->where('posts.featured', '=', 1)
                                ->limit(10)
                                ->orderBy('posts.id', 'DESC')
                                ->get();
    }
    
    public function coolVideo() {
        return  News::select('posts.*')
                            ->where('posts.cool_video', '=', 1)
                            ->where('posts.status', '=', 3)
                            ->orderBy('posts.id', 'DESC')
                            ->get();
    }

    public function chnageLanguage($lang = 'bd') {
        App::setLocale($lang);
        Session::put('lang', $lang);
        return Session::get('lang');
    }

    public function addBlock() {
        $show = Pages::where('slung', 'adds_settings')->first();
        if ($show->phone == 1) {
               return $show->name;
        } else {
               return "<a href=".$show->email."><img src=/storage/images/adsblock_thumb/".$show->name_en."></a>";
        }
    }
    
    public function increasePopular($id, $popular) {
        $presentView = $popular+1;
        News::where('id', $id)->update(['popular' => $presentView]);
    }
    
    public function search($lang = 'bd', Request $request) {
        $data = $this->pageComponent();
        if ($lang == 'en') {
            $data['title'] = 'Search';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'সার্চ';
            Session::put('lang', 'bd');
        }
        App::setLocale($lang);
        $news = News::select('posts.*', 'c1.id as parent_category_id', 'c1.name_bd as category_name_bd', 'c1.name_en as category_name_en', 'c1.slung_en as category_slung_en', 'c1.slung_bd as category_slung_bd', 'c2.id as main_category_id', 'c2.name_bd as main_category_name', 'c2.slung_bd as main_slung_bd', 'c2.slung_en as main_slung_en')
                                             ->join('categories as c1', 'posts.category', '=', 'c1.id')
                                             ->join('categories as c2', 'c2.id', '=', 'c1.parent_id')
                                             ->where('posts.name', 'like', "%$request->search%")
                                             ->orWhere('posts.name_en', 'like',"%$request->search%")
                                             ->orWhere('posts.description', 'like', "%$request->search%")
                                             ->orWhere('posts.description_en',  'like',"%$request->search%")
                                             ->where('posts.status', '=', 3)
                                             ->orderBy('posts.id', 'DESC')
                                             ->paginate($this->pagination);
        $data['main'] = $news;
       // return $request->search;
        return view('category', $data);
    }
}
