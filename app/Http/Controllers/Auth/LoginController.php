<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\UserPermission;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/bd';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm($lang = 'bd') {
        $data = ['title' => 'Login Form'];
        return view('auth.login', $data);
    }

    public function login(Request $request) {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            $permission = UserPermission::select('module_access_permision.id', 'module_access_permision.group_id', 'module_access_permision.module_id', 'module_access_permision.read_access', 'module_access_permision.write_access', 'module_access_permision.edit_access', 'module_access_permision.delete_access', 'm.name as module_name')
                                                                        ->leftJoin('module as m', 'module_access_permision.module_id', '=', 'm.id')
                                                                        ->where('group_id', Auth::user()->group_id)->get();
            session(['permission' => $permission]);
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout($lang = 'bd') {

        Auth::logout();
        return redirect('/');
    }

}
