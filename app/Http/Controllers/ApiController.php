<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use URL;

use Illuminate\Support\Facades\App;

class ApiController extends Controller {

    public $pagination;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
        $this->pagination = 7;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all($lang = 'bd', $limit = 50) {
    
        if ($lang == 'en') {
                $news = News::select('name_en as name', 'slung', 'created_at')
                                        ->where('status', '=', 3)
                                        ->latest()
                                        ->limit($limit)
                                        ->get();
        } else {
                $news = News::select('name', 'slung', 'created_at')
                                    ->where('status', '=', 3)
                                    ->latest()
                                    ->limit($limit)
                                    ->get();
       }
       $data = [];
       $i=0;
       foreach($news as $row){
           $data[$i] ['title']=$row->name;
           $data[$i] ['link'] = URL::to('/')."/news/".$row->slung."/$lang";
           $data[$i] ['cretedAt'] =$row->created_at;
           $i++;
       }
       return response()->json($data);
    }

   


}
