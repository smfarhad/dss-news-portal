<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\UserGroup;
use App\User;
use Session;
use Validator;
use File;
use Image;
use App\Offices;
use App\Designation;
use App\Division;
use App\District;
use App\Upozila;
class UserController extends Controller {

    private $controllerName;
    private $name;
    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'user';
         $this->name = 'UserController';
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
         
        $data = [];
        $data['title'] = 'user';
        $data['title_bd'] = 'ইউসার';
        $data['th'] = '<th>ক্রমিক</th>
                            <th> নাম </th>
                            <th> ই-মেইল </th>
                            <th> ব্যবহারকারীর ধরন </th>
                            <th> অবস্থা </th>
                            <th>সেটিংস</th>';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){
        $data['main'] = User::select('users.id', 'users.group_id', 'users.name', 'users.email', 'users.email', 'users.status',  'g.name as group_name' )
                                                ->leftJoin('user_group as g', 'users.group_id', '=', 'g.id')
                                                ->get();
        return view('admin/' . $this->controllerName . '/index', $data);
          }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
      
        $data = [];
        $data['title'] = 'user';
        $data['title_bd'] = 'ইউসার';
         $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){
        $data['group'] = UserGroup::get();
        $data['main'] = User::get();
        $data['office'] = Offices::get();
        $data['designation'] = Designation::get();
        $data['division'] = Division::get();
        $data['district'] = District::get();
        $data['thana'] = Upozila::get();
        return view('admin/' . $this->controllerName . '/create', $data);
           }else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {
        $insert = [];
        $insert['name'] = $request->name;
        $insert['group_id'] = $request->groupID;
        $insert['status'] = 1;
        $insert['email'] = $request->email;
        $insert['mobile'] = $request->mobile;
        $insert['office'] = $request->office;
        $insert['job_title'] = $request->job_title;
        $insert['division'] = $request->division;
        $insert['district'] = $request->district;
        $insert['thana'] = $request->thana;
        $insert['password'] = Hash::make($request->password);
        User::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'user';
        $data['title_bd'] = 'ইউসার';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){
        $data['group'] = UserGroup::get();
        $data['main'] = User::first();
        return view('admin/' . $this->controllerName . '/show', $data);
               }else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'user';
        $data['title_bd'] = 'ইউসার';
        $data['type'] = 'Update';
        $data['permission'] =  $this->filter();
        if($data['permission']['edit']==1){
                $data['show'] = User::where('id', $id)->first();
                $data['group'] = UserGroup::get();
                 $data['office'] = Offices::get();
                $data['designation'] = Designation::get();
                $data['division'] = Division::get();
                $data['district'] = District::get();
                $data['thana'] = Upozila::get();
                return view('admin/' . $this->controllerName . '/edit', $data);
         } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
       //return $request->all();
        
        $request->validate([
            'name' => 'min:3|max:50',
            'email' => 'email',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);
        
        $insert = [];
        $insert['name'] = $request->name;
        $insert['group_id'] = $request->groupID;
        $insert['status'] = $request->status;
        $insert['email'] = $request->email;
        $insert['mobile'] = $request->mobile;
        $insert['office'] = $request->office;
        $insert['job_title'] = $request->job_title;
        $insert['division'] = $request->division;
        $insert['district'] = $request->district;
        $insert['thana'] = $request->thana;
        $insert['password'] = Hash::make($request->password);
         
        if ($request->hasFile('feature_image_upload')) {
            $this->validate($request, [
                'feature_image_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $image = $request->file('feature_image_upload');
            $insert['profile_picture'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/profile';
            $destinationPath_thumb = storage_path() . '/images/profilethumb';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['profile_picture']);
            Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['profile_picture']);
        }else {
             $insert['profile_picture'] = $request->image_upload_edit;
        }
        //return $insert;
        $data = User::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang = 'bd', $id) {
        $data =  [];
        $data['permission'] =  $this->filter();
        if($data['permission']['delete']==1){
        User::destroy($id);
        Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
        return redirect()->back();
         } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }
  
    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }
}
