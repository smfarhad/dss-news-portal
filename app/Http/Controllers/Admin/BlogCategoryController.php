<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogCategory;
use Session;
use Validator;
use Illuminate\Support\Str;
class BlogCategoryController extends Controller {

    private $controllerName;
    private $name;
    
    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'blogcategory';
        $this->name = 'BlogCategoryController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ শ্রেণী';
        $data['th'] = '<th>ক্রমিক</th>
                                <th> প্রধান বিভাগের নাম </th>
                                <th>বিভাগ নাম</th>
                                <th>সেটিংস</th>';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){      
            $data['main'] = BlogCategory::select('blog_categories.id', 'blog_categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 'blog_categories.name_bd', 'blog_categories.name_en')
                    ->leftJoin('blog_categories as c', 'blog_categories.parent_id', '=', 'c.id')
                    ->get();
            return view('admin/' . $this->controllerName . '/index', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ শ্রেণী';
        $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){
            $data['main'] = BlogCategory::where('parent_id', 0)->get();
            return view('admin/' . $this->controllerName . '/create', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {
        
        $insert = [];
        if ($request->parent_id == null) {
            $insert['parent_id'] = 0;
        } else {
            $insert['parent_id'] = $request->parent_id;
        }
        $insert['name_bd'] = $request->name_bd;
        $insert['name_en'] = $request->name_en;
        $insert['slung_bd'] = str_slug($request->name_bd, "-");
        $insert['slung_en'] = str_slug($request->name_en, "-");
         if (strlen($insert['slung_bd']) == 0) {
            $insert['slung_bd'] = Str::uuid();
        }
        BlogCategory::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ শ্রেণী';
        $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){
        $data['main'] = BlogCategory::where('parent_id', 0)->get();
        $data['show'] = BlogCategory::select('blog_categories.id', 'blog_categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 'blog_categories.name_bd', 'blog_categories.name_en', 'blog_categories.created_at')
                ->leftJoin('blog_categories as c', 'blog_categories.parent_id', '=', 'c.id')
                ->where('blog_categories.id', $id)
                ->first();
        return view('admin/' . $this->controllerName . '/show', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
      
        $data = [];
        $data['title']        = $this->controllerName;
        $data['title_bd']   = 'ব্লগ শ্রেণী';
        $data['type']       = 'Update';
         $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){
        $data['main']      = BlogCategory::where('parent_id', 0)->get();
        $data['show']      = BlogCategory::select('blog_categories.id', 'blog_categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 
                                                        'blog_categories.name_bd', 'blog_categories.name_en', 'blog_categories.created_at')
                                                        ->leftJoin('blog_categories as c', 'blog_categories.parent_id', '=', 'c.id')
                                                        ->where('blog_categories.id', $id)
                                                        ->first();
        return view('admin/' . $this->controllerName . '/edit', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
        $insert = [];
        if ($request->parent_id == null) {
            $insert['parent_id'] = 0;
        } else {
            $insert['parent_id'] = $request->parent_id;
        }
        $insert['name_bd'] = $request->name_bd;
        $insert['name_en'] = $request->name_en;
        $insert['slung_bd'] = str_slug($request->name_bd, "-");
        $insert['slung_en'] = str_slug($request->name_en, "-");
        if (strlen($insert['slung_bd']) == 0) {
            $insert['slung_bd'] = Str::uuid();
        }
        $data = BlogCategory::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        //return back()->withInput($request->input());
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($lang = 'bd', $id) {
                $data = [];
                $data['permission'] =  $this->filter();
                if($data['permission']['write']==1){
                        BlogCategory::destroy($id);
                        Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
                        return redirect()->back();
                }else{
                        return redirect()->to(route('home', 'bd'));
                        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
                }
        }
        public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

}
