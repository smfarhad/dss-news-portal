<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\News;
use App\Status;
use App\Tags;
use Session;
use File;
use Image;
use Validator;
use Auth;
use Illuminate\Support\Str;

class NewsController extends Controller {

    private $controllerName;
    private $name;

    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'news';
        $this->name = 'NewsController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        $data['th'] = '<th>ক্রমিক</th>
                            <th>চিত্র </th>
                            <th>  নাম </th>
                            <th>  ক্যাটাগরি </th>
                            <th>  ট্যাগ </th>
                           <th> আলোচিত সংবাদ </th>
                            </th><th> তারিখ </th>
                            </th><th> অথর</th>
                            <th style="width:100px;" >সেটিংস</th>';
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            if (Auth::user()->group_id == 1 || Auth::user()->group_id == 2) {
                 
                  $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_bd', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                       // ->where('posts.created_by', '=', Auth::user()->id)
                            ->where('posts.status', '<=',2)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            } else {
                $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_en', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.created_by', '=', Auth::user()->id)
                        ->where('posts.status', '<=',2)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            }
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
        //return $data['main'] ;
        return view('admin/' . $this->controllerName . '/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        $data['main'] = Category::get();
        $data['permission'] = $this->filter();
        if ($data['permission']['write'] == 1) {
            $data['news'] = News::where('language', 2)->get();
            $data['tags'] = Tags::get();
            return view('admin/' . $this->controllerName . '/create', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {
        $insert = [];
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['slung'] = str_slug($request->name, "-");
        $insert['video'] = $request->video;
        $insert['description'] = $request->description;
        $insert['photographer'] = $request->photographer;
        $insert['description_en'] = $request->description_en;
        $insert['category'] = $request->category;
        $insert['featured'] = $request->featured;
        $insert['cool_video'] = $request->coolVideo;
        $insert['tag'] = $request->tags;
        $insert['language'] = $request->language;
        $insert['status'] = $request->status;
        $insert['created_by'] = Auth::id();

        if (strlen($insert['slung']) == 0) {
            str_slug($request->name, "-");
            $insert['slung'] = Str::uuid();
        }

        if ($request->hasFile('feature_image_upload')) {
            $this->validate($request, [
                'feature_image_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);
            $image = $request->file('feature_image_upload');
            $insert['image'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/news';
            $destinationPath_thumb = storage_path() . '/images/news_thumb';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(350, 250, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['image']);
            $img->resize(null, 430, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $insert['image']);
            // Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['image']);
        }

        if ($request->hasFile('gallery_image_upload')) {
            $images = $request->file('gallery_image_upload');
            $i = 0;
            $newImage = [];
            foreach ($images as $image) {
                $newImage[$i] = md5(time()) . '_' . $i . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path() . '/images/news';
                $destinationPath_thumb = storage_path() . '/images/news_thumb';

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0700, true);
                }
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath_thumb, 0700, true);
                }
                $img = Image::make($image->getRealPath());
                $img->resize(350, 250, function ($constraint) {
                    //$constraint->aspectRatio();
                })->save($destinationPath_thumb . '/' . $newImage[$i]);
                $img->resize(null, 430, function ($constraint) {
                    //$constraint->aspectRatio();
                })->save($destinationPath_thumb . '/' . $newImage[$i]);
                //Image::make($image->getRealPath())->save($destinationPath . '/' . $newImage[$i] );
                $i++;
            }
            $insert['image_gallery'] = json_encode($newImage);
        }
        if (strlen($insert['slung']) == 0) {
            $insert['slung'] = Str::uuid();
        }

        News::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {

        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'ব্লগ';
        //$data['main'] = Category::where('parent_id', 0)->get();
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            $data['show'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en')
                    ->where('posts.id', $id)
                    ->leftJoin('categories as c', 'posts.category', '=', 'c.id')
                    ->first();
            return view('admin/' . $this->controllerName . '/show', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        $data['type'] = 'Update';
        $data['main'] = Category::get();
        $data['news'] = News::where('language', 2)->get();
        $data['tags'] = Tags::get();
        $data['permission'] = $this->filter();
        $data['status'] = Status::orderBy('order', 'asc')->get();

        if ($data['permission']['edit'] == 1) {
            $data['show'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en')
                    ->where('posts.id', $id)
                    ->leftJoin('categories as c', 'posts.category', '=', 'c.id')
                    ->first();
            return view('admin/' . $this->controllerName . '/edit', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
        $insert = [];
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['photographer'] = $request->photographer;
        $insert['slung'] = str_slug($request->name, "-");
        $insert['video'] = $request->video;
        $insert['description'] = $request->description;
        $insert['description_en'] = $request->description_en;
        $insert['category'] = $request->category;
        $insert['featured'] = $request->featured;
        $insert['cool_video'] = $request->coolVideo;
        $insert['tag'] = $request->tags;
        $insert['language'] = $request->language;
        $insert['status'] = $request->status;
        $insert['created_by'] = Auth::id();

        if (strlen($insert['slung']) == 0) {
            $insert['slung'] = Str::uuid();
        }
        if ($request->hasFile('feature_image_upload')) {
            $this->validate($request, [
                'feature_image_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);
            $image = $request->file('feature_image_upload');
            $insert['image'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/news';
            $destinationPath_thumb = storage_path() . '/images/news_thumb';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(350, 250, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['image']);
            $img->resize(null, 430, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $insert['image']);
            // Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['image']);
        } else {
            $insert['image'] = $request->image_upload_edit;
        }

        if ($request->hasFile('gallery_image_upload')) {
            $images = $request->file('gallery_image_upload');
            $i = 0;
            $newImage = [];
            foreach ($images as $image) {
                $newImage[$i] = md5(time()) . '_' . $i . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path() . '/images/news';
                $destinationPath_thumb = storage_path() . '/images/news_thumb';

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0700, true);
                }
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath_thumb, 0700, true);
                }
                $img = Image::make($image->getRealPath());
                $img->resize(350, 250, function ($constraint) {
                    //$constraint->aspectRatio();
                })->save($destinationPath_thumb . '/' . $newImage[$i]);
                $img->resize(null, 430, function ($constraint) {
                    //$constraint->aspectRatio();
                })->save($destinationPath_thumb . '/' . $newImage[$i]);
                // Image::make($image->getRealPath())->save($destinationPath . '/' . $newImage[$i] );
                $i++;
            }
            $insert['image_gallery'] = json_encode($newImage);
        } else {
            $insert['image_gallery'] = $request->gallery_image_upload_edit;
        }

        $data = News::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");

        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang = 'bd', $id) {
        $data = [];
        $data['permission'] = $this->filter();
        if ($data['permission']['delete'] == 1) {
            News::destroy($id);
            Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
            return redirect()->back();
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    // test 
    public function ajaxShow($lang = 'bd', $id) {
        return News::where('id', $id)->first();
    }

    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

    public function published($lang = 'bd') {

        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        $data['th'] = '<th>ক্রমিক</th>
                            <th>চিত্র </th>
                            <th>  নাম </th>
                            <th>  ক্যাটাগরি </th>
                            <th>  ট্যাগ </th>
                           <th> আলোচিত সংবাদ </th>
                            </th><th> তারিখ </th>
                            </th><th> অথর</th>
                            <th style="width:100px;" >সেটিংস</th>';
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            if (Auth::user()->group_id == 1 || Auth::user()->group_id == 2) {
                $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_bd', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.status', '=', 3)
                      //  ->orWhere('posts.created_by', '=', Auth::user()->id)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            } else {
                 $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_en', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.created_by', '=', Auth::user()->id)
                        ->where('posts.status', '=', 3)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            }
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
        //return $data['main'] ;
        return view('admin/' . $this->controllerName . '/index', $data);
    }
      public function rejectedNews($lang = 'bd') {

        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        $data['th'] = '<th>ক্রমিক</th>
                            <th>চিত্র </th>
                            <th>  নাম </th>
                            <th>  ক্যাটাগরি </th>
                            <th>  ট্যাগ </th>
                           <th> আলোচিত সংবাদ </th>
                            </th><th> তারিখ </th>
                            </th><th> অথর</th>
                            <th style="width:100px;" >সেটিংস</th>';
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            if (Auth::user()->group_id == 1 || Auth::user()->group_id == 2) {
                $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_bd', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.status', '=', 4)
                        //->orWhere('posts.created_by', '=', Auth::user()->id)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            } else {
             //   return  Auth::user()->id;
                 $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by', 't.name as tag_name_en', 't.name_en as tag_name_en')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('tags as t', 't.id', '=', 'posts.tag')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_by')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.created_by', '=', Auth::user()->id)
                        ->where('posts.status', '=', 4)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
            }
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
        //return $data['main'] ;
        return view('admin/' . $this->controllerName . '/index', $data);
    }
}
