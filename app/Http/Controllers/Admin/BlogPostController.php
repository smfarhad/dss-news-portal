<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogCategory;
use App\BlogPost;
use App\Status;
use App\BlogTags;
use Session;
use File;
use Image;
use Validator;
use Auth;
use Illuminate\Support\Str;

class BlogPostController extends Controller {

    private $controllerName;
    private $name;

    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'blogpost';
        $this->name = 'BlogPostController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ পোস্ট';
        $data['th'] = "<th>ক্রমিক</th>
                                <th>চিত্র </th>     
                                <th>নাম </th>
                                <th> ক্যাটাগরি </th>     
                                <th>  ট্যাগ </th>
                                <th> আলোচিত সংবাদ </th>
                                <th> তারিখ </th>
                                <th> অথর</th>
                                <th style='width:100px;'>সেটিংস</th>";
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            if (Auth::user()->group_id == 1 || Auth::user()->group_id == 2) {
                $data['main'] = BlogPost::select('blog_posts.*', 's.name_alt','c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt', 'u.name as created_by')
                        ->join('blog_categories as c', 'c.id', '=', 'blog_posts.category')
                         ->leftJoin('users as u', 'u.id','=','blog_posts.created_by')
                        ->leftJoin('status as s', 's.id', '=', 'blog_posts.status')
                        ->where('blog_posts.status', '>', 1)
                        ->orWhere('blog_posts.created_by', '=', Auth::user()->id)
                        ->groupBy('blog_posts.id')
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('blog_posts.id', 'DESC')
                        ->get();
            } else {
                $data['main'] = BlogPost::select('blog_posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt')
                        ->join('blog_categories as c', 'c.id', '=', 'blog_posts.category')
                         ->leftJoin('users as u', 'u.id','=','blog_posts.created_by')
                        ->leftJoin('status as s', 's.id', '=', 'blog_posts.status')
                        ->orWhere('blog_posts.created_by', '=', Auth::user()->id)
                        ->groupBy('blog_posts.id')
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('blog_posts.id', 'DESC')
                        ->get();
            }
            return view('admin/' . $this->controllerName . '/index', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ';
        $data['main'] = BlogCategory::get();
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            $data['news'] = BlogPost::where('language', 2)->get();
            $data['tags'] = BlogTags::get();
            return view('admin/' . $this->controllerName . '/create', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {

        $insert = [];
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['photographer'] = $request->photographer;
        $insert['slung'] = str_slug($request->name, '-');
        $insert['description'] = $request->description;
        $insert['description_en'] = $request->description_en;
        $insert['category'] = $request->category;
        $insert['featured'] = $request->featured;
        $insert['tag'] = $request->tags;
        $insert['language'] = $request->language;
        $insert['created_by'] = Auth::id();
        $insert['status'] = $request->status;
        $serviceThumb = 'feature_image_upload';
        if ($request->hasFile($serviceThumb)) {
            $this->validate($request, [
                'feature_image_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);
            $image = $request->file('feature_image_upload');
            $insert['image'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/blog';
            $destinationPath_thumb = storage_path() . '/images/blog_thumb';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(350, 250, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['image']);
             $img->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $insert['image']);
            //Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['image']);
        }

        if (strlen($insert['slung']) == 0) {
            $insert['slung'] = Str::uuid();
        }
        BlogPost::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ';
        $data['permission'] = $this->filter();

        if ($data['permission']['read'] == 1) {
            $data['show'] = BlogPost::select('blog_posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en')
                    ->where('blog_posts.id', $id)
                    ->leftJoin('blog_categories as c', 'blog_posts.category', '=', 'c.id')
                    ->first();
            return view('admin/' . $this->controllerName . '/show', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'ব্লগ';
        $data['type'] = 'Update';
        $data['main'] = BlogCategory::get();
        $data['news'] = BlogPost::where('language', 2)->get();
        $data['tags'] = BlogTags::get();
        $data['status'] = Status::orderBy('order', 'asc')->get();
        $data['permission'] = $this->filter();
        if ($data['permission']['edit'] == 1) {
            $data['show'] = BlogPost::select('blog_posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en')
                    ->where('blog_posts.id', $id)
                    ->leftJoin('blog_categories as c', 'blog_posts.category', '=', 'c.id')
                    ->first();
            return view('admin/' . $this->controllerName . '/edit', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
       // return $request->all();
        $insert = [];        
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['photographer'] = $request->photographer;
        $insert['slung'] = str_slug($request->name, '-');
        $insert['description'] = $request->description;
        $insert['description_en'] = $request->description_en;
        $insert['category'] = $request->category;
        $insert['featured'] = $request->featured;
        $insert['tag'] = $request->tags;
        $insert['language'] = $request->language;
        $insert['created_by'] = Auth::id();
        $insert['status'] = $request->status;
        $serviceThumb = 'feature_image_upload';
        if (strlen($insert['slung']) == 0) {
            $insert['slung'] = Str::uuid();
        }
        if ($request->hasFile($serviceThumb)) {
            $this->validate($request, [
                'feature_image_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            ]);
            $image = $request->file('feature_image_upload');
            $insert['image'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/blog';
            $destinationPath_thumb = storage_path() . '/images/blog_thumb';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(350, 250, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['image']);
            $img->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $insert['image']);
           // Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['image']);
        }

        $data = BlogPost::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        //return back()->withInput($request->input());
        //return $insert;
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang = 'bd', $id) {
        $data = [];
        $data['permission'] = $this->filter();
        if ($data['permission']['edit'] == 1) {
            BlogPost::destroy($id);
            Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
            return redirect()->back();
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    public function ajaxShow($lang = 'bd', $id) {
        return BlogPost::where('id', $id)->first();
    }

    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

}
