<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Str;

class CategoryController extends Controller {

    private $controllerName;
    private $name;

    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'category';
        $this->name = 'CategoryController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {

            $data['title'] = 'category';
            $data['title_bd'] = 'ক্যাটাগরি';
            $data['th'] = '<th>ক্রমিক</th>
                                    <th> প্রধান বিভাগের নাম </th>
                                    <th>বিভাগ নাম</th>
                                    <th style="width:90px;" >সেটিংস</th>';
            $data['main'] = Category::select('categories.id', 'categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 'categories.name_bd', 'categories.name_en')
                    ->leftJoin('categories as c', 'categories.parent_id', '=', 'c.id')
                    ->get();
            return view('admin/' . $this->controllerName . '/index', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        $data = [];
        $data['title'] = 'category';
        $data['title_bd'] = 'বার্তা ক্যাটাগরি';
        $data['permission'] = $this->filter();
        if ($data['permission']['write'] == 1) {
            $data['main'] = Category::where('parent_id', 0)->get();
            return view('admin/' . $this->controllerName . '/create', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {
        $insert = [];
        if ($request->parent_id == null) {
            $insert['parent_id'] = 0;
        } else {
            $insert['parent_id'] = $request->parent_id;
        }
        $insert['name_bd'] = $request->name_bd;
        $insert['name_en'] = $request->name_en;
        $insert['slung_bd'] = str_slug($request->name_bd, '-');
        $insert['slung_en'] = str_slug($request->name_en, "-");
        if (strlen($insert['slung_bd']) == 0) {
            $insert['slung_bd'] = Str::uuid();
        }
        
        Category::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'category';
        $data['title_bd'] = 'ক্যাটাগরি';
        $data['permission'] = $this->filter();
        if ($data['permission']['read'] == 1) {
            $data['main'] = Category::where('parent_id', 0)->get();
            $data['show'] = Category::select('categories.id', 'categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 'categories.name_bd', 'categories.name_en', 'categories.created_at')
                    ->leftJoin('categories as c', 'categories.parent_id', '=', 'c.id')
                    ->where('categories.id', $id)
                    ->first();
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'category';
        $data['title_bd'] = 'ক্যাটাগরি';
        $data['type'] = 'Update';
        $data['permission'] = $this->filter();
        if ($data['permission']['edit'] == 1) {
            $data['main'] = Category::where('parent_id', 0)->get();
            $data['show'] = Category::select('categories.id', 'categories.parent_id', 'c.name_bd as parent_name_bd', 'c.name_en as parent_name_en', 'categories.name_bd', 'categories.name_en', 'categories.created_at')
                    ->leftJoin('categories as c', 'categories.parent_id', '=', 'c.id')
                    ->where('categories.id', $id)
                    ->first();
            return view('admin/' . $this->controllerName . '/edit', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
        $insert = [];
        if ($request->parent_id == null) {
            $insert['parent_id'] = 0;
        } else {
            $insert['parent_id'] = $request->parent_id;
        }
        $insert['name_bd'] = $request->name_bd;
        $insert['name_en'] = $request->name_en;
        $insert['slung_bd'] = str_slug($request->name_bd, "-");
        $insert['slung_en'] = str_slug($request->name_en, "-");
        if (strlen($insert['slung_bd']) == 0) {
            $insert['slung_bd'] = Str::uuid();
        }
        
        

        //update query;
        $data = Category::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        //return back()->withInput($request->input());
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang = 'bd', $id) {
        $data = [];
        $data['permission'] = $this->filter();
        if ($data['permission']['delete'] == 1) {
            Category::destroy($id);
            Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
            return redirect()->back();
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

}
