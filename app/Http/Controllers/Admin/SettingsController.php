<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notices;
use App\Pages;
use Validator;
use Session;
use Image;
use Auth;
use File;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller {

        private $controllerName;
        private $name;

        public function __construct() {
            $this->middleware('auth');
            $this->controllerName = 'SettingsController';
            $this->name = 'settings';
        }
    
        public function addBlock($lang = 'bd') {
        $data = [];
        $data['title'] = 'Settings';
        $data['title_bd'] = 'সেটিংস';
        $data['permission'] = $this->filter();
        $data['add_img'] = '/storage/images/adsblock/';
        $data['add_img_thumb'] = '/storage/images/adsblock_thumb/';
        if ($data['permission']['read'] == 1) {
            $data['show'] = Pages::where('slung', 'adds_settings')->first();
            return view('admin/' . 'settings' . '/edit', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

        public function addBlockUpdate($lang = 'bd', Request $request) {
        
        $insert = [];
        $insert['name']         = $request->embedCode;
        $insert['email']         = $request->link;
        $insert['phone']       = $request->addblock;
         if ($request->hasFile('adsImage')) {
          
            $this->validate($request, [
                'adsImage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $image = $request->file('adsImage');
            $insert['name_en'] = md5(time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path() . '/storage/images/adsblock';
            $destinationPath_thumb = storage_path() . '/storage/images/adsblock_thumb';
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            $img = Image::make($image->getRealPath());
            $img->resize(150, 150, function ($constraint) {
                //$constraint->aspectRatio();
            })->save($destinationPath_thumb . '/' . $insert['name_en']);
            Image::make($image->getRealPath())->save($destinationPath . '/' . $insert['name_en']);
        }else{
             
             $insert['name_en']    =$request->adsImage_edit;
        }
        //return $insert;
        $data = Pages::where('slung', 'adds_settings')->update($insert);
        
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        return redirect()->to(route('admin_addblock', 'bd'));
    }
    
        public function aboutUs($lang = 'bd') {
                $data = [];
                $data['title'] = 'About Us';
                $data['title_bd'] = 'আমাদের সম্পর্কে';
                $data['permission'] = $this->filter();
                $data['add_img'] = '/storage/images/adsblock/';
                $data['add_img_thumb'] = '/storage/images/adsblock_thumb/';
                if ($data['permission']['read'] == 1) {
                    $data['show'] = Pages::where('slung', 'about_us')->first();
                    return view('admin/' . 'settings' . '/about_us', $data);
                } else {
                    return redirect()->to(route('home', 'bd'));
                    Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
                }
        }

        public function aboutUsUpdate($lang = 'bd', Request $request) {
                $insert = [];
                $insert['name'] = $request->name;
                $insert['name_en'] = $request->name_en;
                $insert['description'] = $request->description;
                $insert['description_en'] = $request->description_en;
               // return $insert;
                $data = Pages::where('slung', 'about_us')->update($insert);
                Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
                return redirect()->to(route('aboutus', 'bd'));
    }

        public function filter() {
            $permit = [];
            $permission = session()->get('permission');
            foreach ($permission as $row) {
                if ($row->module_name == $this->controllerName) {
                    $permit['read'] = $row->read_access;
                    $permit['write'] = $row->write_access;
                    $permit['edit'] = $row->write_access;
                    $permit['delete'] = $row->delete_access;
                }
            }
            return $permit;
        }

}
