<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use Validator;
use Session;
use Auth;

class ContactController extends Controller {

    private $controllerName;
    private $name;
    
    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'contact';
        $this->name = 'ContactController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] =  'যোগাযোগ';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){   
            $data['show'] = Pages::where('slung', 'contact')->first();
            return view('admin/' . $this->controllerName . '/contact', $data);
        }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request) {
        $insert = [];
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['email'] = $request->email;
        $insert['phone'] = $request->phone;
        $insert['address'] = $request->address;
        $insert['description'] = $request->description;
        $insert['description_en'] = $request->description_en;

        $data = Pages::where('slung', 'contact')->update($insert);        
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        return redirect()->to(route( 'admin_contact', 'bd'));
    }
    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

}
