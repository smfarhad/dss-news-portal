<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserGroup;
use App\UserPermission;
use App\Module;
use Session;
use Validator;

class UserPermissionController extends Controller {

    private $controllerName;
    private $name;
    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'userpermission';
        $this->name = 'UserPermissionController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = 'userpermission';
        $data['title_bd'] = 'ইউজার পারমিশন ';
        $data['th'] = '<th>ক্রমিক</th>
                           <th> মডিউল </th>         
                            <th> ইউজার দল </th>                                           
                            <th>দেখার পারমিশন </th>
                            <th>লেখার পারমিশন </th>
                            <th>পরিবর্তন পারমিশন </th>
                            <th>মুছে ফেলার পারমিশন </th>
                             <th style="width:90px;">সেটিংস</th>';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){ 
        $data['main'] = UserPermission::select('module_access_permision.*', 'g.name as group_name', 'm.name as module_name')
                ->leftJoin('user_group as g', 'module_access_permision.group_id', '=', 'g.id')
                ->leftJoin('module as m', 'module_access_permision.module_id', '=', 'm.id')
                ->get();
        return view('admin/' . $this->controllerName . '/index', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        $data = [];
        $data['title'] = 'userpermission';
        $data['title_bd'] = 'ইউজার পারমিশন';
         $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){       
        $data['userGroup'] = UserGroup::get();
        $data['module'] = Module::get();
        return view('admin/' . $this->controllerName . '/create', $data);
          }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {
        $insert = [];
        $insert['group_id'] = $request->userGroup;
        $insert['module_id'] = $request->module;
        $insert['read_access'] = $request->read;
        $insert['write_access'] = $request->write;
        $insert['edit_access'] = $request->edit;
        $insert['delete_access'] = $request->delete;
        UserPermission::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'userpermission';
        $data['title_bd'] = 'ইউজার পারমিশন';
         $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){       
        $data['show'] = UserPermission::select('module_access_permision.*', 'g.name as group_name', 'm.name as module_name')
                ->leftJoin('user_group as g', 'module_access_permision.group_id', '=', 'g.id')
                ->leftJoin('module as m', 'module_access_permision.module_id', '=', 'm.id')
                ->first();
        return view('admin/' . $this->controllerName . '/show', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = 'userpermission';
        $data['title_bd'] = 'ইউজার পারমিশন';
        $data['type'] = 'Update';
         $data['permission'] =  $this->filter();
        if($data['permission']['edit']==1){       
        $data['userGroup'] = UserGroup::get();
        $data['module'] = Module::get();
        $data['show'] = UserPermission::where('id', $id)->first();
        return view('admin/' . $this->controllerName . '/edit', $data);
         }else{
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
        $insert = [];
        $insert['group_id'] = $request->userGroup;
        $insert['module_id'] = $request->module;
        $insert['read_access'] = $request->read;
        $insert['write_access'] = $request->write;
        $insert['edit_access'] = $request->edit;
        $insert['delete_access'] = $request->delete;
        $data = UserPermission::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        //return back()->withInput($request->input());
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($lang = 'bd', $id) {
            $data=[];
            $data['permission'] =  $this->filter();
            if($data['permission']['delete']==1){       
            UserPermission::destroy($id);
            Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
            return redirect()->back();
             }else{
                return redirect()->to(route('home', 'bd'));
                Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
            }
        }

        public function bulkAdd($lang = 'bd') {
            $data = [];
            $data['title'] = 'userpermission';
            $data['title_bd'] = 'ইউজার পারমিশন';
            $data['userGroup'] = UserGroup::get();
            $data['module'] = Module::get();
            $data['permission'] =  $this->filter();
            if($data['permission']['write']==1){
                    return view('admin/' . $this->controllerName . '/bulk_create', $data);
             }else{
                return redirect()->to(route('home', 'bd'));
                Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
            }
        }

        public function bulkAddPost($lang = 'bd', Request $request) {
            $permission = $request->all();
            $input = [];
            $module = [];
            foreach ($permission['module'] as $key => $val) {
                $input[$val]['group_id'] = $request->userGroup;
                $input[$val]['module_id'] = $val;
                $input[$val]['read_access'] = 0;
                $input[$val]['write_access'] = 0;
                $input[$val]['edit_access'] = 0;
                $input[$val]['delete_access'] = 0;

                if (isset($permission['read'])) {
                    foreach ($permission['read'] as $key => $val) {
                        $input[$val]['read_access'] = 1;
                    }
                }
                if (isset($permission['write'])) {
                    foreach ($permission['write'] as $key => $val) {
                        $input[$val]['write_access'] = 1;
                    }
                }
                if (isset($permission['edit'])) {
                    foreach ($permission['edit'] as $key => $val) {
                        $input[$val]['edit_access'] = 1;
                    }
                }
                if (isset($permission['delete'])) {
                    foreach ($permission['delete'] as $key => $val) {
                        $input[$val]['delete_access'] = 1;
                    }
                }
            }
            UserPermission::where('group_id', $request->userGroup)->delete();
            $data = array();
            $data['title'] = 'All User Permission List';
            $data['list'] = 'Dashboard';
            $data['add'] = 'Add New Users Permission';
            $user = UserPermission::insert(array_values($input));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
            return back()->withInput($request->input())->with('disc_add', 1);
        }
       
        public function filter() {
                $permit = [];
                $permission = session()->get('permission');
                foreach ($permission as $row) {
                    if ($row->module_name == $this->name) {
                        $permit['read'] = $row->read_access;
                        $permit['write'] = $row->write_access;
                        $permit['edit'] = $row->write_access;
                        $permit['delete'] = $row->delete_access;
                    }
                }
                return $permit;
            }
}
