<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notices;
use App\Pages;
use Validator;
use Session;
use Image;
use Auth;
use File;
use Illuminate\Support\Facades\Storage;
use App\Division;
class DivisionController extends Controller {

    private $controllerName;
    private $name;
    public function __construct() {
        $this->middleware('auth');
        $this->controllerName = 'division';
        $this->name = 'DivisionController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
     
        $data = [];
        $data['title'] = $this->controllerName;
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){ 
        $data['title_bd'] = 'বিভাগ';
        $data['th'] = "<th>ক্রমিক</th>
                            <th>   ইংরেজি নাম </th>
                            <th>   বাংলা নাম </th>
                            <th style='width:100px;'>সেটিংস</th>";
        $data['main'] = Division::get();
        return view('admin/' . $this->controllerName . '/index', $data);
        }else{
            return redirect()->to(route('home', 'bd'));
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang = 'bd') {
        
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] =  'বিভাগ';
        $data['permission'] =  $this->filter();
        if($data['permission']['write']==1){
                return view('admin/' . $this->controllerName . '/create', $data);
         }else {
                return redirect()->to(route('home', 'bd'));
                Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang = 'bd', Request $request) {

        $insert = [];
        $insert['name_en']         = $request->name_bd;
        $insert['name_bd']    = $request->name_en;
        Division::insert($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Inserted Successfully </span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang = 'bd', $id) {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] = 'বিভাগ';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1){
            $data['show'] = Division::where('id', $id)->first();
            return view('admin/' . $this->controllerName . '/show', $data);
         }else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang = 'bd', $id) {
        $data = [];
        $data['title'] = $this->controllerName;
        $data['title_bd'] =  'বিভাগ';
        $data['type'] = 'Update';
        $data['permission'] =  $this->filter();
        if($data['permission']['edit']==1){
            $data['show'] = Division::where('id', $id)->first();
            return view('admin/' . $this->controllerName . '/edit', $data);
        } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang = 'bd', Request $request, $id) {
        $insert = [];
        $insert['name_en']     = $request->name_bd;
        $insert['name_bd']    = $request->name_en;
        $data = Division::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        return redirect()->to(route($this->controllerName . '.index', 'bd'));  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang = 'bd', $id) {
        $data =[];
        $data['permission'] =  $this->filter();
        if($data['permission']['delete']==1) {
        Division::destroy($id);
        Session::flash('success', "<span class=text-red> &nbsp; &nbsp; Data Deleted Successfully </span>");
        return redirect()->back();
         } else {
            return redirect()->to(route('home', 'bd'));
            Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
        
    }

    public function appointment($lang = 'bd') {
        $data = [];
        $data['title'] = 'appointments';
        $data['title_bd'] =  'এপয়েন্টমেন্ট';
        $data['permission'] =  $this->filter();
        if($data['permission']['read']==1) {
                $data['show'] = Pages::where('slung', 'appointments')->first();
                return view('admin/' . 'appointments' . '/appointment', $data);
        } else {
                return redirect()->to(route('home', 'bd'));
                Session::flash('success', "<span class=text-green> &nbsp; &nbsp; You do not have permission to visit this page </span>");
        }
    }

    public function appointmentUpdate($lang = 'bd', Request $request) {
        
        $insert = [];
        $insert['name'] = $request->name;
        $insert['name_en'] = $request->name_en;
        $insert['description'] = $request->description;
        $insert['description_en'] = $request->description_en;
        $data = Pages::where('slung', 'appointments')->update($insert);
        
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        //return back()->withInput($request->input());
        return redirect()->to(route( 'admin_appointment', 'bd'));
    }
        public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

}
