<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Status;
use App\News;
use App\User;
use Session;
use Image;
use App\UserPermission;
use Auth;
use App\Offices;
use App\Designation;
use App\Division;
use App\District;
use App\Upozila;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller {
    /* Controller Name */

    private $name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->name = 'HomeController';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'bd') {
        $data = [];
        $data['title'] = 'news';
        $data['title_bd'] = 'খবর';
        if ($lang == 'bd') {
            Session::put('lang', $lang);
        } else {
            Session::put('lang', 'bd');
        }

        $data['access'] = $this->filter();
        $data['status'] = Status::get();
        $data['permission'] = $this->filter();
        $data['th'] = '<th>ক্রমিক</th>
                            <th>চিত্র </th>     
                            <th> ভাষা </th>     
                            <th> শ্রেণী </th>     
                            <th>  নাম </th>
                            <th> সর্বোচ্চ পাঠিত </th>
                            <th> আলোচিত সংবাদ </th>
                            <th style="width:100px;" >সেটিংস</th>';
        if (Auth::user()->group_id == 1 || Auth::user()->group_id == 2) {
             $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_at')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.status', '>', 1)
                        ->orWhere('posts.created_by','=', Auth::user()->id)
                        ->orderBy('s.name_alt', 'DESC')
                       ->orderBy('posts.id', 'DESC')
                    ->get();
        } else {
            $data['main'] = News::select('posts.*', 'c.name_bd as category_name_bd', 'c.name_en as category_name_en', 's.name as status_name', 's.name_alt as status_name_alt')
                        ->groupBy('posts.id')
                        ->join('categories as c', 'c.id', '=', 'posts.category')
                        ->leftJoin('users as u', 'u.id', '=', 'posts.created_at')
                        ->leftJoin('status as s', 's.id', '=', 'posts.status')
                        ->where('posts.created_by', '=', Auth::user()->id)
                        ->where('posts.status', '<=', 2)
                        ->orderBy('s.name_alt', 'DESC')
                        ->orderBy('posts.id', 'DESC')
                        ->get();
        }
        return view('admin/dashboard', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile($lang = 'bd') {
        $data = [];
        $data['title'] = 'appointments';
        $data['title_bd'] = 'এপয়েন্টমেন্ট';
        $data['show'] = Auth::user();
        $data['office'] = Offices::get();
        $data['designation'] = Designation::get();
        $data['division'] = Division::get();
        $data['district'] = District::get();
        $data['thana'] = Upozila::get();
        return view('admin/' . 'profile', $data);
    }

    public function filter() {
        $permit = [];
        $permission = session()->get('permission');
        foreach ($permission as $row) {
            if ($row->module_name == $this->name) {
                $permit['read'] = $row->read_access;
                $permit['write'] = $row->write_access;
                $permit['edit'] = $row->write_access;
                $permit['delete'] = $row->delete_access;
            }
        }
        return $permit;
    }

    public function CrooperJsGet($lang = 'bd') {
       
        if ($lang == 'en') {
            $data['title'] = 'Home';
            Session::put('lang', $lang);
        } else {
            $data['title'] = 'খবর';
            Session::put('lang', 'bd');
        }
        $data['title_bd'] = 'খবর';
        return view('admin/' . 'croop', $data);
    }

    public function crooperJsPost($lang = 'bd', Request $request) {
        $browseImg = 'browseImg';
        if ($request->hasFile($browseImg)) {
            $width = $request->width[0];
            $height = $request->height[0];
            $x = $request->x[0];
            $y = $request->y[0];
            $image = $request->file($browseImg);
           $this->validate($request, [
                'browseImg' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $insertImage = md5(time()) . '.' . $image->getClientOriginalExtension();
            //$insert['image_thumb'] = md5(time()) . '_thumb.' . $image->getClientOriginalExtension();
            // path where to save
            $destinationPath = storage_path() . '/images/croop';
            $destinationPath_thumb = storage_path() . '/images/croops';

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0700, true);
            }
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath_thumb, 0700, true);
            }
            //return $image->getRealPath();
            $img = Image::make($image->getRealPath());
            $img->crop(intval($width), intval($height), intval($x), intval($y))->save($destinationPath_thumb . '/' . $insertImage);
            //Image::make($image->getRealPath())->save($destinationPath . '/' . $insertImage);
        }
    }

    public function changePasswordAdmin($lang = 'bd', Request $request, $id) {
           $request->validate([
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);
        $insert = [];
        $insert['password'] = Hash::make($request->password);
        $data = User::findOrFail($id);
        $data->update($insert);
        Session::flash('success', "<span class=text-green> &nbsp; &nbsp; Data Updated Successfully</span>");
        return redirect()->to(route('user.edit', ['lang'=>'bd', 'id'=>Auth::user()->id]));
    }
}
