<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
      use Notifiable;
    // table name
    protected $table = 'tag';
  
}
