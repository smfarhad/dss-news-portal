<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
      use Notifiable;
      
    // table name
    protected $table = 'blog_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       protected $fillable = ['id','status','name','name_en','photographer','slung','featured','image', 'description','description_en','flags','language', 'category','created_by'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];
}
