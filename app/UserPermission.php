<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserPermission extends Authenticatable
{
    use Notifiable;
    protected $table = 'module_access_permision';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'module_id', 'read_access', 'write_access', 'edit_access', 'delete_access'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

      protected $hidden = ['created_at','updated_at'];

}
