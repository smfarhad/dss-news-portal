-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2019 at 05:53 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dss_newsportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `slung_en` varchar(200) NOT NULL,
  `slung_bd` varchar(200) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_bd` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `parent_id`, `slung_en`, `slung_bd`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(1, 0, 'programmes', '7c8a3f01-7285-47f6-ae1c-42f7ed1ee921', 'Programmes', 'কর্মসূচি', '2018-12-25 17:02:15', '2018-12-25 17:02:15'),
(2, 0, 'projects', '9f2b92f4-6324-404d-a4eb-318379025586', 'Projects', 'প্রকল্প', '2018-12-25 17:02:20', '2018-12-25 17:02:20'),
(3, 0, 'finance', '97d40b2b-91fe-4178-a57c-2b940ad8f38f', 'Finance', 'আর্থিক', '2018-12-25 17:02:31', '2018-12-25 17:02:31'),
(4, 0, 'publication', 'f52816b2-58be-4618-93d9-86fbe172789c', 'Publication', 'প্রকাশনা', '2018-12-25 17:02:36', '2018-12-25 17:02:36'),
(5, 1, 'cancer-kidney-liver-cirrhosis-stroke-paralyzed-and-congenital-heart-disease-financial-support', '4b6316f4-0886-44a3-a2bf-026a194114a3', 'Cancer, kidney, liver cirrhosis, stroke paralyzed and congenital heart disease financial support', 'ক্যান্সার, কিডনী, লিভার সিরোসিস, স্ট্রোকে প্যারালাইজড ও জন্মগত হৃদরোগীর আর্থিক সহায়তা', '2018-12-25 17:02:51', '2018-12-25 17:02:51'),
(6, 1, 'development-of-living-standards-of-tea-workers', '025b93bd-b8df-4dea-9fe1-d329ac0ad09e', 'Development of living standards of tea workers', 'চা শ্রমিকদের জীবনমান উন্নয়ন', '2018-12-25 17:02:56', '2018-12-25 17:02:56'),
(7, 1, 'development-of-living-standards-of-hijra-people', '3e226e4e-f0b3-4823-822e-4668ef8ce3e1', 'Development of living standards of hijra people', 'হিজড়া জনগোষ্ঠীর জীবনমান উন্নয়ন', '2018-12-25 17:03:01', '2018-12-25 17:03:01'),
(8, 1, 'disability-identification-survey-program', '35a59e17-3a34-46ab-bf32-926d3bfeb277', 'Disability Identification Survey Program', 'প্রতিবন্ধিতা শনাক্তকরণ জরিপ কর্মসূচি', '2018-12-25 17:04:30', '2018-12-25 17:04:30'),
(9, 2, 'development-budget-project-program-based', 'a4e88e0b-4277-4690-ad39-14e54cacbfc1', 'Development budget (project / program based)', 'উন্নয়ন বাজেট (প্রকল্প/কর্মসূচিভিত্তিক)', '2018-12-25 17:03:07', '2018-12-25 17:03:07'),
(10, 2, 'projects-details', '1', 'Projects (Details)', 'প্রকল্পসমূহ (বিস্তারিত)1', '2018-12-25 17:03:14', '2018-12-25 17:03:14'),
(11, 2, 'monthly-report', 'ae21c666-3c6b-4937-ba0a-20612f72e4d1', 'Monthly report', 'মাসিক প্রতিবেদন', '2018-12-25 17:03:50', '2018-12-25 17:03:50'),
(12, 3, 'non-development-budget', 'a71b0875-eb50-410a-842f-52de3525a827', 'non Development Budget', 'অনুন্নয়ন বাজেট', '2018-12-25 17:03:58', '2018-12-25 17:03:58'),
(13, 3, 'allocation-and-grants17-18', '6b7e2ef5-38f1-4e05-b0e0-0140e7d8c195', 'Allocation and Grants(17-18)', 'বরাদ্দ ও মঞ্জুরী (২০১৭-১৮)', '2018-12-25 17:05:01', '2018-12-25 17:05:01'),
(14, 4, 'annual-report', '750bdeb6-711e-4808-9dc8-e89cf3c40a24', 'Annual Report', 'বার্ষিক প্রতিবেদন', '2018-12-25 17:04:05', '2018-12-25 17:04:05'),
(15, 4, 'monthly-report', '7bf5515e-9c90-4007-9ea4-cb9ddb496c53', 'Monthly report', 'মাসিক প্রতিবেদন', '2018-12-25 17:04:12', '2018-12-25 17:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `name_en` varchar(200) DEFAULT NULL,
  `slung` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `photographer` varchar(250) DEFAULT NULL,
  `description` longtext,
  `description_en` longtext,
  `featured` tinyint(4) DEFAULT NULL,
  `language` tinyint(1) DEFAULT NULL COMMENT '1=english,2=bangla',
  `category` int(11) DEFAULT NULL,
  `tag` smallint(6) DEFAULT NULL,
  `popular` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `name`, `name_en`, `slung`, `image`, `photographer`, `description`, `description_en`, `featured`, `language`, `category`, `tag`, `popular`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'Development budget (project / program based)', '65846d8a-a8ac-4810-b6b2-b1a978628aef', '5205940dd76e0efda09f1c222ec4a1fd.jpg', 'Farhad', '<div>&lt;p&gt;বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত &lt;/p&gt;&nbsp;</div><div>&lt;p&gt;বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত &lt;/p&gt;\r\n&lt;p&gt;বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত &lt;/p&gt;\r\n<br></div>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 0, NULL, 7, 1, NULL, 1, 1, '2018-12-25 14:38:47', NULL),
(2, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that was first', '8c6c9d5a-65e6-464e-a6fd-2020a646be02', 'cfac2913ff910f7e545c9d4e03f97635.jpg', 'Jeeku', 'বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত  বাংলায় বিস্তারিত', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 14, 2, 1, 1, 3, '2018-12-25 12:56:30', '2019-01-02 18:15:51'),
(3, 'জাতীয় সমাজসেবা একাডেমি আয়োজিত ২০-২৯ নভেম্বর ২০১৮', 'Neque porro quisquam est qui dolorem ipsum quia', 'c0e97fe3-8231-4557-9b38-644f07b84c30', 'a8fb545a4ec10fc8ecf7ad3df90e522b.jpg', 'Nishad', '<p>&nbsp;</p>\r\n\r\n<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত</p>\r\n\r\n<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>', 1, NULL, 6, 2, 2, 2, 3, '2018-12-25 16:12:31', '2019-09-06 17:11:45'),
(4, 'বয়স্ক ভাতা, মুক্তিযোদ্ধ ভাতা, প্রতিবন্ধী ভাতা ও উপবৃত্তি সেল', 'Neque porro quisquam est qui dolorem ipsum', 'd015c1d9-d21e-474f-8bb5-0f5d3276f346', '07d424e311053945e883dd3702dbb324.jpg', 'Zehad', '<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিতবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত\r\n</p>\r\n\r\n<p>\r\nবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিতবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত\r\n</p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 5, 1, 6, 3, 3, '2018-12-25 16:19:54', '2019-09-05 12:34:08'),
(5, '৬৬ লক্ষের বেশি মানুষ ভাতা পাচ্ছেন সমাজসেবা অধিদফতর থেকে', 'Neque porro quisquam est qui dolorem ipsum quia dolor', '6cadc12b-2831-4492-aa76-7639a5568060', 'b7d487c7e4766082b224f9507f74a576.jpg', 'Imran', '<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত&nbsp;</p><p></p><p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত <br>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত <br></p><p></p><p><br></p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 7, 3, 9, 3, 3, '2018-12-25 16:22:53', '2019-01-02 19:08:25'),
(6, 'দগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়ন', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet', '9711fe75-66ae-48db-9775-9d58891eb3eb', 'c70db64382385f5740e21e821657df40.jpg', 'Setu', '<p>দগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়ন<br></p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 7, 2, NULL, 3, 1, '2018-12-25 16:27:58', NULL),
(7, 'চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি', 'Neque porro quisquam est quia', '278a64f9-2b10-42a2-b985-694c4b9416a8', 'c0125358361ade14c043b5bf361a8f99.jpg', 'Jony', '<p>চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি </p>', '<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>\r\n<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>\r\n<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>', 1, NULL, 12, 2, NULL, 3, 1, '2018-12-25 16:33:12', '2018-12-25 10:47:23'),
(8, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that', '94fa486c-7fad-41a6-8e40-e70c5455d0cc', '175c3586016c9ad4b8a1ebd65abbda8b.jpg', 'A.kader', '<p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p>\r\n</p><p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p>\r\n</p><p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p></p>', 'English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that.English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that.English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that', 1, NULL, 8, 1, NULL, 3, 1, '2018-12-25 16:37:14', '2018-12-25 10:37:28'),
(9, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that', '2f441cc6-b28c-49ab-9273-cb73d90ecb11', '7e910f1b62657c23e773cc9c3006d687.jpg', 'Mohon', '<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত&nbsp;</p><p></p><p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত <br></p><br><p></p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 9, 3, NULL, 3, 1, '2018-12-25 16:44:08', NULL),
(10, 'প্রাকবৃত্তিমূলক প্রশিক্ষণ কেন্দ্র', 'You may use the where method on a query builder', 'd6cc56a0-dcfc-4403-986f-aedd815421ec', '77f2052a939ebdb48adabb1b2599614f.jpg', 'Parvej', '<p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত&nbsp;</p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><br><p></p>', '<p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl&nbsp;</p><p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl <br></p><p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl <br></p><br><p></p>', 1, NULL, 10, 3, NULL, 3, 1, '2018-12-25 16:46:23', NULL),
(11, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that was first', '83f785be-2507-40e7-9439-c180f2e8a2ab', 'a94b2c328194628a6c44dff04e1d718d.jpg', 'Abdullah', '<p>বাংলায় বিস্তারিত<br></p>', '<p>You may use the wheremethod on a query builder instance to add where You may use the where method on a query builder instance to add You may use the  method on a query builder instance to add</p>', 1, NULL, 6, 2, 16, 2, 3, '2018-12-26 18:09:01', '2019-01-02 18:13:21'),
(12, 'দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'fi English is a West Germanic language that was', '667dd3d2-b57b-4e34-90f3-1e2815aff424', 'cafb4663cfd6ff4e685fcb11a38835d8.jpg', 'Shovon', '<p>বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়</p>\r\n\r\n<p>&lt;p&gt;বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়বাংলায় বিস্তারিত বাংলায় বাংলায় বিস্তারিত বাংলায়&lt;/p&gt;</p>', '<p>Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;</p>\r\n\r\n<p>Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;Description Description&nbsp;</p>', 1, NULL, 7, 2, 16, 2, 3, '2018-12-27 10:38:08', '2019-09-06 17:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(11) NOT NULL,
  `slung` varchar(100) NOT NULL,
  `slung_en` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `slung`, `slung_en`, `name`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'f99c0855-e31e-48ec-bb0a-b6d4de87f706', 'tax-ratee', 'আয়কর পরি', 'Tax Ratee', '2018-11-28 16:21:55', '2018-12-25 17:08:05'),
(2, '6630a307-3174-4769-8acf-2e9f47ce3865', 'paid', 'পরিশোধ', 'Paid', '2018-11-28 16:21:55', '2018-12-25 17:08:09'),
(3, '11963e1d-032f-4640-9a1f-07cb16bacdce', 'scholarship-application', 'শিক্ষাবৃত্তির আবেদন', 'Scholarship Application', '2018-11-28 16:21:55', '2018-12-25 17:08:13'),
(4, '1c8443f9-c7ee-43b3-b16e-20a9b8d900f5', 'socialservice-academy', 'সমাজসেবা একাডেমি', 'Socialservice Academy', '2018-11-28 16:21:55', '2018-12-25 17:08:16'),
(5, '2b5302a4-0785-4cf4-9cd5-31b3501b7dae', 'social-degradation', 'সামাজিক অবক্ষয়', 'Social Degradation', '2018-11-28 16:21:55', '2018-12-25 17:08:19'),
(6, '58939e80-1789-4e91-b899-6b97f9a32d73', 'degradation', 'অবক্ষয়', 'Degradation', '2018-11-28 16:21:55', '2018-12-25 17:08:22'),
(7, 'e4bb00e2-9322-4f13-b0bf-efc1dbd302bc', 'professional-skill', 'পেশাগত দক্ষতা', 'Professional Skill', '2018-11-28 16:21:55', '2018-12-25 17:08:26');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `slung_en` varchar(200) NOT NULL,
  `slung_bd` varchar(200) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_bd` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `slung_en`, `slung_bd`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(1, 0, 'programmes', 'c102b87a-4d5d-4db8-8bcb-fe3bf5419016', 'Programmes', 'কর্মসূচি', '2018-12-19 09:14:13', '2018-12-19 09:14:13'),
(2, 0, 'projects', 'e0f88939-4b28-486e-92a7-4f960ac9f9ba', 'Projects', 'প্রকল্প', '2018-12-25 14:26:23', '2018-12-25 14:26:23'),
(3, 0, 'finance', '4535788e-c94c-426d-beb2-e4b048cdffd4', 'Finance', 'আর্থিক', '2018-12-25 14:26:48', '2018-12-25 14:26:48'),
(4, 0, 'publication', '501f2de0-01b8-4627-91e9-892805e46c38', 'Publication', 'প্রকাশনা', '2018-12-25 14:26:31', '2018-12-25 14:26:31'),
(5, 1, 'cancer-kidney-liver-cirrhosis-stroke-paralyzed-and-congenital-heart-disease-financial-support', 'a7becb95-34b2-4025-9e60-83b0d60d73f5', 'Cancer, kidney, liver cirrhosis, stroke paralyzed and congenital heart disease financial support', 'ক্যান্সার, কিডনী, লিভার সিরোসিস, স্ট্রোকে প্যারালাইজড ও জন্মগত হৃদরোগীর আর্থিক সহায়তা', '2018-12-25 14:27:00', '2018-12-25 14:27:00'),
(6, 1, 'development-of-living-standards-of-tea-workers', '6cf7c95d-0280-40c2-bb67-3b4b6c9b2628', 'Development of living standards of tea workers', 'চা শ্রমিকদের জীবনমান উন্নয়ন', '2018-12-25 14:27:06', '2018-12-25 14:27:06'),
(7, 1, 'development-of-living-standards-of-hijra-people', 'ef5cc0ce-6659-4880-9f3d-d77741c3fc21', 'Development of living standards of hijra people', 'হিজড়া জনগোষ্ঠীর জীবনমান উন্নয়ন', '2018-12-25 14:27:19', '2018-12-25 14:27:19'),
(8, 1, 'disability-Identification-survey-program', 'প্রতিবন্ধিতা-শনাক্তকরণ-জরিপ-কর্মসূচি', 'Disability Identification Survey Program', 'প্রতিবন্ধিতা শনাক্তকরণ জরিপ কর্মসূচি', '2018-11-16 18:23:23', '0000-00-00 00:00:00'),
(9, 2, 'development-budget-project-program-based', '83f9e7b7-ed0c-4cd1-b837-342dc2ed2304', 'Development budget (project / program based)', 'উন্নয়ন বাজেট (প্রকল্প/কর্মসূচিভিত্তিক)', '2018-12-25 14:27:33', '2018-12-25 14:27:33'),
(10, 2, 'projects-details', '64cbc7f4-b6a1-4b7e-b7fb-5d0bff88cdab', 'Projects (Details)', 'প্রকল্পসমূহ (বিস্তারিত)', '2018-12-25 14:27:59', '2018-12-25 14:27:59'),
(11, 2, 'monthly-report', '2ea566a0-9889-42bb-a169-0d50ab027cc4', 'Monthly report', 'মাসিক প্রতিবেদন', '2018-12-25 14:28:22', '2018-12-25 14:28:23'),
(12, 3, 'non-development-budget', 'f2c31330-04e7-4fda-a8e5-a57edbd2ce63', 'non Development Budget', 'অনুন্নয়ন বাজেট', '2018-12-25 14:28:29', '2018-12-25 14:28:29'),
(13, 3, 'allocation-and-grants17-18', 'f515ffe2-b4c6-4cd9-85e7-e8ec831ae178', 'Allocation and Grants(17-18)', 'বরাদ্দ ও মঞ্জুরী (২০১৭-১৮)', '2018-12-25 14:28:37', '2018-12-25 14:28:37'),
(14, 4, 'annual-report', '8bbdc4c7-a9c1-4db8-8603-e1838f2e605b', 'Annual Report', 'বার্ষিক প্রতিবেদন', '2018-12-25 14:29:09', '2018-12-25 14:29:09'),
(15, 4, 'monthly-report', '6478c147-b1de-4d2f-9bd8-c8843ae331cc', 'Monthly report', 'মাসিক প্রতিবেদন', '2018-12-25 14:28:48', '2018-12-25 14:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `name_bd` varchar(250) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `name_bd`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'উপজেলা সমাজসেবা অফিসার', 'Upperdivision Assistant', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'সহকারি পরিচালক', 'Assistant Director', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'সমাজসেবা অফিসার (রেজিস্ট্রেশন)', 'Social Wellfare Officer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'উপজেলা সমাজসেবা অফিসার', 'Upozila Wellfare Officer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'প্রবেশন অফিসার', 'probationary officer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'উপতত্বাবধায়ক', 'Assistant Supervisor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'উপসহকারি পরিচালক', 'Sub Assistant Director', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'সমাজসেবা অফিসার (রেজিস্ট্রেশন)', 'Social Wellfare Officer(Registration)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'আই.টি ব্যবস্থাপক', 'IT manager', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'মহাব্যবস্থাপক', 'General Manager', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'ডিপ্লোমা ইঞ্জিনিয়ার', 'Diploma Engineer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'বিএসসি ইঞ্জিনিয়ার', 'B.sc Engineer', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `div_id` varchar(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_bd` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `div_id`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(1, '4', 'বরগুনা', 'BARGUNA', '0000-00-00 00:00:00', '2019-09-04 12:18:37'),
(2, '1', 'বরিশাল', 'BARISAL', '0000-00-00 00:00:00', '2019-09-04 12:18:49'),
(3, '2', 'BHOLA', 'ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '7', 'ঝালকাঠি', 'JHALOKATI', '0000-00-00 00:00:00', '2019-09-04 12:19:03'),
(5, '2', 'পটুয়াখালী', 'PATUAKHALI', '0000-00-00 00:00:00', '2019-09-04 12:20:30'),
(6, '7', 'পিরোজপুর', 'PIROJPUR', '0000-00-00 00:00:00', '2019-09-04 12:20:41'),
(7, '20', 'BANDARBA', 'বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '20', 'BRAHMANBARIA', 'ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '20', 'CHANDPUR', 'চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '20', 'CHITTAGONG', 'চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '20', 'COMILLA', 'কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '2', 'COX\'S BAZAR', 'কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '20', 'FENI', 'ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '20', 'KHAGRACHHARI', 'খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '20', 'LAKSHMIPUR', 'লক্ষ্মীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '20', 'NOAKHALI', 'নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '20', 'RANGAMATI', 'রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '40', 'BAGERHAT', 'বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '40', 'CHUADANGA', 'চুয়াডাংগা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '40', 'JESSORE', 'যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '40', 'JHENAIDAH', 'ঝিনাইদহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '40', 'KHULNA', 'খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '40', 'KUSHTIA', 'কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '40', 'MAGURA', 'মাগুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '40', 'MEHERPUR', 'মেহেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '40', 'NARAIL', 'নড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '40', 'SATKHIRA', 'সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '50', 'BOGRA', 'বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '50', 'JOYPURHAT', 'জয়পুরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '50', 'NAOGAO', 'নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '50', 'NATORE', 'নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '50', 'CHAPAINAWABGANJ', 'চাঁপাইনবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '50', 'PABNA', 'পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '50', 'RAJSHAHI', 'রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '50', 'SIRAJGANJ', 'সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '60', 'HABIGANJ', 'হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '60', 'MAULVIBAZAR', 'মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '60', 'SUNAMGANJ', 'সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '60', 'SYLHET', 'সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '55', 'DINAJPUR', 'দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '55', 'GAIBANDHA', 'গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '55', 'KURIGRAM', 'কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '55', 'LALMONIRHAT', 'লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '55', 'NILPHAMARI ZILA', 'নীলফামারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '55', 'PANCHAGARH', 'পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '55', 'RANGPUR', 'রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '55', 'THAKURGAO', 'ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '30', 'DHAKA', 'ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '30', 'FARIDPUR', 'ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '30', 'GAZIPUR', 'গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '30', 'GOPALGANJ', 'গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '30', 'JAMALPUR', 'জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '30', 'KISHOREGONJ', 'কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '30', 'MADARIPUR', 'মাদারীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '30', 'MANIKGANJ', 'মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '30', 'MUNSHIGANJ', 'মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '30', 'MYMENSINGH', 'ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '30', 'NARAYANGANJ', 'নারায়ণগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '30', 'NARSINGDI', 'নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '30', 'NETRAKONA', 'নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '30', 'RAJBARI', 'রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '30', 'SHARIATPUR', 'শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '30', 'SHERPUR', 'শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '30', 'TANGAIL', 'টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_bd` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(1, 'BARISAL', 'বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'CHITTAGONG', 'চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'KHULNA', 'খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'RAJSHAHI', 'রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'SYLHET', 'সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'RANGPUR', 'রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'DHAKA', 'ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `flags`
--

CREATE TABLE `flags` (
  `id` smallint(6) NOT NULL,
  `flagtype` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `description`, `added_on`, `added_by`) VALUES
(2, 'সমাবর্তন', 'জানুয়ারির সাত তারিখ কুষ্টিয়ার ইসলামী বিশ্ববিদ্যালয়ের সমাবর্তন অনুষ্ঠানে আমাকে সমাবর্তন ভাষণ দেওয়ার জন্য অনুরোধ করা হয়েছিল। প্রায় দশ হাজার গ্রাজুয়েটদের সামনে বক্তৃতা দেওয়ার সুযোগ আমার মতো মানুষের জন্য একটা অনেক বড় সুযোগ। আমি এর আগে যখনই সমাবর্তন ভাষণ দেওয়ার সুযোগ পেয়েছি ঘুরে ফিরে একই কথা বলেছি। এবারও তাই,', '2018-08-02 21:03:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `table_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id` int(11) NOT NULL DEFAULT '0',
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `table_name`, `image_name`, `fk_id`, `added_on`, `added_by`) VALUES
(5, 'event', 'dcaa3589bc4a9a8191938ece306affb1.PNG', 1, '2018-08-05 16:46:13', 0),
(6, 'event', '9de612c376619b98168cafc28ebf84ea.jpg', 2, '2018-08-07 08:30:20', 0),
(7, 'gallery', 'c57bb45ea3416d31e70ff6bd3564c65b.png', 2, '2018-08-07 09:53:20', 0),
(8, 'gallery', '31e9495a972dbabc02167911d733087c.png', 2, '2018-08-07 09:53:20', 0),
(9, 'gallery', 'e2af6418b6af44893f1a8ec42c7bd292.png', 2, '2018-08-07 09:53:20', 0),
(10, 'gallery', 'e45a543ae414ed0363eee67ebe08e6d4.png', 2, '2018-08-07 09:53:20', 0),
(11, 'gallery', '2708bdb1a1622331acff9984580e6e71.png', 2, '2018-08-07 09:53:20', 0),
(12, 'gallery', '3c3987ce2daef1e70357952413fbd7ed.png', 2, '2018-08-07 09:53:20', 0),
(13, 'news', 'c410f1bdf760983da18297ec53bf91e1.jpg', 2, '2018-08-07 12:43:21', 0),
(14, 'news', '96a2a67c66547b79446687ee382e0811.jpg', 3, '2018-08-07 12:56:16', 0),
(15, 'event', 'e35e20a12322427ccd36d2279ae08fe9.jpg', 3, '2018-08-07 16:07:53', 0),
(16, 'slider', 'a8f5979e24517c3961669bb94a5fdf26.png', 1, '2018-08-08 17:54:36', 0),
(17, 'slider', '365eff992ffa2675e1f2f307b29b9781.png', 1, '2018-08-08 17:54:49', 0),
(18, 'slider', '13b2f8d8c21d9481e7dd02585b5625f5.png', 1, '2018-08-08 17:55:03', 0),
(19, 'slider', '5375725aa3ae3194e37b50e6df1397dd.png', 1, '2018-08-08 17:55:18', 0),
(20, 'slider', 'e1dc3da6682c7fb21f987fd7b0c44b68.png', 1, '2018-08-08 17:55:50', 0),
(27, 'event', '5944b6303fa78d3f1f47caa8c7f7bb04.jpg', 53, '2018-09-18 10:29:08', 0),
(28, 'event', '6cd52414adc607c187d4f5e99d6a874b.jpg', 54, '2018-09-18 10:36:37', 0),
(30, 'event', '4193dd3ff696febaa0e8349e60448f70.png', 10, '2018-10-02 18:53:17', 0),
(31, 'event', '5bdfcba33e068238fad03d367a3c4c2c.jpg', 10, '2018-10-02 18:53:23', 0),
(32, 'event', '62aaece1fa693a53d8f24e185b7e9dc9.png', 10, '2018-10-02 18:53:32', 0),
(33, 'users', 'dd523f8b6587653ce33d6a732852fd90.jpg', 10, '2018-10-09 04:04:58', 0),
(36, 'user_information', '0c337847138af5546bbc85c1da921816.jpg', 2, '2018-10-10 17:51:24', 0),
(37, 'user_information', '98d74d7b22b399d39b0c0ae3164012b9.jpg', 2, '2018-10-10 17:51:24', 0),
(38, 'user_information', '65ac6b9a7e1faf5ae6bd051d3d0c85cd.jpg', 2, '2018-10-10 17:51:24', 0),
(39, 'user_information', 'f048394d37d1e761ca790632d6df6c34.jpg', 2, '2018-10-10 17:51:24', 0),
(40, 'user_information', '63a235e259411017ec2b0337a705f41a.jpg', 2, '2018-10-10 17:51:24', 0),
(42, 'users', '1a0467995bd3ac179f8e98e0ba61c669_thumb.png', 2, '2018-10-11 04:52:08', 0),
(43, 'users', 'a50629f9ca50997e36aee98ab5e2a1c5_thumb.jpg', 3, '2018-10-16 09:14:54', 0),
(44, 'users_special_days', 'fd9424f2f70de4477d126b8c23ca4913_thumb.png', 2, '2018-10-21 05:00:54', 0),
(45, 'users', '2f882f17d3e28437f78efb3d0a8f9aed_thumb.jpg', 1, '2018-11-29 12:11:42', 0),
(46, 'user_information', '370046a8d21051ecb360d0410fed06eb.jpg', 1, '2018-11-29 12:28:12', 0),
(47, 'user_information', '1cfb7fc5af57bfc76eab04cd43fa884d.jpg', 1, '2018-11-29 12:28:12', 0),
(48, 'user_information', '612a6cecbc7803e0488049f9c620adef.jpg', 1, '2018-11-29 12:28:12', 0),
(49, 'user_information', '71ca596575c4aec130479b2e42fac88f.jpg', 1, '2018-11-29 12:28:12', 0),
(50, 'user_information', '37085dcfea0876d974d510c80db3b2cf.jpg', 1, '2018-11-29 12:30:07', 0),
(51, 'users', '99a1af17232ed1dfe2636b98a0dda373_thumb.jpg', 18, '2019-05-05 00:12:10', 0),
(52, 'users', '239b212f31bc7cd8f3d7ebf8085fea54_thumb.jpg', 16, '2019-06-12 08:33:20', 0),
(53, 'users', 'b0e5248aa29ab846401e3af1d062da37_thumb.jpg', 25, '2019-06-12 10:26:23', 0),
(54, 'users', 'ab47a60aebf0901fdafeb6880ad6f402_thumb.jpg', 21, '2019-06-12 10:28:59', 0),
(55, 'users', '6c81c7f51bb6a4d9ed049461d83e97c1_thumb.jpg', 22, '2019-06-12 10:30:22', 0),
(56, 'users', 'aa77d95e8b7a1553e8e0b0afb2697a19_thumb.jpg', 23, '2019-06-12 10:31:50', 0),
(57, 'users', 'e46ef556b438885f0aa3c9da519c845d_thumb.jpg', 24, '2019-06-12 10:33:05', 0),
(58, 'users', '03412cc045ef2467bc52b941ac7dc6ef_thumb.jpg', 26, '2019-06-12 10:35:00', 0),
(59, 'users', 'a3be4880fb05fcd0afe5d8b7489157da_thumb.jpg', 27, '2019-06-12 10:36:17', 0),
(60, 'users', '69d2b7c7ded8f35df87f6daf8fb3004f_thumb.jpg', 28, '2019-06-12 10:37:37', 0),
(61, 'users', 'b18bb78b76e92400729438131ee16810_thumb.jpg', 31, '2019-06-12 10:39:28', 0),
(62, 'users', '3c0f654d9752ecd72b17308d9c4fe390_thumb.jpg', 17, '2019-06-12 10:41:55', 0),
(63, 'user_information', 'e3ccfbb14815f1f9535341bdfd74faa7.jpg', 16, '2019-06-12 12:45:12', 0),
(64, 'user_information', 'afb202dd11e3872afaab955304099056.jpg', 16, '2019-06-12 12:45:12', 0),
(65, 'user_information', 'ec61a37e4020ffa4fc039f51c6c05fe9.jpg', 16, '2019-06-12 12:45:12', 0),
(66, 'user_information', '3102c9e1ce236a43dcc532cc5167814c.jpg', 16, '2019-06-12 12:45:12', 0),
(67, 'user_information', '0f1a7f7db82cefe9045c5908e500f41b.jpg', 16, '2019-06-12 12:45:12', 0),
(68, 'users', 'b6c12075e62c26bed5c1aef86a62ee7f_thumb.JPG', 44, '2019-06-12 15:44:01', 0),
(69, 'users', '8c20316172fe8bd6ddbe30a38b0e19ff_thumb.jpg', 73, '2019-06-13 01:21:43', 0),
(70, 'users', '13822945207edad0d87287adc14e493d_thumb.jpg', 60, '2019-06-13 07:56:07', 0),
(71, 'users', 'b91bfd375b7db0262274f4a79e1b5834_thumb.jpg', 86, '2019-06-14 15:46:03', 0),
(72, 'users', 'd61b9be3f6938f9e6662fa01935a62b4_thumb.jpg', 74, '2019-06-16 11:35:11', 0),
(73, 'users', 'd9c65149e34a0a95c623c26af46c1e7e_thumb.jpg', 75, '2019-06-24 09:38:20', 0),
(74, 'user_information', 'cc67fd8035c6656d5633ea5ca172ed74.jpg', 73, '2019-06-27 07:29:56', 0),
(75, 'user_information', '49d1f8b8b780c6dcaef8b589404aa3ea.jpg', 73, '2019-06-27 07:30:16', 0),
(76, 'user_information', 'be59e211816b7e6cd44871af670e6661.JPG', 18, '2019-08-26 01:05:15', 0),
(77, 'user_information', '11331ac030cdd47e3a494c393bd8349c.JPG', 18, '2019-08-26 01:06:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HomeController', '2018-12-08 11:16:32', '2018-12-08 05:37:16'),
(2, 'CategoryController', '2018-12-08 11:14:07', NULL),
(3, 'ContactController', '2018-12-08 11:14:46', NULL),
(4, 'ModuleController', '2018-12-08 11:15:10', NULL),
(5, 'NewsController', '2018-12-08 11:15:22', NULL),
(6, 'NoticeController', '2018-12-08 11:15:34', NULL),
(7, 'TagsController', '2018-12-08 11:15:43', NULL),
(8, 'BlogTagsController', '2018-12-08 11:17:03', NULL),
(9, 'BlogCategoryController', '2018-12-08 11:16:46', NULL),
(10, 'BlogPostController', '2018-12-08 11:16:54', NULL),
(11, 'UserController', '2018-12-11 10:00:15', NULL),
(12, 'UserGroupController', '2018-12-11 10:01:03', NULL),
(13, 'UserPermissionController', '2018-12-11 10:01:20', NULL),
(14, 'SettingsController', '2018-12-27 05:20:49', NULL),
(15, 'UserActivitiesController', '2019-01-01 04:19:47', NULL),
(16, 'DivisionController', '2019-09-04 10:07:03', NULL),
(17, 'DistrictController', '2019-09-04 10:59:06', NULL),
(18, 'UpozilaController', '2019-09-04 13:00:33', NULL),
(19, 'DesignationController', '2019-09-05 08:57:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_access_permision`
--

CREATE TABLE `module_access_permision` (
  `id` int(11) NOT NULL,
  `group_id` tinyint(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `read_access` tinyint(11) DEFAULT NULL,
  `write_access` tinyint(11) DEFAULT NULL,
  `edit_access` tinyint(11) DEFAULT NULL,
  `delete_access` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_access_permision`
--

INSERT INTO `module_access_permision` (`id`, `group_id`, `module_id`, `read_access`, `write_access`, `edit_access`, `delete_access`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(20, 1, 1, 1, 1, 1, 1, '2018-12-11 11:18:25', NULL, NULL, NULL),
(21, 1, 2, 1, 1, 1, 1, '2018-12-11 11:17:00', NULL, NULL, NULL),
(22, 1, 3, 1, 1, 1, 1, '2018-12-11 11:18:28', NULL, NULL, NULL),
(23, 1, 4, 1, 1, 1, 1, '2018-12-11 11:18:33', NULL, NULL, NULL),
(24, 1, 5, 1, 1, 1, 1, '2018-12-11 11:18:31', NULL, NULL, NULL),
(25, 1, 6, 1, 1, 1, 1, '2018-12-11 11:18:38', NULL, NULL, NULL),
(26, 1, 7, 1, 1, 1, 1, '2018-12-11 11:18:40', NULL, NULL, NULL),
(27, 1, 8, 1, 1, 1, 1, '2018-12-11 11:18:44', NULL, NULL, NULL),
(28, 1, 9, 1, 1, 1, 1, '2018-12-11 11:18:47', NULL, NULL, NULL),
(29, 1, 10, 1, 1, 1, 1, '2018-12-11 11:18:51', NULL, NULL, NULL),
(30, 1, 11, 1, 1, 1, 1, '2018-12-11 11:18:54', NULL, NULL, NULL),
(31, 1, 12, 1, 1, 1, 1, '2018-12-11 11:18:56', NULL, NULL, NULL),
(32, 1, 13, 1, 1, 1, 1, '2018-12-11 11:19:01', NULL, NULL, NULL),
(33, 2, 1, 1, 1, 1, 1, '2018-12-23 11:26:57', NULL, NULL, NULL),
(34, 2, 5, 1, 1, 1, 1, '2018-12-19 12:42:38', NULL, NULL, NULL),
(35, 2, 2, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(36, 2, 3, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(37, 2, 4, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(38, 2, 6, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(39, 2, 7, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(40, 2, 8, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(41, 2, 9, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(42, 2, 10, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(43, 2, 11, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(44, 2, 12, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(45, 2, 13, 0, 0, 0, 0, '2018-12-19 12:42:38', NULL, NULL, NULL),
(46, 3, 1, 1, 1, 1, 1, '2018-12-23 10:03:04', NULL, NULL, NULL),
(47, 3, 5, 1, 1, 1, 1, '2018-12-23 10:03:04', NULL, NULL, NULL),
(48, 3, 10, 1, 1, 1, 1, '2018-12-23 10:03:04', NULL, NULL, NULL),
(49, 3, 2, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(50, 3, 3, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(51, 3, 4, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(52, 3, 6, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(53, 3, 7, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(54, 3, 8, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(55, 3, 9, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(56, 3, 11, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(57, 3, 12, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(58, 3, 13, 0, 0, 0, 0, '2018-12-23 10:03:04', NULL, NULL, NULL),
(59, 1, 14, 1, 1, 1, 1, '2018-12-27 05:21:45', NULL, NULL, NULL),
(61, 2, 14, 1, 1, 1, 1, '2018-12-27 14:03:35', NULL, NULL, NULL),
(62, 1, 15, 1, 1, 1, 1, '2019-01-01 04:20:51', NULL, NULL, NULL),
(63, 3, 15, 1, 1, 1, 1, '2019-01-01 04:21:08', NULL, NULL, NULL),
(64, 1, 16, 1, 1, 1, 1, '2019-09-04 10:07:34', NULL, NULL, NULL),
(65, 1, 17, 1, 1, 1, 1, '2019-09-04 10:59:23', NULL, NULL, NULL),
(66, 1, 18, 1, 1, 1, 1, '2019-09-04 13:02:27', NULL, NULL, NULL),
(67, 1, 19, 1, 1, 1, 1, '2019-09-05 08:57:57', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `published` date DEFAULT NULL,
  `download` varchar(400) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `title`, `published`, `download`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '২৯/১০/২০১৮ তারিখে অনুষ্ঠিত ব্যবহারিক পরীক্ষার ফলাফ...', '2018-11-28', '6uMPLycxpC81qOUF14B8nW2EQL5FpYmyVQSGXtBF.doc', 1, '2018-11-29 18:14:32', NULL),
(3, 'বিস্তারিত বিস্তারিত', '2018-11-29', '774c7eaa97de3a021c1e1a631d5770ff.docx', 1, '2018-11-29 19:35:01', '2018-11-29 13:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `dist_id` varchar(11) NOT NULL,
  `upza_id` varchar(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_bd` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `dist_id`, `upza_id`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(1, '87', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KALIGANJ,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,কালীগঞ্জ,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '87', '82', 'UPAZILA SOCIAL SERVICES OFFICE,SATKHIRA SADAR,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,সাতক্ষীরা সদর,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '87', '86', 'UPAZILA SOCIAL SERVICES OFFICE,SHYAMNAGAR,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,শ্যামনগর,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '87', '90', 'UPAZILA SOCIAL SERVICES OFFICE,TALA,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,তালা,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '10', '20', 'UPAZILA SOCIAL SERVICES OFFICE,BOGRA SADAR,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,বগুড়া সদর,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '10', '06', 'UPAZILA SOCIAL SERVICES OFFICE,ADAMDIGHI,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,আদমদিঘী,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '10', '27', 'UPAZILA SOCIAL SERVICES OFFICE,DHUNAT,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,ধুনট,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '10', '33', 'UPAZILA SOCIAL SERVICES OFFICE,DHUPCHANCHIA,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,দুপচাঁচিয়া,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '10', '40', 'UPAZILA SOCIAL SERVICES OFFICE,GABTALI,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,গাবতলী,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '10', '54', 'UPAZILA SOCIAL SERVICES OFFICE,KAHALOO,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,কাহালু,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '36', '05', 'UPAZILA SOCIAL SERVICES OFFICE,BAHUBAL,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,বাহুবল,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '36', '11', 'UPAZILA SOCIAL SERVICES OFFICE,BANIACHONG,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,বানিয়াচং,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '36', '26', 'UPAZILA SOCIAL SERVICES OFFICE,CHUNARUGHAT,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,চুনারুঘাট,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '36', '44', 'UPAZILA SOCIAL SERVICES OFFICE,HABIGANJ SADAR,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,হবিগঞ্জ সদর,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '36', '68', 'UPAZILA SOCIAL SERVICES OFFICE,LAKHAI,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,লাখাই,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '36', '71', 'UPAZILA SOCIAL SERVICES OFFICE,MADHABPUR,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,মাধবপুর,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '36', '77', 'UPAZILA SOCIAL SERVICES OFFICE,NABIGANJ,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,নবীগঞ্জ ,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '58', '14', 'UPAZILA SOCIAL SERVICES OFFICE,BARLEKHA,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,বড়লেখা,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '58', '35', 'UPAZILA SOCIAL SERVICES OFFICE,JURI,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,জুড়ী,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '58', '56', 'UPAZILA SOCIAL SERVICES OFFICE,KAMALGANJ,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কমলগঞ্জ ,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '48', '49', 'UPAZILA SOCIAL SERVICES OFFICE,KISHOREGANJ SADAR,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,কিশোরগঞ্জ সদর,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '48', '54', 'UPAZILA SOCIAL SERVICES OFFICE,KULIAR CHAR,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,কুলিয়ারচর,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '48', '59', 'UPAZILA SOCIAL SERVICES OFFICE,MITHAMAIN,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,মিঠামঈন,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '48', '76', 'UPAZILA SOCIAL SERVICES OFFICE,NIKLI,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,নিকলী,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '48', '79', 'UPAZILA SOCIAL SERVICES OFFICE,PAKUNDIA,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,পাকুন্দিয়া,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '48', '92', 'UPAZILA SOCIAL SERVICES OFFICE,TARAIL,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,তাড়াইল,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '54', '40', 'UPAZILA SOCIAL SERVICES OFFICE,KALKINI,MADARIPUR', 'উপজেলা সমাজসেবা কার্যালয়,কালকিনি,মাদারীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '54', '54', 'UPAZILA SOCIAL SERVICES OFFICE,MADARIPUR SADAR,MADARIPUR', 'উপজেলা সমাজসেবা কার্যালয়,মাদারীপুর সদর,মাদারীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '54', '80', 'UPAZILA SOCIAL SERVICES OFFICE,RAJOIR,MADARIPUR', 'উপজেলা সমাজসেবা কার্যালয়,রাজৈর,মাদারীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '54', '87', 'UPAZILA SOCIAL SERVICES OFFICE,SHIBCHAR,MADARIPUR', 'উপজেলা সমাজসেবা কার্যালয়,শিবচর,মাদারীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '56', '10', 'UPAZILA SOCIAL SERVICES OFFICE,DAULATPUR,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,দৌলতপুর,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '56', '22', 'UPAZILA SOCIAL SERVICES OFFICE,GHIOR,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,ঘিওর,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '56', '28', 'UPAZILA SOCIAL SERVICES OFFICE,HARIRAMPUR,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,হরিরামপুর,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '56', '46', 'UPAZILA SOCIAL SERVICES OFFICE,MANIKGANJ SADAR,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,মানিকগঞ্জ সদর,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '56', '70', 'UPAZILA SOCIAL SERVICES OFFICE,SATURIA,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সাটুরিয়া,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '56', '78', 'UPAZILA SOCIAL SERVICES OFFICE,SHIBALAYA,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,শিবালয়,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '56', '82', 'UPAZILA SOCIAL SERVICES OFFICE,SINGAIR,MANIKGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সিংগাইর,মানিকগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '59', '24', 'UPAZILA SOCIAL SERVICES OFFICE,GAZARIA,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,গজারিয়া,মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '59', '44', 'UPAZILA SOCIAL SERVICES OFFICE,LOHAJANG,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়, লৌহজং, মুন্সিগঞ্জ,লৌহজং,মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '59', '56', 'UPAZILA SOCIAL SERVICES OFFICE,MUNSHIGANJ SADAR,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়, মুন্সিগঞ্জ সদর, মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '59', '74', 'UPAZILA SOCIAL SERVICES OFFICE,SERAJDIKHAN,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সিরাজদিখান, মুন্সিগঞ্জ,সিরাজদিখান,মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '59', '84', 'UPAZILA SOCIAL SERVICES OFFICE,SREENAGAR,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়, শ্রীনগর, মুন্সিগঞ্জ,শ্রীনগর,মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '59', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TONGIBARI,MUNSHIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,টঙ্গীবাড়ী,মুন্সিগঞ্জ,টংগীবাড়ী,মুন্সিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '61', '13', 'UPAZILA SOCIAL SERVICES OFFICE,BHALUKA,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ভালুকা,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '61', '16', 'UPAZILA SOCIAL SERVICES OFFICE,DHOBAURA,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ধোবাউড়া,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '61', '20', 'UPAZILA SOCIAL SERVICES OFFICE,FULBARIA,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ফুলবাড়ীয়া,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '61', '22', 'UPAZILA SOCIAL SERVICES OFFICE,GAFFARGAON,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,গফরগাঁও,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '61', '23', 'UPAZILA SOCIAL SERVICES OFFICE,GAURIPUR,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,গৌরীপুর,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '61', '24', 'UPAZILA SOCIAL SERVICES OFFICE,HALUAGHAT,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,হালুয়াঘাট,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '61', '31', 'UPAZILA SOCIAL SERVICES OFFICE,ISHWARGANJ,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ঈশ্বরগঞ্জ,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '61', '52', 'UPAZILA SOCIAL SERVICES OFFICE,MYMENSINGH SADAR,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ময়মনসিংহ সদর,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '61', '65', 'UPAZILA SOCIAL SERVICES OFFICE,MUKTAGACHHA,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,মুক্তাগাছা,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '61', '65', 'UPAZILA SOCIAL SERVICES OFFICE,MUKTAGACHHA,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,মুক্তাগাছা,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '61', '72', 'UPAZILA SOCIAL SERVICES OFFICE,NANDAIL,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,নান্দাইল,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '61', '81', 'UPAZILA SOCIAL SERVICES OFFICE,PHULPUR,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ফুলপুর,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '61', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TRISHAL,MYMENSINGH', 'উপজেলা সমাজসেবা কার্যালয়,ত্রিশাল,ময়মনসিংহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '67', '02', 'UPAZILA SOCIAL SERVICES OFFICE,ARAIHAZAR,NARAYANGANJ', 'উপজেলা সমাজসেবা কার্যালয়,আড়াইহাজার,নারায়ণগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '67', '04', 'UPAZILA SOCIAL SERVICES OFFICE,SONARGAON,NARAYANGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সোনারগাঁও,নারায়ণগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '67', '06', 'UPAZILA SOCIAL SERVICES OFFICE,BANDAR,NARAYANGANJ', 'উপজেলা সমাজসেবা কার্যালয়,বন্দর,নারায়ণগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '67', '68', 'UPAZILA SOCIAL SERVICES OFFICE,RUPGANJ,NARAYANGANJ', 'উপজেলা সমাজসেবা কার্যালয়,রূপগঞ্জ,নারায়ণগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '68', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BELABO,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,বেলাবো,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '68', '52', 'UPAZILA SOCIAL SERVICES OFFICE,MANOHARDI,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,মনোহরদী ,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '68', '60', 'UPAZILA SOCIAL SERVICES OFFICE,NARSINGDI SADAR,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,নরসিংদী সদর,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '68', '63', 'UPAZILA SOCIAL SERVICES OFFICE,PALASH,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,পলাশ,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, '68', '64', 'UPAZILA SOCIAL SERVICES OFFICE,ROYPURA,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,রায়পুরা,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, '68', '76', 'UPAZILA SOCIAL SERVICES OFFICE,SHIBPUR,NARSINGDI', 'উপজেলা সমাজসেবা কার্যালয়,শিবপুর,নরসিংদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, '72', '04', 'UPAZILA SOCIAL SERVICES OFFICE,ATPARA,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,আটপাড়া,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, '72', '09', 'UPAZILA SOCIAL SERVICES OFFICE,BARHATTA,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,বারহাট্টা,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, '72', '18', 'UPAZILA SOCIAL SERVICES OFFICE,DURGAPUR,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,দূর্গাপুর ,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, '72', '38', 'UPAZILA SOCIAL SERVICES OFFICE,KHALIAJURI,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,খালিয়াজুরী,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, '72', '40', 'UPAZILA SOCIAL SERVICES OFFICE,KALMAKANDA,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,কলমাকান্দা,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, '72', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KENDUA,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,কেন্দুয়া,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, '72', '56', 'UPAZILA SOCIAL SERVICES OFFICE,MADAN,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,মদন,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, '72', '63', 'UPAZILA SOCIAL SERVICES OFFICE,MOHANGANJ,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,মোহনগঞ্জ,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, '04', '09', 'UPAZILA SOCIAL SERVICES OFFICE,AMTALI,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,আমতলী,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, '04', '19', 'UPAZILA SOCIAL SERVICES OFFICE,BAMNA,BARGUNA ', 'উপজেলা সমাজসেবা কার্যালয়,বামনা,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, '04', '28', 'UPAZILA SOCIAL SERVICES OFFICE,BARGUNA SADAR,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,বরগুনা সদর,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, '04', '47', 'UPAZILA SOCIAL SERVICES OFFICE,BETAGI,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,বেতাগী,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, '04', '85', 'UPAZILA SOCIAL SERVICES OFFICE,PATHARGHATA,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,পাথরঘাটা,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, '04', '90', 'UPAZILA SOCIAL SERVICES OFFICE,AMTALI,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,আমতলী,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, '06', '02', 'UPAZILA SOCIAL SERVICES OFFICE,AGAILJHARA,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,আগৈলঝড়া,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, '06', '03', 'UPAZILA SOCIAL SERVICES OFFICE,BABUGANJ,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,বাবুগঞ্জ,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, '06', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BAKERGANJ,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,বাকেরগঞ্জ,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, '06', '10', 'UPAZILA SOCIAL SERVICES OFFICE,BANARI PARA,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,বানারী পাড়া,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, '06', '32', 'UPAZILA SOCIAL SERVICES OFFICE,GAURNADI,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,গৌরনদী,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, '06', '36', 'UPAZILA SOCIAL SERVICES OFFICE,HIZLA,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,হিজলা,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, '06', '51', 'UPAZILA SOCIAL SERVICES OFFICE,BARISAL SADAR (KOTWALI),BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,বরিশাল সদর,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, '06', '62', 'UPAZILA SOCIAL SERVICES OFFICE,MHENDIGANJ,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,মেহেন্দীগঞ্জ,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, '06', '69', 'UPAZILA SOCIAL SERVICES OFFICE,MULADI,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,মূলাদী,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, '06', '94', 'UPAZILA SOCIAL SERVICES OFFICE,WAZIRPUR,BARISAL', 'উপজেলা সমাজসেবা কার্যালয়,উজীরপুর,বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, '09', '18', 'UPAZILA SOCIAL SERVICES OFFICE,BHOLA SADAR,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,ভোলা সদর,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, '09', '21', 'UPAZILA SOCIAL SERVICES OFFICE,BURHANUDDIN,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,বোরহানউদ্দিন,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, '09', '25', 'UPAZILA SOCIAL SERVICES OFFICE,CHAR FASSON,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,চরফ্যাশন,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, '09', '29', 'UPAZILA SOCIAL SERVICES OFFICE,DAULAT KHAN,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,দৌলতখান,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, '09', '54', 'UPAZILA SOCIAL SERVICES OFFICE,LALMOHAN,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,লালমোহন,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, '09', '65', 'UPAZILA SOCIAL SERVICES OFFICE,MANPURA,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,মনপুরা,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, '09', '91', 'UPAZILA SOCIAL SERVICES OFFICE,TAZUMUDDIN,BHOLA', 'উপজেলা সমাজসেবা কার্যালয়,তজুমদ্দিন,ভোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, '42', '40', 'UPAZILA SOCIAL SERVICES OFFICE,JHALOKATI SADAR,JHALOKATI', 'উপজেলা সমাজসেবা কার্যালয়,ঝালকাঠি সদর,ঝালকাঠি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, '42', '43', 'UPAZILA SOCIAL SERVICES OFFICE,KANTHALIA,JHALOKATI', 'উপজেলা সমাজসেবা কার্যালয়,কাঠালিয়া,ঝালকাঠি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, '42', '73', 'UPAZILA SOCIAL SERVICES OFFICE,NALCHITY,JHALOKATI', 'উপজেলা সমাজসেবা কার্যালয়,নলছিটি,ঝালকাঠি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, '42', '84', 'UPAZILA SOCIAL SERVICES OFFICE,RAJAPUR,JHALOKATI', 'উপজেলা সমাজসেবা কার্যালয়,রাজাপুর,ঝালকাঠি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, '78', '38', 'UPAZILA SOCIAL SERVICES OFFICE,BAUPHAL,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,বাউফল,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, '78', '52', 'UPAZILA SOCIAL SERVICES OFFICE,DASHMINA,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,দশমিনা,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, '78', '55', 'UPAZILA SOCIAL SERVICES OFFICE,DUMKI,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,দুমকি ,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, '78', '57', 'UPAZILA SOCIAL SERVICES OFFICE,GALACHIPA,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,গলাচিপা,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, '78', '66', 'UPAZILA SOCIAL SERVICES OFFICE,KALAPARA,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,কলাপাড়া,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, '78', '76', 'UPAZILA SOCIAL SERVICES OFFICE,MIRZAGANJ,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,মির্জাগঞ্জ,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, '78', '95', 'UPAZILA SOCIAL SERVICES OFFICE,PATUAKHALI SADAR,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,পটুয়াখালী সদর,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, '78', '97', 'UPAZILA SOCIAL SERVICES OFFICE,RANGABALI,PATUAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,রাঙ্গাবালি,পটুয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, '79', '14', 'UPAZILA SOCIAL SERVICES OFFICE,BHANDARIA,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,ভান্ডারিয়া,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, '79', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KAWKHALI,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,কাউখালী,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, '79', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MATHBARIA,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,মঠবাড়ীয়া,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, '79', '76', 'UPAZILA SOCIAL SERVICES OFFICE,NAZIRPUR,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,নাজিরপুর,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, '79', '80', 'UPAZILA SOCIAL SERVICES OFFICE,PIROJPUR SADAR,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,পিরোজপুর সদর,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, '79', '87', 'UPAZILA SOCIAL SERVICES OFFICE,NESARABAD (SWARUPKATI),PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,নেছারাবাদ,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, '79', '90', 'UPAZILA SOCIAL SERVICES OFFICE,ZIANAGAR,PIROJPUR', 'উপজেলা সমাজসেবা কার্যালয়,জিয়ানগর,পিরোজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, '03', '14', 'UPAZILA SOCIAL SERVICES OFFICE,BANDARBAN SADAR,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,বান্দরবান সদর,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, '03', '51', 'UPAZILA SOCIAL SERVICES OFFICE,LAMA,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,লামা,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, '03', '73', 'UPAZILA SOCIAL SERVICES OFFICE,NAIKHONGCHHARI,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,নাইক্ষ্যংছড়ি,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, '03', '89', 'UPAZILA SOCIAL SERVICES OFFICE,ROWANGCHHARI,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,রোয়াংছড়ি,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, '03', '91', 'UPAZILA SOCIAL SERVICES OFFICE,RUMA,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,রুমা,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, '03', '95', 'UPAZILA SOCIAL SERVICES OFFICE,THANCHI,BANDARBA', 'উপজেলা সমাজসেবা কার্যালয়,থানচি,বান্দরবান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, '12', '02', 'UPAZILA SOCIAL SERVICES OFFICE,AKHAURA,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,আখাউড়া,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, '12', '04', 'UPAZILA SOCIAL SERVICES OFFICE,BANCHHARAMPUR,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,বাঞ্ছারামপুর,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, '12', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BIJOYNAGAR,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,বিজয়নগর,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, '12', '13', 'UPAZILA SOCIAL SERVICES OFFICE,BRAHMANBARIA SADAR,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,ব্রাহ্মণবাড়িয়া সদর,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, '12', '33', 'UPAZILA SOCIAL SERVICES OFFICE,ASHUGANJ,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,আশুগঞ্জ,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, '12', '63', 'UPAZILA SOCIAL SERVICES OFFICE,KASBA,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,কসবা,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, '12', '85', 'UPAZILA SOCIAL SERVICES OFFICE,NABINAGAR,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,নবীনগর ,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, '12', '90', 'UPAZILA SOCIAL SERVICES OFFICE,NASIRNAGAR,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,নাসিরনগর,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, '12', '94', 'UPAZILA SOCIAL SERVICES OFFICE,SARAIL,BRAHMANBARIA', 'উপজেলা সমাজসেবা কার্যালয়,সরাইল,ব্রাহ্মণবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, '13', '22', 'UPAZILA SOCIAL SERVICES OFFICE,CHANDPUR SADAR,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,চাঁদপুর সদর,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, '13', '45', 'UPAZILA SOCIAL SERVICES OFFICE,FARIDGANJ,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,ফরিদগঞ্জ,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, '13', '47', 'UPAZILA SOCIAL SERVICES OFFICE,HAIM CHAR,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,হাইমচর,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, '13', '49', 'UPAZILA SOCIAL SERVICES OFFICE,HAJIGANJ,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,হাজীগঞ্জ,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, '13', '58', 'UPAZILA SOCIAL SERVICES OFFICE,KACHUA,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,কচুয়া,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, '13', '76', 'UPAZILA SOCIAL SERVICES OFFICE,MATLAB DAKSHIN,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,মতলব দক্ষিণ,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, '13', '79', 'UPAZILA SOCIAL SERVICES OFFICE,MATLAB UTTAR,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,মতলব উত্তর,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, '13', '95', 'UPAZILA SOCIAL SERVICES OFFICE,SHAHRASTI,CHANDPUR', 'উপজেলা সমাজসেবা কার্যালয়,শাহরাস্তি,চাঁদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, '15', '04', 'UPAZILA SOCIAL SERVICES OFFICE,ANOWARA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,আনোয়ারা,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, '15', '08', 'UPAZILA SOCIAL SERVICES OFFICE,BANSHKHALI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,বাশঁখালী,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, '15', '12', 'UPAZILA SOCIAL SERVICES OFFICE,BOALKHALI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,বোয়ালখালী,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, '15', '18', 'UPAZILA SOCIAL SERVICES OFFICE,CHANDANAISH,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,চন্দনাইশ,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, '15', '33', 'UPAZILA SOCIAL SERVICES OFFICE,FATIKCHARI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,ফটিকছড়ি,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, '15', '37', 'UPAZILA SOCIAL SERVICES OFFICE,HATHAZARI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,হাটহাজারী,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, '58', '56', 'UPAZILA SOCIAL SERVICES OFFICE,KAMALGANJ,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কমলগঞ্জ ,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, '86', '36', 'UPAZILA SOCIAL SERVICES OFFICE,GOSAIRHAT,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,গোসাইরহাট,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, '86', '36', 'UPAZILA SOCIAL SERVICES OFFICE,GOSAIRHAT,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,গোসাইরহাট,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, '86', '65', 'UPAZILA SOCIAL SERVICES OFFICE,NARIA,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,নড়িয়া ,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, '86', '69', 'UPAZILA SOCIAL SERVICES OFFICE,SHARIATPUR SADAR,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,শরীয়তপুর সদর,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, '86', '94', 'UPAZILA SOCIAL SERVICES OFFICE,ZANJIRA,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,জাজিরা ,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, '89', '37', 'UPAZILA SOCIAL SERVICES OFFICE,JHENAIGATI,SHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,ঝিনাইগাতি,শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, '89', '67', 'UPAZILA SOCIAL SERVICES OFFICE,NAKLA,SHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,নকলা,শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, '89', '70', 'UPAZILA SOCIAL SERVICES OFFICE,NALITABARI,SHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,নলিতাবাড়ী,শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, '89', '88', 'UPAZILA SOCIAL SERVICES OFFICE,SHERPUR SADAR,SHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,শেরপুর সদর,শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, '89', '90', 'UPAZILA SOCIAL SERVICES OFFICE,SREEBARDI,SHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,শ্রীবর্দী ,শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, '93', '09', 'UPAZILA SOCIAL SERVICES OFFICE,BASAIL,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,বাসাইল ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, '93', '19', 'UPAZILA SOCIAL SERVICES OFFICE,BHUAPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,ভুঞাপুর,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, '93', '23', 'UPAZILA SOCIAL SERVICES OFFICE,DELDUAR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,দেলদুয়ার,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, '93', '25', 'UPAZILA SOCIAL SERVICES OFFICE,DHANBARI,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,ধনবাড়ী ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, '93', '28', 'UPAZILA SOCIAL SERVICES OFFICE,GHATAIL,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,ঘাটাইল ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, '93', '38', 'UPAZILA SOCIAL SERVICES OFFICE,GOPALPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,গোপালপুর ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, '93', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KALIHATI,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,কালীহাতি ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, '93', '57', 'UPAZILA SOCIAL SERVICES OFFICE,MADHUPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,মধুপুর ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, '93', '66', 'UPAZILA SOCIAL SERVICES OFFICE,MIRZAPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,মির্জাপুর ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, '93', '76', 'UPAZILA SOCIAL SERVICES OFFICE,NAGARPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,নাগরপুর,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, '93', '85', 'UPAZILA SOCIAL SERVICES OFFICE,SAKHIPUR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,সখিপুর ,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, '93', '95', 'UPAZILA SOCIAL SERVICES OFFICE,TANGAIL SADAR,TANGAIL', 'উপজেলা সমাজসেবা কার্যালয়,টাঙ্গাইল সদর,টাঙ্গাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, '27', '10', 'UPAZILA SOCIAL SERVICES OFFICE,BIRAMPUR,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,বিরামপুর,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, '27', '12', 'UPAZILA SOCIAL SERVICES OFFICE,BIRGANJ,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,বীরগঞ্জ,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, '27', '17', 'UPAZILA SOCIAL SERVICES OFFICE,BIRAL,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,বিরল,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, '27', '21', 'UPAZILA SOCIAL SERVICES OFFICE,BOCHAGANJ,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,বোচাগঞ্জ,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, '27', '30', 'UPAZILA SOCIAL SERVICES OFFICE,CHIRIRBANDAR,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,চিরিরবন্দর,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, '27', '38', 'UPAZILA SOCIAL SERVICES OFFICE,FULBARI,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,ফুলবাড়ী,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, '27', '43', 'UPAZILA SOCIAL SERVICES OFFICE,GHORAGHAT,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,ঘোড়াঘাট,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, '27', '47', 'UPAZILA SOCIAL SERVICES OFFICE,HAKIMPUR,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,হাকিমপুর,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, '27', '56', 'UPAZILA SOCIAL SERVICES OFFICE,KAHAROLE,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,কাহারোল,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, '27', '60', 'UPAZILA SOCIAL SERVICES OFFICE,KHANSAMA,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,খানসামা,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, '27', '64', 'UPAZILA SOCIAL SERVICES OFFICE,DINAJPUR SADAR,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,দিনাজপুর সদর,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, '27', '69', 'UPAZILA SOCIAL SERVICES OFFICE,NAWABGANJ,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,নবাবগঞ্জ,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, '27', '77', 'UPAZILA SOCIAL SERVICES OFFICE,PARBATIPUR,DINAJPUR', 'উপজেলা সমাজসেবা কার্যালয়,পার্বতীপুর,দিনাজপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, '32', '21', 'UPAZILA SOCIAL SERVICES OFFICE,FULCHHARI,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,ফুলছড়ি,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, '32', '24', 'UPAZILA SOCIAL SERVICES OFFICE,GAIBANDHA SADAR,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,গাইবান্ধা সদর,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, '32', '30', 'UPAZILA SOCIAL SERVICES OFFICE,GOBINDAGANJ,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,গোবিন্দগঞ্জ,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, '32', '67', 'UPAZILA SOCIAL SERVICES OFFICE,PALASHBARI,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,পলাশবাড়ী,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, '32', '82', 'UPAZILA SOCIAL SERVICES OFFICE,SADULLAPUR,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,সাদুল্যাপুর,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, '32', '88', 'UPAZILA SOCIAL SERVICES OFFICE,SAGHATA,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,সাঘাটা,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, '32', '91', 'UPAZILA SOCIAL SERVICES OFFICE,SUNDARGANJ,GAIBANDHA', 'উপজেলা সমাজসেবা কার্যালয়,সুন্দরগঞ্জ,গাইবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, '49', '06', 'UPAZILA SOCIAL SERVICES OFFICE,BHURUNGAMARI,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,ভূরুঙ্গামারী,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, '49', '52', 'UPAZILA SOCIAL SERVICES OFFICE,KURIGRAM SADAR,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,কুড়িগ্রাম সদর,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, '49', '08', 'UPAZILA SOCIAL SERVICES OFFICE,CHAR RAJIBPUR,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,রাজিবপুর,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, '49', '09', 'UPAZILA SOCIAL SERVICES OFFICE,CHILMARI,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,চিলমারী,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, '49', '61', 'UPAZILA SOCIAL SERVICES OFFICE,NAGESHWARI,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,নাগেশ্বরী,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, '49', '77', 'UPAZILA SOCIAL SERVICES OFFICE,RAJARHAT,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,রাজারহাট,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, '49', '79', 'UPAZILA SOCIAL SERVICES OFFICE,RAUMARI,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,রৌমারী,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, '49', '94', 'UPAZILA SOCIAL SERVICES OFFICE,ULIPUR,KURIGRAM', 'উপজেলা সমাজসেবা কার্যালয়,উলিপুর ,কুড়িগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, '52', '02', 'UPAZILA SOCIAL SERVICES OFFICE,ADITMARI,LALMONIRHAT', 'উপজেলা সমাজসেবা কার্যালয়,আদিতমারী,লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, '52', '33', 'UPAZILA SOCIAL SERVICES OFFICE,HATIBANDHA,LALMONIRHAT', 'উপজেলা সমাজসেবা কার্যালয়,হাতীবান্ধা ,লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, '52', '39', 'UPAZILA SOCIAL SERVICES OFFICE,KALIGANJ,LALMONIRHAT', 'উপজেলা সমাজসেবা কার্যালয়,কালীগঞ্জ,লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, '52', '55', 'UPAZILA SOCIAL SERVICES OFFICE,LALMONIRHAT SADAR,LALMONIRHAT', 'উপজেলা সমাজসেবা কার্যালয়,লালমনিরহাট সদর,লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, '52', '70', 'UPAZILA SOCIAL SERVICES OFFICE,PATGRAM,LALMONIRHAT', 'উপজেলা সমাজসেবা কার্যালয়,পাটগ্রাম,লালমনিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, '73', '12', 'UPAZILA SOCIAL SERVICES OFFICE,DIMLA UPAZILA,NILPHAMARI ZILA', 'উপজেলা সমাজসেবা কার্যালয়,ডিমলা,নীলফামারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, '73', '36', 'UPAZILA SOCIAL SERVICES OFFICE,JALDHAKA UPAZILA,NILPHAMARI ZILA', 'উপজেলা সমাজসেবা কার্যালয়,জলঢাকা,নীলফামারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, '73', '45', 'UPAZILA SOCIAL SERVICES OFFICE,KISHOREGANJ UPAZILA,NILPHAMARI ZILA', 'উপজেলা সমাজসেবা কার্যালয়,কিশোরগঞ্জ,নীলফামারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, '73', '64', 'UPAZILA SOCIAL SERVICES OFFICE,NILPHAMARI SADAR UPAZ,NILPHAMARI ZILA', 'উপজেলা সমাজসেবা কার্যালয়,নীলফামারী সদর,নীলফামারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, '77', '04', 'UPAZILA SOCIAL SERVICES OFFICE,ATWARI,PANCHAGARH', 'উপজেলা সমাজসেবা কার্যালয়,আটোয়ারী,পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, '77', '25', 'UPAZILA SOCIAL SERVICES OFFICE,BODA,PANCHAGARH', 'উপজেলা সমাজসেবা কার্যালয়,বোদা,পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, '77', '34', 'UPAZILA SOCIAL SERVICES OFFICE,DEBIGANJ,PANCHAGARH', 'উপজেলা সমাজসেবা কার্যালয়,দেবীগঞ্জ,পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, '77', '73', 'UPAZILA SOCIAL SERVICES OFFICE,PANCHAGARH SADAR,PANCHAGARH', 'উপজেলা সমাজসেবা কার্যালয়,পঞ্চগড় সদর,পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, '77', '90', 'UPAZILA SOCIAL SERVICES OFFICE,TENTULIA,PANCHAGARH', 'উপজেলা সমাজসেবা কার্যালয়,তেঁতুলিয়া,পঞ্চগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, '85', '03', 'UPAZILA SOCIAL SERVICES OFFICE,BADARGANJ,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,বদরগঞ্জ,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, '85', '27', 'UPAZILA SOCIAL SERVICES OFFICE,GANGACHARA,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,গঙ্গাচড়া,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, '85', '42', 'UPAZILA SOCIAL SERVICES OFFICE,KAUNIA,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,কাউনিয়া,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, '85', '49', 'UPAZILA SOCIAL SERVICES OFFICE,RANGPUR SADAR,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,রংপুর সদর,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, '85', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MITHA PUKUR,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,মিঠাপুকুর,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, '72', '63', 'UPAZILA SOCIAL SERVICES OFFICE,MOHANGANJ,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,মোহনগঞ্জ,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, '72', '74', 'UPAZILA SOCIAL SERVICES OFFICE,NETROKONA SADAR,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,নেত্রকোনা সদর,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, '72', '83', 'UPAZILA SOCIAL SERVICES OFFICE,PURBADHALA,NETRAKONA', 'উপজেলা সমাজসেবা কার্যালয়,পূর্বধলা,নেত্রকোনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, '82', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BALIAKANDI,RAJBARI', 'উপজেলা সমাজসেবা কার্যালয়,বালিয়াকান্দি,রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, '82', '29', 'UPAZILA SOCIAL SERVICES OFFICE,GOALANDA,RAJBARI', 'উপজেলা সমাজসেবা কার্যালয়,গোয়ালন্দ,রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, '82', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KALUKHALI,RAJBARI', 'উপজেলা সমাজসেবা কার্যালয়,কালুখালি,রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, '82', '73', 'UPAZILA SOCIAL SERVICES OFFICE,PANGSHA,RAJBARI', 'উপজেলা সমাজসেবা কার্যালয়,পাংশা ,রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, '82', '76', 'UPAZILA SOCIAL SERVICES OFFICE,RAJBARI SADAR,RAJBARI', 'উপজেলা সমাজসেবা কার্যালয়,রাজবাড়ী সদর,রাজবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, '86', '14', 'UPAZILA SOCIAL SERVICES OFFICE,BHEDARGANJ,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,ভেদরগঞ্জ ,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, '86', '25', 'UPAZILA SOCIAL SERVICES OFFICE,DAMUDYA,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,ডামুড্যা ,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, '86', '36', 'UPAZILA SOCIAL SERVICES OFFICE,GOSAIRHAT,SHARIATPUR', 'উপজেলা সমাজসেবা কার্যালয়,গোসাইরহাট,শরীয়তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, '84', '47', 'UPAZILA SOCIAL SERVICES OFFICE,JURAI CHHARI UPAZIL,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,জুড়াছড়ি,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, '84', '58', 'UPAZILA SOCIAL SERVICES OFFICE,LANGADU  UPAZILA,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,লংগদু,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, '84', '75', 'UPAZILA SOCIAL SERVICES OFFICE,NANIARCHAR  UPAZILA,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,নানিয়ারচর,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, '84', '78', 'UPAZILA SOCIAL SERVICES OFFICE,RAJASTHALI  UPAZILA,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,রাজস্থলী,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, '84', '87', 'UPAZILA SOCIAL SERVICES OFFICE,RANGAMATI SADAR  UP,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,রাঙ্গামাটি সদর,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, '01', '08', 'UPAZILA SOCIAL SERVICES OFFICE,BAGERHAT SADAR,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,বাগেরহাট সদর,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, '01', '14', 'UPAZILA SOCIAL SERVICES OFFICE,CHITALMARI,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,চিতলমারী,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, '01', '34', 'UPAZILA SOCIAL SERVICES OFFICE,FAKIRHAT,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,ফকিরহাট,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, '01', '38', 'UPAZILA SOCIAL SERVICES OFFICE,KACHUA,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,কচুয়া,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, '01', '56', 'UPAZILA SOCIAL SERVICES OFFICE,MOLLAHAT,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,মোল্লাহাট,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, '01', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MONGLA,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,মংলা,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, '01', '60', 'UPAZILA SOCIAL SERVICES OFFICE,MORRELGANJ,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,মোড়েলগঞ্জ,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, '01', '73', 'UPAZILA SOCIAL SERVICES OFFICE,RAMPAL,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,রামপাল,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, '01', '77', 'UPAZILA SOCIAL SERVICES OFFICE,SARANKHOLA,BAGERHAT', 'উপজেলা সমাজসেবা কার্যালয়,শরণখোলা,বাগেরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, '18', '07', 'UPAZILA SOCIAL SERVICES OFFICE,ALAMDANGA,CHUADANGA', 'উপজেলা সমাজসেবা কার্যালয়,আলমডাংগা,চুয়াডাংগা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, '18', '23', 'UPAZILA SOCIAL SERVICES OFFICE,CHUADANGA SADAR,CHUADANGA', 'উপজেলা সমাজসেবা কার্যালয়,চুয়াডাংগা সদর,চুয়াডাংগা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, '18', '31', 'UPAZILA SOCIAL SERVICES OFFICE,DAMURHUDA,CHUADANGA', 'উপজেলা সমাজসেবা কার্যালয়,দামুড়হুদা ,চুয়াডাংগা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, '18', '55', 'UPAZILA SOCIAL SERVICES OFFICE,JIBAN NAGAR,CHUADANGA', 'উপজেলা সমাজসেবা কার্যালয়,জীবননগর,চুয়াডাংগা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, '41', '04', 'UPAZILA SOCIAL SERVICES OFFICE,ABHAYNAGAR,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,অভয়নগর,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, '41', '09', 'UPAZILA SOCIAL SERVICES OFFICE,BAGHER PARA,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,বাঘার পাড়া,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, '41', '11', 'UPAZILA SOCIAL SERVICES OFFICE,CHAUGACHHA,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,চৌগাছা,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, '41', '23', 'UPAZILA SOCIAL SERVICES OFFICE,JHIKARGACHHA,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,ঝিকরগাছা,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, '41', '38', 'UPAZILA SOCIAL SERVICES OFFICE,KESHABPUR,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,কেশবপুর,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, '41', '47', 'UPAZILA SOCIAL SERVICES OFFICE,JESSORE SADAR,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,যশোর সদর,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, '41', '61', 'UPAZILA SOCIAL SERVICES OFFICE,MANIRAMPUR,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,মনিরামপুর,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, '41', '90', 'UPAZILA SOCIAL SERVICES OFFICE,SHARSHA,JESSORE', 'উপজেলা সমাজসেবা কার্যালয়,শার্শা,যশোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, '44', '14', 'UPAZILA SOCIAL SERVICES OFFICE,HARINAKUNDA,JHENAIDAH', 'উপজেলা সমাজসেবা কার্যালয়,হরিণাকুন্ডু,ঝিনাইদহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, '44', '19', 'UPAZILA SOCIAL SERVICES OFFICE,JHENAIDAH SADAR,JHENAIDAH', 'উপজেলা সমাজসেবা কার্যালয়,ঝিনাইদহ সদর,ঝিনাইদহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, '44', '33', 'UPAZILA SOCIAL SERVICES OFFICE,KALIGANJ,JHENAIDAH', 'উপজেলা সমাজসেবা কার্যালয়,কালীগঞ্জ,ঝিনাইদহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, '44', '42', 'UPAZILA SOCIAL SERVICES OFFICE,KOTCHANDPUR,JHENAIDAH', 'উপজেলা সমাজসেবা কার্যালয়,কোটচাঁদপুর,ঝিনাইদহ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, '47', '12', 'UPAZILA SOCIAL SERVICES OFFICE,BATIAGHATA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,বটিয়াঘাটা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, '47', '17', 'UPAZILA SOCIAL SERVICES OFFICE,DACOPE,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,দাকোপ,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, '47', '40', 'UPAZILA SOCIAL SERVICES OFFICE,,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,দিঘলিয়া,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, '47', '30', 'UPAZILA SOCIAL SERVICES OFFICE,DUMURIA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,ডুমুরিয়া,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, '85', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MITHA PUKUR,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,মিঠাপুকুর,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, '85', '73', 'UPAZILA SOCIAL SERVICES OFFICE,PIRGACHHA,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,পীরগাছা,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, '85', '76', 'UPAZILA SOCIAL SERVICES OFFICE,PIRGANJ,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,পীরগঞ্জ,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, '85', '92', 'UPAZILA SOCIAL SERVICES OFFICE,TARAGANJ,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,তারাগঞ্জ,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, '94', '08', 'UPAZILA SOCIAL SERVICES OFFICE,BALIADANGI,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,বালিয়াডাংগী ,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, '94', '51', 'UPAZILA SOCIAL SERVICES OFFICE,HARIPUR,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,হরিপুর,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, '94', '82', 'UPAZILA SOCIAL SERVICES OFFICE,PIRGANJ,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,পীরগঞ্জ,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, '94', '86', 'UPAZILA SOCIAL SERVICES OFFICE,RANISANKAIL,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,রানীশংকৈল,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, '94', '94', 'UPAZILA SOCIAL SERVICES OFFICE,THAKURGAON SADAR,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,ঠাকুরগাঁও সদর,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, '94', '94', 'UPAZILA SOCIAL SERVICES OFFICE,THAKURGAON SADAR,THAKURGAO', 'উপজেলা সমাজসেবা কার্যালয়,ঠাকুরগাঁও সদর,ঠাকুরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, '47', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TEROKHADA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,তেরখাদা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, '87', '43', 'UPAZILA SOCIAL SERVICES OFFICE,KALAROA,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,কলারোয়া,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, '10', '54', 'UPAZILA SOCIAL SERVICES OFFICE,KAHALOO,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,কাহালু,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, '10', '67', 'UPAZILA SOCIAL SERVICES OFFICE,NANDIGRAM,BOGRA', 'উপজেলা সমাজসেবা কার্যালয় নন্দিগ্রাম,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, '10', '81', 'UPAZILA SOCIAL SERVICES OFFICE,SARIAKANDI,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,সারিয়াকান্দি,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, '10', '85', 'UPAZILA SOCIAL SERVICES OFFICE,SHAJAHANPUR,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,শাহজাহানপুর,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, '10', '88', 'UPAZILA SOCIAL SERVICES OFFICE,SHERPUR,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,শেরপুর,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, '10', '94', 'UPAZILA SOCIAL SERVICES OFFICE,SHIBGANJ,BOGRA', 'উপজেলা সমাজসেবা কার্যালয়,শিবগঞ্জ,বগুড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, '38', '13', 'UPAZILA SOCIAL SERVICES OFFICE,AKKELPUR,JOYPURHAT', 'উপজেলা সমাজসেবা কার্যালয়,আক্কেলপুর,জয়পুরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, '38', '58', 'UPAZILA SOCIAL SERVICES OFFICE,KALAI,JOYPURHAT', 'উপজেলা সমাজসেবা কার্যালয়,কালাই,জয়পুরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, '38', '74', 'UPAZILA SOCIAL SERVICES OFFICE,PANCHBIBI,JOYPURHAT', 'উপজেলা সমাজসেবা কার্যালয়,পাঁচবিবি,জয়পুরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, '64', '03', 'UPAZILA SOCIAL SERVICES OFFICE,ATRAI,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,আত্রাই,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, '64', '06', 'UPAZILA SOCIAL SERVICES OFFICE,BADALGACHHI,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,বদলগাছী,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, '64', '28', 'UPAZILA SOCIAL SERVICES OFFICE,DHAMOIRHAT,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,ধামুইরহাট,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, '64', '47', 'UPAZILA SOCIAL SERVICES OFFICE,MANDA,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,মান্দা,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, '64', '50', 'UPAZILA SOCIAL SERVICES OFFICE,MAHADEBPUR,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,মহাদেবপুর,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, '64', '60', 'UPAZILA SOCIAL SERVICES OFFICE,NAOGAON SADAR,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,নওগাঁ সদর,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, '64', '69', 'UPAZILA SOCIAL SERVICES OFFICE,NIAMATPUR,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,নিয়ামতপুর,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, '64', '75', 'UPAZILA SOCIAL SERVICES OFFICE,PATNITALA,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,পত্নীতলা,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, '64', '79', 'UPAZILA SOCIAL SERVICES OFFICE,PORSHA,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,পোরশা,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, '64', '85', 'UPAZILA SOCIAL SERVICES OFFICE,RANINAGAR,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,রানীনগর,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, '64', '86', 'UPAZILA SOCIAL SERVICES OFFICE,SAPAHAR,NAOGAO', 'উপজেলা সমাজসেবা কার্যালয়,সাপাহার,নওগাঁ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, '69', '09', 'UPAZILA SOCIAL SERVICES OFFICE,BAGATIPARA,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,বাগাতিপাড়া,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, '69', '15', 'UPAZILA SOCIAL SERVICES OFFICE,BARAIGRAM,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,বড়াইগ্রাম,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, '69', '44', 'UPAZILA SOCIAL SERVICES OFFICE,LALPUR,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,লালপুর,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, '69', '55', 'UPAZILA SOCIAL SERVICES OFFICE,NALDANGA,NATORE', 'উপজেলা সমাজসেবা কার্যালয়, নলডাঙ্গা নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, '69', '63', 'UPAZILA SOCIAL SERVICES OFFICE,NATORE SADAR,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,নাটোর সদর,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, '69', '55', 'UPAZILA SOCIAL SERVICES OFFICE,NALDANGA,NATORE', 'উপজেলা সমাজসেবা কার্যালয়, নলডাঙ্গা নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, '69', '63', 'UPAZILA SOCIAL SERVICES OFFICE,NATORE SADAR,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,নাটোর সদর,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, '69', '55', 'UPAZILA SOCIAL SERVICES OFFICE,NALDANGA,NATORE', 'উপজেলা সমাজসেবা কার্যালয়, নলডাঙ্গা নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, '69', '63', 'UPAZILA SOCIAL SERVICES OFFICE,NATORE SADAR,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,নাটোর সদর,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, '69', '63', 'UPAZILA SOCIAL SERVICES OFFICE,NATORE SADAR,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,নাটোর সদর,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, '69', '91', 'UPAZILA SOCIAL SERVICES OFFICE,SINGRA,NATORE', 'উপজেলা সমাজসেবা কার্যালয়,সিংড়া,নাটোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, '70', '18', 'UPAZILA SOCIAL SERVICES OFFICE,BHOLAHAT,CHAPAINAWABGANJ', 'উপজেলা সমাজসেবা কার্যালয়,ভোলাহাট,চাঁপাইনবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, '70', '37', 'UPAZILA SOCIAL SERVICES OFFICE,GOMASTAPUR,CHAPAINAWABGANJ', 'উপজেলা সমাজসেবা কার্যালয়,গোমস্তাপুর,চাঁপাইনবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, '70', '66', 'UPAZILA SOCIAL SERVICES OFFICE,CHAPAI NABABGANJ SADAR,CHAPAINAWABGANJ', 'উপজেলা সমাজসেবা কার্যালয়,চাঁপাইনবাবগঞ্জ সদর,চাঁপাইনবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `offices` (`id`, `dist_id`, `upza_id`, `name_en`, `name_bd`, `created_at`, `updated_at`) VALUES
(307, '76', '05', 'UPAZILA SOCIAL SERVICES OFFICE,ATGHARIA,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,আটঘরিয়া,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, '76', '16', 'UPAZILA SOCIAL SERVICES OFFICE,BERA,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,বেড়া,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, '76', '19', 'UPAZILA SOCIAL SERVICES OFFICE,BHANGURA,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,ভাঙ্গুরা,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, '76', '22', 'UPAZILA SOCIAL SERVICES OFFICE,CHATMOHAR,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,চাটমোহর,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, '76', '33', 'UPAZILA SOCIAL SERVICES OFFICE,FARIDPUR,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,ফরিদপুর,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, '76', '55', 'UPAZILA SOCIAL SERVICES OFFICE,PABNA SADAR,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,পাবনা সদর,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, '76', '72', 'UPAZILA SOCIAL SERVICES OFFICE,SANTHIA,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,সাথিয়া,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, '76', '83', 'UPAZILA SOCIAL SERVICES OFFICE,SUJANAGAR,PABNA', 'উপজেলা সমাজসেবা কার্যালয়,সুজানগর,পাবনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, '81', '10', 'UPAZILA SOCIAL SERVICES OFFICE,BAGHA,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,বাঘা,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, '81', '31', 'UPAZILA SOCIAL SERVICES OFFICE,DURGAPUR,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,দুর্গাপুর,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, '81', '31', 'UPAZILA SOCIAL SERVICES OFFICE,DURGAPUR,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,দুর্গাপুর,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, '81', '34', 'UPAZILA SOCIAL SERVICES OFFICE,GODAGARI,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,গোদাগাড়ী,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, '81', '53', 'UPAZILA SOCIAL SERVICES OFFICE,MOHANPUR,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,মোহনপুর,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, '81', '72', 'UPAZILA SOCIAL SERVICES OFFICE,PABA,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,পবা,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, '81', '82', 'UPAZILA SOCIAL SERVICES OFFICE,PUTHIA,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,পুঠিয়া,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, '81', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TANORE,RAJSHAHI', 'উপজেলা সমাজসেবা কার্যালয়,তানোর,রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, '88', '27', 'UPAZILA SOCIAL SERVICES OFFICE,CHAUHALI,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,চৌহালি,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, '88', '44', 'UPAZILA SOCIAL SERVICES OFFICE,KAMARKHANDA,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,কামারখন্দ,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, '88', '50', 'UPAZILA SOCIAL SERVICES OFFICE,KAZIPUR,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,কাজীপুর,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, '88', '61', 'UPAZILA SOCIAL SERVICES OFFICE,ROYGANJ,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,রায়গঞ্জ,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, '88', '67', 'UPAZILA SOCIAL SERVICES OFFICE,SHAHJADPUR,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,শাহজাদপুর,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, '88', '78', 'UPAZILA SOCIAL SERVICES OFFICE,SIRAJGANJ SADAR,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সিরাজগঞ্জ সদর,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, '88', '89', 'UPAZILA SOCIAL SERVICES OFFICE,TARASH,SIRAJGANJ', 'উপজেলা সমাজসেবা কার্যালয়,তাড়াশ,সিরাজগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, '36', '02', 'UPAZILA SOCIAL SERVICES OFFICE,AJMIRIGANJ,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,আজমেরীগঞ্জ,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, '36', '05', 'UPAZILA SOCIAL SERVICES OFFICE,BAHUBAL,HABIGANJ', 'উপজেলা সমাজসেবা কার্যালয়,বাহুবল,হবিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, '47', '53', 'UPAZILA SOCIAL SERVICES OFFICE,KOYRA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,কয়রা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, '47', '64', 'UPAZILA SOCIAL SERVICES OFFICE,PAIKGACHA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,পাইকগাছা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, '47', '69', 'UPAZILA SOCIAL SERVICES OFFICE,PHULTALA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,ফুলতলা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, '47', '75', 'UPAZILA SOCIAL SERVICES OFFICE,RUPSA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,রূপসা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, '47', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TEROKHADA,KHULNA', 'উপজেলা সমাজসেবা কার্যালয়,তেরখাদা,খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, '50', '15', 'UPAZILA SOCIAL SERVICES OFFICE,BHERAMARA,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,ভেড়ামারা,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, '50', '39', 'UPAZILA SOCIAL SERVICES OFFICE,DAULATPUR,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,দৌলতপুর,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, '50', '63', 'UPAZILA SOCIAL SERVICES OFFICE,KHOKSA,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,খোকসা,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, '50', '71', 'UPAZILA SOCIAL SERVICES OFFICE,KUMARKHALI,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,কুমারখালী,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, '50', '79', 'UPAZILA SOCIAL SERVICES OFFICE,KUSHTIA SADAR,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,কুষ্টিয়া সদর,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, '50', '94', 'UPAZILA SOCIAL SERVICES OFFICE,MIRPUR,KUSHTIA', 'উপজেলা সমাজসেবা কার্যালয়,মিরপুর,কুষ্টিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, '55', '57', 'UPAZILA SOCIAL SERVICES OFFICE,MAGURA SADAR,MAGURA', 'উপজেলা সমাজসেবা কার্যালয়,মাগুরা সদর,মাগুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, '55', '66', 'UPAZILA SOCIAL SERVICES OFFICE,MOHAMMADPUR,MAGURA', 'উপজেলা সমাজসেবা কার্যালয়,মোহাম্মদপুর,মাগুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, '55', '85', 'UPAZILA SOCIAL SERVICES OFFICE,SHALIKHA,MAGURA', 'উপজেলা সমাজসেবা কার্যালয়,শালিখা,মাগুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, '55', '95', 'UPAZILA SOCIAL SERVICES OFFICE,SREEPUR,MAGURA', 'উপজেলা সমাজসেবা কার্যালয়,শ্রীপুর,মাগুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, '57', '47', 'UPAZILA SOCIAL SERVICES OFFICE,GANGNI,MEHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,গাংনী,মেহেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, '57', '60', 'UPAZILA SOCIAL SERVICES OFFICE,MUJIB NAGAR,MEHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,মুজিবনগর,মেহেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, '57', '87', 'UPAZILA SOCIAL SERVICES OFFICE,MEHERPUR SADAR,MEHERPUR', 'উপজেলা সমাজসেবা কার্যালয়,মেহেরপুর সদর,মেহেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, '65', '28', 'UPAZILA SOCIAL SERVICES OFFICE,KALIA,NARAIL', 'উপজেলা সমাজসেবা কার্যালয়,কালিয়া,নড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, '65', '52', 'UPAZILA SOCIAL SERVICES OFFICE,LOHAGARA,NARAIL', 'উপজেলা সমাজসেবা কার্যালয়,লোহাগড়া,নড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, '65', '76', 'UPAZILA SOCIAL SERVICES OFFICE,NARAIL SADAR,NARAIL', 'উপজেলা সমাজসেবা কার্যালয়,নড়াইল সদর,নড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, '87', '04', 'UPAZILA SOCIAL SERVICES OFFICE,ASSASUNI,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,আশাশুনি,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, '87', '25', 'UPAZILA SOCIAL SERVICES OFFICE,DEBHATA,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,দেবহাটা,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, '87', '43', 'UPAZILA SOCIAL SERVICES OFFICE,KALAROA,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,কলারোয়া,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, '87', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KALIGANJ,SATKHIRA', 'উপজেলা সমাজসেবা কার্যালয়,কালীগঞ্জ,সাতক্ষীরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, '15', '37', 'UPAZILA SOCIAL SERVICES OFFICE,HATHAZARI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,হাটহাজারী,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, '15', '39', 'UPAZILA SOCIAL SERVICES OFFICE,PATIYA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,পটিয়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, '15', '47', 'UPAZILA SOCIAL SERVICES OFFICE,LOHAGARA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,লোহাগড়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, '15', '53', 'UPAZILA SOCIAL SERVICES OFFICE,MIRSHARAI,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,মিরসরাই,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, '15', '70', 'UPAZILA SOCIAL SERVICES OFFICE,RANGUNIA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,রাংগুনিয়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, '15', '70', 'UPAZILA SOCIAL SERVICES OFFICE,RANGUNIA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,রাংগুনিয়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, '15', '70', 'UPAZILA SOCIAL SERVICES OFFICE,RANGUNIA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,রাংগুনিয়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, '15', '74', 'UPAZILA SOCIAL SERVICES OFFICE,RAOZAN,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,রাউজান ,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, '15', '82', 'UPAZILA SOCIAL SERVICES OFFICE,SATKANIA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,সাতকানিয়া,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, '15', '86', 'UPAZILA SOCIAL SERVICES OFFICE,SITAKUNDA,CHITTAGONG', 'উপজেলা সমাজসেবা কার্যালয়,সীতাকুন্ড,চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, '19', '09', 'UPAZILA SOCIAL SERVICES OFFICE,BARURA,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,বরুড়া,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, '19', '15', 'UPAZILA SOCIAL SERVICES OFFICE,BRAHMAN PARA,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,ব্রাহ্মণপাড়া,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, '19', '18', 'UPAZILA SOCIAL SERVICES OFFICE,BURICHANG,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,বুড়ীচং,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, '19', '27', 'UPAZILA SOCIAL SERVICES OFFICE,CHANDINA,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,চান্দিনা,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, '19', '31', 'UPAZILA SOCIAL SERVICES OFFICE,CHAUDDAGRAM,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,চৌদ্দগ্রাম,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, '19', '33', 'UPAZILA SOCIAL SERVICES OFFICE,COMILLA SADAR DAKSHIN,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,সদর দক্ষিণ,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, '19', '36', 'UPAZILA SOCIAL SERVICES OFFICE,DAUDKANDI,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,দাউদকান্দি,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, '19', '40', 'UPAZILA SOCIAL SERVICES OFFICE,DEBIDWAR,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,দেবীদ্বার ,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, '19', '54', 'UPAZILA SOCIAL SERVICES OFFICE,HOMNA,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,হোমনা ,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, '19', '67', 'UPAZILA SOCIAL SERVICES OFFICE,COMILLA ADARSHA SADAR,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়, আদর্শ সদর,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, '19', '72', 'UPAZILA SOCIAL SERVICES OFFICE,LAKSAM,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,লাকসাম,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, '19', '74', 'UPAZILA SOCIAL SERVICES OFFICE,MANOHARGANJ,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,মনোহরগঞ্জ,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, '19', '75', 'UPAZILA SOCIAL SERVICES OFFICE,MEGHNA,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,মেঘনা,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, '19', '81', 'UPAZILA SOCIAL SERVICES OFFICE,MURADNAGAR,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,মুরাদনগর,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, '19', '87', 'UPAZILA SOCIAL SERVICES OFFICE,NANGALKOT,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,নাংগলকোট,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, '19', '94', 'UPAZILA SOCIAL SERVICES OFFICE,TITAS,COMILLA', 'উপজেলা সমাজসেবা কার্যালয়,তিতাস,কুমিল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, '22', '16', 'UPAZILA SOCIAL SERVICES OFFICE,CHAKARIA,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,চকরিয়া,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, '22', '24', 'UPAZILA SOCIAL SERVICES OFFICE,COX\'S BAZAR SADAR,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কক্সবাজার সদর,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, '22', '45', 'UPAZILA SOCIAL SERVICES OFFICE,KUTUBDIA,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কুতুবদিয়া,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, '22', '49', 'UPAZILA SOCIAL SERVICES OFFICE,MAHESHKHALI,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,মহেশখালী,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, '22', '56', 'UPAZILA SOCIAL SERVICES OFFICE,PEKUA,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,পেকুয়া,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, '22', '66', 'UPAZILA SOCIAL SERVICES OFFICE,RAMU,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,রামু,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, '22', '90', 'UPAZILA SOCIAL SERVICES OFFICE,TEKNAF,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,টেকনাফ ,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, '22', '94', 'UPAZILA SOCIAL SERVICES OFFICE,UKHIA,COX\'S BAZAR', 'উপজেলা সমাজসেবা কার্যালয়,উখিয়া,কক্সবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, '30', '14', 'UPAZILA SOCIAL SERVICES OFFICE,CHHAGALNAIYA,FENI', 'উপজেলা সমাজসেবা কার্যালয়,ছাগলনাইয়া,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, '30', '25', 'UPAZILA SOCIAL SERVICES OFFICE,DAGANBHUIYAN,FENI', 'উপজেলা সমাজসেবা কার্যালয়,দাগনভূঞা,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, '30', '29', 'UPAZILA SOCIAL SERVICES OFFICE,FENI SADAR,FENI', 'উপজেলা সমাজসেবা কার্যালয়,ফেনী সদর,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, '30', '41', 'UPAZILA SOCIAL SERVICES OFFICE,FULGAZI,FENI', 'উপজেলা সমাজসেবা কার্যালয়,ফুলগাজী,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, '30', '51', 'UPAZILA SOCIAL SERVICES OFFICE,PARSHURAM,FENI', 'উপজেলা সমাজসেবা কার্যালয়,পরশুরাম,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, '30', '94', 'UPAZILA SOCIAL SERVICES OFFICE,SONAGAZI,FENI', 'উপজেলা সমাজসেবা কার্যালয়,সোনাগাজী,ফেনী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, '46', '43', 'UPAZILA SOCIAL SERVICES OFFICE,DIGHINALA,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,দীঘিনালা,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, '46', '49', 'UPAZILA SOCIAL SERVICES OFFICE,KHAGRACHHARI SADAR,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,খাগড়াছড়ি সদর,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, '46', '61', 'UPAZILA SOCIAL SERVICES OFFICE,LAKSHMICHHARI,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,লক্ষীছড়ি,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, '46', '67', 'UPAZILA SOCIAL SERVICES OFFICE,MANIKCHHARI,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,মানিকছড়ি,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, '46', '70', 'UPAZILA SOCIAL SERVICES OFFICE,MATIRANGA,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,মাটিরাংগা,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, '46', '77', 'UPAZILA SOCIAL SERVICES OFFICE,PANCHHARI,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,পানছড়ি,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, '46', '77', 'UPAZILA SOCIAL SERVICES OFFICE,PANCHHARI,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,পানছড়ি,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, '46', '80', 'UPAZILA SOCIAL SERVICES OFFICE,RAMGARH,KHAGRACHHARI', 'উপজেলা সমাজসেবা কার্যালয়,রামগড়,খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, '51', '33', 'UPAZILA SOCIAL SERVICES OFFICE,KAMALNAGAR,LAKSHMIPUR', 'উপজেলা সমাজসেবা কার্যালয়,কমলনগর,লক্ষ্মীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, '51', '43', 'UPAZILA SOCIAL SERVICES OFFICE,LAKSHMIPUR SADAR,LAKSHMIPUR', 'উপজেলা সমাজসেবা কার্যালয়,লক্ষ্মীপুর সদর,লক্ষ্মীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, '51', '58', 'UPAZILA SOCIAL SERVICES OFFICE,ROYPUR,LAKSHMIPUR', 'উপজেলা সমাজসেবা কার্যালয়,রায়পুর ,লক্ষ্মীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, '51', '73', 'UPAZILA SOCIAL SERVICES OFFICE,RAMGATI,LAKSHMIPUR', 'উপজেলা সমাজসেবা কার্যালয়,রামগতি ,লক্ষ্মীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, '75', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BEGUMGANJ,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,বেগমগঞ্জ,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, '75', '10', 'UPAZILA SOCIAL SERVICES OFFICE,CHATKHIL,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,চাটখিল,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, '75', '21', 'UPAZILA SOCIAL SERVICES OFFICE,COMPANIGANJ,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,কোম্পানীগঞ্জ,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, '75', '36', 'UPAZILA SOCIAL SERVICES OFFICE,HATIYA,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,হাতিয়া,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, '75', '47', 'UPAZILA SOCIAL SERVICES OFFICE,KABIRHAT,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,কবিরহাট,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, '75', '80', 'UPAZILA SOCIAL SERVICES OFFICE,SENBAGH,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,সেনবাগ,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, '75', '83', 'UPAZILA SOCIAL SERVICES OFFICE,SONAIMURI,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,সোনাইমুড়ি,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, '75', '85', 'UPAZILA SOCIAL SERVICES OFFICE,SUBARNACHAR,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,সুবর্ণচর,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, '75', '87', 'UPAZILA SOCIAL SERVICES OFFICE,NOAKHALI SADAR,NOAKHALI', 'উপজেলা সমাজসেবা কার্যালয়,নোয়াখালী সদর,নোয়াখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, '84', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BAGHAICHHARI,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,বাঘাইছড়ি,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, '84', '21', 'UPAZILA SOCIAL SERVICES OFFICE,BARKAL UPAZILA,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,বরকল,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, '84', '29', 'UPAZILA SOCIAL SERVICES OFFICE,BELAI CHHARI  UPAZI,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,বিলাইছড়ি,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, '84', '36', 'UPAZILA SOCIAL SERVICES OFFICE,KAPTAI  UPAZILA,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,কাপ্তাই,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, '84', '47', 'UPAZILA SOCIAL SERVICES OFFICE,JURAI CHHARI UPAZIL,RANGAMATI', 'উপজেলা সমাজসেবা কার্যালয়,জুড়াছড়ি,রাঙ্গামাটি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, '04', '09', 'UPAZILA SOCIAL SERVICES OFFICE,AMTALI,BARGUNA', 'উপজেলা সমাজসেবা কার্যালয়,আমতলী,বরগুনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, '58', '56', 'UPAZILA SOCIAL SERVICES OFFICE,KAMALGANJ,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কমলগঞ্জ ,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, '58', '65', 'UPAZILA SOCIAL SERVICES OFFICE,KULAURA,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,কুলাউড়া,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, '58', '74', 'UPAZILA SOCIAL SERVICES OFFICE,MAULVIBAZAR SADAR,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,মৌলভীবাজার সদর,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, '58', '80', 'UPAZILA SOCIAL SERVICES OFFICE,RAJNAGAR,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,রাজনগর,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, '58', '83', 'UPAZILA SOCIAL SERVICES OFFICE,SREEMANGAL,MAULVIBAZAR', 'উপজেলা সমাজসেবা কার্যালয়,শ্রীমঙ্গল,মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, '90', '18', 'UPAZILA SOCIAL SERVICES OFFICE,BISHWAMBARPUR,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,বিশম্ভরপুর,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, '90', '23', 'UPAZILA SOCIAL SERVICES OFFICE,CHHATAK,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,ছাতক,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, '90', '27', 'UPAZILA SOCIAL SERVICES OFFICE,DAKSHIN SUNAMGANJ,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,দক্ষিণ সুনামগঞ্জ,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, '90', '29', 'UPAZILA SOCIAL SERVICES OFFICE,DERAI,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,দিরাই,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, '90', '32', 'UPAZILA SOCIAL SERVICES OFFICE,DHARAMPASHA,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,ধর্মপাশা,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, '90', '47', 'UPAZILA SOCIAL SERVICES OFFICE,JAGANNATHPUR,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,জগন্নাথপুর,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, '90', '50', 'UPAZILA SOCIAL SERVICES OFFICE,JAMALGANJ,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,জামালগঞ্জ ,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, '90', '86', 'UPAZILA SOCIAL SERVICES OFFICE,SULLA,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,শাল্লা,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, '90', '89', 'UPAZILA SOCIAL SERVICES OFFICE,SUNAMGANJ SADAR,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,সুনামগঞ্জ সদর,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, '90', '92', 'UPAZILA SOCIAL SERVICES OFFICE,TAHIRPUR,SUNAMGANJ', 'উপজেলা সমাজসেবা কার্যালয়,তাহিরপুর,সুনামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, '91', '08', 'UPAZILA SOCIAL SERVICES OFFICE,BALAGANJ,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,বালাগঞ্জ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, '91', '17', 'UPAZILA SOCIAL SERVICES OFFICE,BEANI BAZAR,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,বিয়ানীবাজার,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, '91', '20', 'UPAZILA SOCIAL SERVICES OFFICE,BISHWANATH,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,বিশ্বনাথ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, '91', '27', 'UPAZILA SOCIAL SERVICES OFFICE,COMPANIGANJ,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,কোম্পানীগঞ্জ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, '91', '31', 'UPAZILA SOCIAL SERVICES OFFICE,DAKSHIN SURMA,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,দক্ষিণ সুরমা,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, '91', '35', 'UPAZILA SOCIAL SERVICES OFFICE,FENCHUGANJ,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,ফেঞ্চুগঞ্জ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, '91', '38', 'UPAZILA SOCIAL SERVICES OFFICE,GOLAPGANJ,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,গোলাপগঞ্জ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, '91', '41', 'UPAZILA SOCIAL SERVICES OFFICE,GOWAINGHAT,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,গোয়াইনঘাট,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, '91', '53', 'UPAZILA SOCIAL SERVICES OFFICE,JAINTIAPUR,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,জৈন্তাপুর,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, '91', '59', 'UPAZILA SOCIAL SERVICES OFFICE,KANAIGHAT,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,কানাইঘাট,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, '91', '59', 'UPAZILA SOCIAL SERVICES OFFICE,KANAIGHAT,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,কানাইঘাট,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, '91', '62', 'UPAZILA SOCIAL SERVICES OFFICE,SYLHET SADAR,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,সিলেট সদর,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, '91', '94', 'UPAZILA SOCIAL SERVICES OFFICE,ZAKIGANJ,SYLHET', 'উপজেলা সমাজসেবা কার্যালয়,জকিগঞ্জ,সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, '26', '04', 'UPAZILA SOCIAL SERVICES OFFICE,TEJGAON CIRCLE,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,তেজগাঁও সার্কেল,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, '26', '14', 'UPAZILA SOCIAL SERVICES OFFICE,DHAMRAI,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,ধামরাই,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, '26', '18', 'UPAZILA SOCIAL SERVICES OFFICE,DOHAR,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,দোহার,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, '26', '38', 'UPAZILA SOCIAL SERVICES OFFICE,KERANIGANJ,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,কেরানীগঞ্জ,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, '26', '62', 'UPAZILA SOCIAL SERVICES OFFICE,NAWABGANJ,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,নবাবগঞ্জ,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, '26', '72', 'UPAZILA SOCIAL SERVICES OFFICE,SAVAR,DHAKA', 'উপজেলা সমাজসেবা কার্যালয়,সাভার,ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, '29', '03', 'UPAZILA SOCIAL SERVICES OFFICE,ALFADANGA,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,আলফাডাঙ্গা,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, '29', '10', 'UPAZILA SOCIAL SERVICES OFFICE,BHANGA,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,ভাঙ্গা,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, '29', '18', 'UPAZILA SOCIAL SERVICES OFFICE,BOALMARI,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,বোয়ালমারী,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, '29', '21', 'UPAZILA SOCIAL SERVICES OFFICE,CHAR BHADRASAN,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,চরভদ্রাসন,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, '29', '47', 'UPAZILA SOCIAL SERVICES OFFICE,FARIDPUR SADAR,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,ফরিদপুর সদর,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, '29', '56', 'UPAZILA SOCIAL SERVICES OFFICE,MADHUKHALI,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,মধুখালী,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, '29', '62', 'UPAZILA SOCIAL SERVICES OFFICE,NAGARKANDA,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,নগরকান্দা,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, '29', '84', 'UPAZILA SOCIAL SERVICES OFFICE,SADARPUR,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,সদরপুর,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, '29', '90', 'UPAZILA SOCIAL SERVICES OFFICE,SALTHA,FARIDPUR', 'উপজেলা সমাজসেবা কার্যালয়,সালথা,ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, '33', '30', 'UPAZILA SOCIAL SERVICES OFFICE,GAZIPUR SADAR,GAZIPUR', 'উপজেলা সমাজসেবা কার্যালয়,গাজীপুর সদর,গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, '33', '32', 'UPAZILA SOCIAL SERVICES OFFICE,KALIAKAIR,GAZIPUR', 'উপজেলা সমাজসেবা কার্যালয়,কালিয়াকৈর,গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, '33', '34', 'UPAZILA SOCIAL SERVICES OFFICE,KALIGANJ,GAZIPUR', 'উপজেলা সমাজসেবা কার্যালয়,কালীগঞ্জ,গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, '33', '36', 'UPAZILA SOCIAL SERVICES OFFICE,KAPASIA,GAZIPUR', 'উপজেলা সমাজসেবা কার্যালয়,কাপাসিয়া,গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, '33', '86', 'UPAZILA SOCIAL SERVICES OFFICE,SREEPUR,GAZIPUR', 'উপজেলা সমাজসেবা কার্যালয়,শ্রীপুর,গাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, '35', '32', 'UPAZILA SOCIAL SERVICES OFFICE,GOPALGANJ SADAR,GOPALGANJ', 'উপজেলা সমাজসেবা কার্যালয়,গোপালগঞ্জ সদর,গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, '35', '43', 'UPAZILA SOCIAL SERVICES OFFICE,KASHIANI,GOPALGANJ', 'উপজেলা সমাজসেবা কার্যালয়,কাশিয়ানী,গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, '35', '51', 'UPAZILA SOCIAL SERVICES OFFICE,KOTALIPARA,GOPALGANJ', 'উপজেলা সমাজসেবা কার্যালয়,কোটালীপাড়া,গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, '35', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MUKSUDPUR,GOPALGANJ', 'উপজেলা সমাজসেবা কার্যালয়,মুকসুদপুর,গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, '35', '91', 'UPAZILA SOCIAL SERVICES OFFICE,TUNGIPARA,GOPALGANJ', 'উপজেলা সমাজসেবা কার্যালয়,টুঙ্গীপাড়া,গোপালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, '39', '07', 'UPAZILA SOCIAL SERVICES OFFICE,BAKSHIGANJ,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,বক্সীগঞ্জ,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, '39', '15', 'UPAZILA SOCIAL SERVICES OFFICE,DEWANGANJ,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,দেওয়ানগঞ্জ,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, '39', '29', 'UPAZILA SOCIAL SERVICES OFFICE,ISLAMPUR,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,ইসলামপুর,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, '39', '36', 'UPAZILA SOCIAL SERVICES OFFICE,JAMALPUR SADAR,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,জামালপুর সদর,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, '39', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MADARGANJ,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,মাদারগঞ্জ,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, '39', '61', 'UPAZILA SOCIAL SERVICES OFFICE,MELANDAHA,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,মেলান্দহ,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(483, '39', '85', 'UPAZILA SOCIAL SERVICES OFFICE,SARISHABARI UPAZILA,JAMALPUR', 'উপজেলা সমাজসেবা কার্যালয়,সরিষাবাড়ী,জামালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(484, '48', '02', 'UPAZILA SOCIAL SERVICES OFFICE,AUSTAGRAM,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,অষ্টগ্রাম,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(485, '48', '06', 'UPAZILA SOCIAL SERVICES OFFICE,BAJITPUR,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,বাজিতপুর,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, '48', '11', 'UPAZILA SOCIAL SERVICES OFFICE,BHAIRAB,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,ভৈরব,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, '48', '27', 'UPAZILA SOCIAL SERVICES OFFICE,HOSSAINPUR,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,হোসেনপুর,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, '48', '33', 'UPAZILA SOCIAL SERVICES OFFICE,ITNA,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,ইটনা,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, '48', '42', 'UPAZILA SOCIAL SERVICES OFFICE,KARIMGANJ,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,করিমগঞ্জ,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, '48', '42', 'UPAZILA SOCIAL SERVICES OFFICE,KARIMGANJ,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,করিমগঞ্জ,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, '48', '45', 'UPAZILA SOCIAL SERVICES OFFICE,KATIADI,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,কটিয়াদী,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, '48', '49', 'UPAZILA SOCIAL SERVICES OFFICE,KISHOREGANJ SADAR,KISHOREGONJ', 'উপজেলা সমাজসেবা কার্যালয়,কিশোরগঞ্জ সদর,কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(493, '85', '58', 'UPAZILA SOCIAL SERVICES OFFICE,MITHA PUKUR,RANGPUR', 'উপজেলা সমাজসেবা কার্যালয়,মিঠাপুকুর,রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `slung` varchar(400) DEFAULT NULL,
  `name` varchar(400) DEFAULT NULL,
  `name_en` varchar(400) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `description` varchar(3000) DEFAULT NULL,
  `description_en` varchar(3000) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slung`, `name`, `name_en`, `email`, `phone`, `address`, `description`, `description_en`, `created_at`, `updated_at`) VALUES
(1, 'appointments', 'SM Farhad Hossain', 'farhad1556@gmail.com', NULL, '', NULL, NULL, NULL, '2018-11-28 18:58:47', '2019-09-03 08:30:22'),
(2, 'contact', 'Get in touch with us', 'আমাদের সাথে যোগাযোগ করুন', 'contact@gmail.com', '+8801729963312', 'Lielirbes iela 29, Zemgales priekšpilsēta, Rīga, LV-1046', 'Google এর বিনামূল্যের পরিষেবা অবিলম্বে শব্দ, বাক্যাংশ এবং ওয়েব পৃষ্ঠাগুলি ইংরেজি ও ১০০টিরও বেশী ভাষার মধ্যে অনুবাদ করে।', 'Mel no postulant concludaturque, ex nec regione senserit. Scripta ponderum deserunt nam ut, nam at porro facer. Eos dolorem percipit tacimates ei, ubique noster ad vis, no sed enim zril.\r\nWe are also active in social media. You can find uson below adresses.', '2018-11-29 19:48:19', '2018-11-29 14:34:22'),
(3, 'about_us', 'আমাদের সম্পর্কে', 'About US', 'farhad1556@gmail.com', '017296633', 'tst', '<strong>৬৬ লক্ষের বেশি মানুষ ভাতা পাচ্ছেন সমাজসেবা অধিদফতর থেকে</strong>\r\nএ বছর সরকারের #সামাজিকনিরাপত্তা কার্যক্রমের আওতায় শুধুমাত্র সমাজসেবা অধিদফতর থেকেই ভাতা পাবেন ৬৬ লক্ষ ৩৮ হাজার ৮৫০ মানুষ। জনপ্রতি মাসিক ৫০০ টাকা #বয়স্কভাতা পাবেন ৪০ লক্ষ জন ও #বিধবাস্বামীনিগৃহীতাভাতা পাবেন ১৪ লক্ষ জন। জনপ্রতি মাসিক ৭০০ টাকা হারে #প্রতিবন্ধীভাতা পাবেন ১০ লক্ষ জন, ৯০ হাজার #প্রতিবন্ধীশিশু জনপ্রতি মাসিক ৭০০-১২০০ টাকা হারে পাবেন #শিক্ষাউপবৃত্তি। মাসিক ১০০০ টাকা হারে বেসরকারি #এতিমখানা</br>\r\n\r\n<p>&nbsp;</p>\r\n\r\nতে প্রতিপালিত ৮৬ হাজার ৪০০ এতিম শিশু পাবে #ক্যাপিট্যাশনগ্রান্ট। ৪০ হাজার #বেদেও #অনগ্রসর জনগোষ্ঠীর প্রবীন ব্যক্তিদের ৫০০ টাকা হারে দেয়া হবে বিশেষ ভাতা। ৫০০ টাকা হারে ভাতা পাবেন ২৫০০ #হিজড়া। ১৯ হাজার #বেদে ও অনগ্রসর জনগোষ্ঠী ও ১৩৫০ জন হিজড়া শিশু পাবেন বিশেষ উপবৃত্তি।', 'About us descripotion at en', '2018-12-16 01:04:21', '2019-01-06 11:45:03'),
(4, 'adds_settings', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 11:48:08', '2019-01-06 10:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('farhad1556@gmail.com', '$2y$10$39nyJImCZS4y7LaGjf3P5eHvJOAsAosPL1oZWzToZJhMFYPXXGgRy', '2018-12-13 10:52:10'),
('farhad1556@gmail.com', '$2y$10$39nyJImCZS4y7LaGjf3P5eHvJOAsAosPL1oZWzToZJhMFYPXXGgRy', '2018-12-13 10:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `name_en` varchar(200) DEFAULT NULL,
  `slung` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `photographer` varchar(250) DEFAULT NULL,
  `image_gallery` text,
  `gallery_photographer` varchar(250) DEFAULT NULL,
  `video` varchar(300) DEFAULT NULL,
  `description` longtext,
  `description_en` longtext,
  `featured` tinyint(4) DEFAULT NULL,
  `language` tinyint(1) DEFAULT NULL COMMENT '1=english,2=bangla',
  `category` int(11) DEFAULT NULL,
  `tag` smallint(6) DEFAULT NULL,
  `popular` int(11) DEFAULT NULL,
  `cool_video` tinyint(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `name_en`, `slung`, `image`, `photographer`, `image_gallery`, `gallery_photographer`, `video`, `description`, `description_en`, `featured`, `language`, `category`, `tag`, `popular`, `cool_video`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(2, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that was first', '6fd51653-4465-4abf-babe-af4b04e2908e', 'cfac2913ff910f7e545c9d4e03f97635.jpg', 'Farhad', '[\"02ffc1eba27e265ffb38a8e8c3c7d4e8_0.jpg\",\"02ffc1eba27e265ffb38a8e8c3c7d4e8_1.jpg\",\"c5bd869c341cb0c869dc469d3938f510_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/07d2dXHYb94\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত</p>', '<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>', 1, NULL, 14, 2, 16, 1, 1, 3, '2018-12-25 12:56:30', '2019-09-07 15:06:19'),
(3, 'জাতীয় সমাজসেবা একাডেমি আয়োজিত ২০-২৯ নভেম্বর ২০১৮', 'Neque porro quisquam est qui dolorem ipsum quia', '0910993d-ffbd-4437-b3ec-620b6eebaafd', '447a70c8ea30a279175d59d25cbebe67.jpg', 'Abul Fazal', '[\"3d175a9c045dd451152b7c0a055bd942_0.jpg\",\"3d175a9c045dd451152b7c0a055bd942_1.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/v5iCs_KTQbs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>&nbsp;</p>\r\n\r\n<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত</p>\r\n\r\n<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>', 1, NULL, 6, 2, 3, 1, 2, 2, '2018-12-25 16:12:31', '2019-01-03 05:06:55'),
(4, 'বয়স্ক ভাতা, মুক্তিযোদ্ধ ভাতা, প্রতিবন্ধী ভাতা ও উপবৃত্তি সেল', 'Neque porro quisquam est qui dolorem ipsum', 'd015c1d9-d21e-474f-8bb5-0f5d3276f346', '07d424e311053945e883dd3702dbb324.jpg', 'Nishad', '[\"b137cb0e99d2584f3b0205bc9a7a6b3f_0.jpg\",\"b137cb0e99d2584f3b0205bc9a7a6b3f_1.jpg\",\"b137cb0e99d2584f3b0205bc9a7a6b3f_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/phQDinMbmic\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিতবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত\r\n</p>\r\n\r\n<p>\r\nবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিতবিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত  বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত বিস্তারিত\r\n</p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 5, 1, 13, 1, 3, 3, '2018-12-25 16:19:54', '2019-09-07 13:27:15'),
(5, '৬৬ লক্ষের বেশি মানুষ ভাতা পাচ্ছেন সমাজসেবা অধিদফতর থেকে', 'Neque porro quisquam est qui dolorem ipsum', 'a6170f51-2a17-4b11-9b7a-c79c94612339', 'b7d487c7e4766082b224f9507f74a576.jpg', 'zehad', '[\"25406c9c79979b28f2a5ea019714be41_0.jpg\",\"70ec9da29bbc0aa0d07b9d30d718e3fb_1.jpg\",\"70ec9da29bbc0aa0d07b9d30d718e3fb_2.jpg\"]', NULL, NULL, '<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত<br />\r\nবাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '<p>Neque porro quisquam &nbsp;Neque porro quisquam&nbsp;Neque porro quisquam&nbsp;Neque porro quisquam&nbsp;Neque porro quisquam&nbsp;</p>', 1, NULL, 7, 3, 113, 1, 2, 3, '2018-12-25 16:22:53', '2019-09-07 10:51:24'),
(6, 'দগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়ন', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet', '9711fe75-66ae-48db-9775-9d58891eb3eb', 'c70db64382385f5740e21e821657df40.jpg', 'Imran', '[\"c70db64382385f5740e21e821657df40_0.jpg\",\"c70db64382385f5740e21e821657df40_1.jpg\",\"c70db64382385f5740e21e821657df40_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/D0a0aNqTehM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>দগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়নদগ্ধ ও প্রতিবন্ধী পুনর্বাসন ও আশ্রয়ন<br></p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 7, 2, NULL, 1, 3, 1, '2018-12-25 16:27:58', NULL),
(7, 'চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি', 'Neque porro quisquam est quia', '278a64f9-2b10-42a2-b985-694c4b9416a8', 'c0125358361ade14c043b5bf361a8f99.jpg', 'Jony', '[\"c0125358361ade14c043b5bf361a8f99_0.jpg\",\"c0125358361ade14c043b5bf361a8f99_1.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/07d2dXHYb94\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি \r\nচা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি  চা শ্রমিকদের জীবনমান উন্নয়ন কর্মসূচি </p>', '<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>\r\n<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>\r\n<p>Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia Neque porro quisquam est quia </p>', 1, NULL, 12, 2, 1, 1, 3, 1, '2018-12-25 16:33:12', '2019-01-22 08:56:06'),
(8, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that', '94fa486c-7fad-41a6-8e40-e70c5455d0cc', '175c3586016c9ad4b8a1ebd65abbda8b.jpg', 'A. Kader', '[\"52f3c1c1c948f872c54c254f306d42b7_0.jpg\",\"52f3c1c1c948f872c54c254f306d42b7_1.jpg\",\"6bfa3e77d3db1b03dd848fa9a712fa23_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/v5iCs_KTQbs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p>\r\n</p><p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p>\r\n</p><p>যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন </p><p></p>', 'English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that.English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that.English is a West Germanic language that English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that  English is a West Germanic language that', 1, NULL, 8, 1, NULL, 1, 3, 1, '2018-12-25 16:37:14', '2018-12-25 10:37:28'),
(9, 'যেভাবে গেলো মনোনয়নপত্র দাখিলের শেষ দিন', 'English is a West Germanic language that', '2f441cc6-b28c-49ab-9273-cb73d90ecb11', '7e910f1b62657c23e773cc9c3006d687.jpg', 'Mohon', '[\"29d1847f74bb650f61a1511cc1bf2d12_0.jpg\",\"29d1847f74bb650f61a1511cc1bf2d12_1.jpg\",\"29d1847f74bb650f61a1511cc1bf2d12_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/phQDinMbmic\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত&nbsp;</p><p></p><p>বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত বাংলায় বিস্তারিত <br></p><br><p></p>', '<div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet&nbsp;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet</div><div><br></div><div>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet Neque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit ametNeque porro quisquam est qui dolorem ipsum quia dolor sit amet<br></div>', 1, NULL, 9, 3, NULL, 0, 3, 1, '2018-12-25 16:44:08', NULL),
(10, 'প্রাকবৃত্তিমূলক প্রশিক্ষণ কেন্দ্র', 'You may use the where method on a query builder', 'd6cc56a0-dcfc-4403-986f-aedd815421ec', '77f2052a939ebdb48adabb1b2599614f.jpg', 'Parvej', '[\"77f2052a939ebdb48adabb1b2599614f_0.jpg\",\"e723001be86f60990c36d63a873b44ff_1.jpg\",\"e723001be86f60990c36d63a873b44ff_2.jpg\"]', NULL, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/sdbHXKlpPAM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত&nbsp;</p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><p>বিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিতবিস্তারিত <br></p><br><p></p>', '<p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl&nbsp;</p><p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl <br></p><p>You may use the <code>where</code> method on a query builder instance to add <code>where</code> cl <br></p><br><p></p>', 1, NULL, 10, 3, NULL, 0, 3, 1, '2018-12-25 16:46:23', NULL),
(11, 'test', 'test', 'test', '74646b71bccad7fa99ec392ceba1d9b6.jpg', 'res', '[\"142e35c091c5f5d81d43bfa29713d442_0.jpg\"]', NULL, 'rse', '<p>swerw rwer wrewer</p>', '<p>&nbsp;wrew wre wre werw e</p>', 1, NULL, 5, 2, 1, 1, 2, 3, '2019-09-06 15:05:52', '2019-09-07 10:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `name_alt` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `order`, `name`, `name_alt`, `created_at`, `updated_at`) VALUES
(1, 3, 'Draft', 'Draft', '2018-12-24 08:53:21', NULL),
(2, 2, 'Save', 'Waiting to get published', '2018-12-24 08:41:56', NULL),
(3, 1, 'Published', 'Published', '2018-12-24 08:53:28', NULL),
(4, 4, 'Disapproved', 'Rejected', '2018-12-24 08:42:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `slung` varchar(100) NOT NULL,
  `slung_en` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `slung`, `slung_en`, `name`, `name_en`, `created_at`, `updated_at`) VALUES
(1, '5cf8a8db-c571-4aa7-a4d4-1fae1c504683', 'tax-ret', 'আয়কর পরি', 'Tax Ret', '2018-11-28 16:21:55', '2018-12-19 09:15:03'),
(2, '23f1feeb-c041-4c7a-9769-7bf3ddb6c634', 'paid', 'পরিশোধ', 'Paid', '2018-11-28 16:21:55', '2018-12-25 17:07:13'),
(3, '16eaef94-9e7e-48eb-aa00-acd126fd300b', 'scholarship-application', 'শিক্ষাবৃত্তির আবেদন', 'Scholarship Application', '2018-11-28 16:21:55', '2018-12-25 17:18:51'),
(4, '852d4b41-886a-4ba6-93bd-8385269ffbba', 'socialservice-academy', 'সমাজসেবা একাডেমি', 'Socialservice Academy', '2018-11-28 16:21:55', '2018-12-25 17:19:09'),
(5, 'b0b18787-7b7b-44fe-b82b-4be024b7b6df', 'social-degradation', 'সামাজিক অবক্ষয়', 'Social Degradation', '2018-11-28 16:21:55', '2018-12-25 17:19:38'),
(6, '6e44c2bd-4f19-4378-ab0b-67a70320c488', 'degradation', 'অবক্ষয়', 'Degradation', '2018-11-28 16:21:55', '2018-12-25 17:19:13'),
(7, 'f202d585-b728-4963-9d9c-676e8a522bc6', 'professional-skill', 'পেশাগত দক্ষতা', 'Professional Skill', '2018-11-28 16:21:55', '2018-12-25 17:19:23'),
(8, 'ec504d40-722a-4c49-8306-bc28a1a2c278', 'exam', 'পরীক্ষা', 'exam', '2018-12-19 18:11:39', '2018-12-25 17:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `upozila`
--

CREATE TABLE `upozila` (
  `id` int(11) NOT NULL,
  `dist_id` varchar(10) NOT NULL,
  `div_id` varchar(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_bd` varchar(255) NOT NULL,
  `mcu_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upozila`
--

INSERT INTO `upozila` (`id`, `dist_id`, `div_id`, `name_en`, `name_bd`, `mcu_type`, `created_at`, `updated_at`) VALUES
(1, '10', '2', 'AMTALI', 'আমতলী', 'Upazila', '0000-00-00 00:00:00', '2019-09-04 15:35:33'),
(2, '1', '2', 'বামনা', 'BAMNA', 'Upazila', '0000-00-00 00:00:00', '2019-09-04 14:57:04'),
(3, '10', '3', 'বরগুনা পৌরসভা', 'BARGUNA PAURASAVA', '', '0000-00-00 00:00:00', '2019-09-04 14:58:12'),
(4, '4', '1', 'BARGUNA SADAR', 'বরগুনা সদর', 'Upazil', '0000-00-00 00:00:00', '2019-09-04 15:34:22'),
(5, '04', '1', 'BETAGI', 'বেতাগী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '04', '1', 'PATHARGHATA', 'পাথরঘাটা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '04', '606', 'TALTALI', 'তালতলী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '06', '607', 'AGAILJHARA', 'আগৈলঝড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '06', '608', 'BABUGANJ', 'বাবুগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '06', '609', 'BAKERGANJ', 'বাকেরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '06', '610', 'BANARI PARA', 'বানারী পাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '06', '611', 'GAURNADI', 'গৌরনদী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '06', '612', 'HIZLA', 'হিজলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '06', '613', 'BARISAL CITY CORPORATIO', 'বরিশাল সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '06', '614', 'BARISAL SADAR (KOTWALI)', 'বরিশাল সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '06', '615', 'MHENDIGANJ', 'মেহেন্দীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '06', '616', 'MULADI', 'মূলাদী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '06', '617', 'WAZIRPUR', 'উজিরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '09', '618', 'BHOLA PAURASAVA', 'ভোলা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '09', '619', 'BHOLA SADAR', 'ভোলা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '09', '620', 'BURHANUDDI', 'বোরহানউদ্দিন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '09', '621', 'CHAR FASSO', 'চরফ্যাশন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '09', '622', 'DAULAT KHA', 'দৌলতখান', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '09', '623', 'LALMOHA', 'লালমোহন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '09', '624', 'MANPURA', 'মনপুরা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '09', '625', 'TAZUMUDDI', 'তজুমদ্দিন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '42', '626', 'JHALOKATI PAURASAVA', 'ঝালকাঠি পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '42', '627', 'JHALOKATI SADAR', 'ঝালকাঠি সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '42', '628', 'KANTHALIA', 'কাঠালিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '42', '629', 'NALCHITY', 'নলছিটি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '42', '630', 'RAJAPUR', 'রাজাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '78', '631', 'BAUPHAL', 'বাউফল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '78', '632', 'DASHMINA', 'দশমিনা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '78', '633', 'DUMKI', 'দুমকি ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '78', '634', 'GALACHIPA', 'গলাচিপা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '78', '635', 'KALAPARA PAURASAVA', 'কলাপাড়া পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '78', '636', 'KALAPARA', 'কলাপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '78', '637', 'MIRZAGANJ', 'মির্জাগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '78', '638', 'PATUAKHALI SADAR', 'পটুয়াখালী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '78', '639', 'PATUAKHALI PAURASHAVA', 'পটুয়াখালী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '78', '640', 'RANGABALI', 'রাঙ্গাবালি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '79', '641', 'BHANDARIA', 'ভান্ডারিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '79', '642', 'KAWKHALI', 'কাউখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '79', '643', 'MATHBARIA', 'মঠবাড়ীয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '79', '644', 'NAZIRPUR', 'নাজিরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '79', '645', 'PIROJPUR PAURASAVA', 'পিরোজপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '79', '646', 'PIROJPUR SADAR', 'পিরোজপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '79', '647', 'NESARABAD (SWARUPKATI)', 'নেছারাবাদ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '79', '648', 'ZIANAGAR', 'জিয়ানগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '03', '649', 'ALIKADAM', 'আলীকদম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '03', '650', 'BANDARBAN SADAR', 'বান্দরবান সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '03', '651', 'BANDARBAN PAURASHAVA', 'বান্দরবান পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '03', '652', 'LAMA PAURASHAVA', 'লামা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '03', '653', 'LAMA', 'লামা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '03', '654', 'NAIKHONGCHHARI', 'নাইক্ষ্যংছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '03', '655', 'ROWANGCHHARI', 'রোয়াংছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '03', '656', 'RUMA', 'রুমা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '03', '657', 'THANCHI', 'থানচি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '12', '658', 'AKHAURA', 'আখাউড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '12', '659', 'BANCHHARAMPUR', 'বাঞ্ছারামপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '12', '660', 'BIJOYNAGAR', 'বিজয়নগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '12', '661', 'BRAHMANBARIA SADAR', 'ব্রাহ্মণবাড়িয়া সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '12', '662', 'BRAHMANBARIA PAURASAVA', 'ব্রাহ্মণবাড়িয়া পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '12', '663', 'ASHUGANJ', 'আশুগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, '12', '664', 'KASBA', 'কসবা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, '12', '665', 'NABINAGAR', 'নবীনগর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, '12', '666', 'NASIRNAGAR', 'নাসিরনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, '12', '667', 'SARAIL', 'সরাইল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, '13', '668', 'CHANDPUR PAURASAVA', 'চাঁদপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, '13', '669', 'CHANDPUR SADAR', 'চাঁদপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, '13', '670', 'FARIDGANJ', 'ফরিদগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, '13', '671', 'HAIM CHAR', 'হাইমচর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, '13', '672', 'HAJIGANJ', 'হাজীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, '13', '673', 'KACHUA', 'কচুয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, '13', '674', 'MATLAB DAKSHI', 'মতলব দক্ষিণ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, '13', '675', 'MATLAB UTTAR', 'মতলব উত্তর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, '13', '676', 'SHAHRASTI', 'শাহারাস্তি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, '15', '677', 'ANOWARA', 'আনোয়ারা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, '15', '678', 'BANSHKHALI', 'বাশঁখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, '15', '679', 'BOALKHALI', 'বোয়ালখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, '15', '680', 'CHANDANAISH', 'চন্দনাইশ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, '15', '681', 'FATIKCHARI', 'ফটিকছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, '15', '682', 'HATHAZARI', 'হাটহাজারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, '15', '683', 'Patiya', 'পটিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, '15', '684', 'LOHAGARA', 'লোহাগড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, '15', '685', 'MIRSHARAI', 'মিরসরাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, '15', '686', 'RANGUNIA', 'রাংগুনিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, '15', '687', 'RAOZA', 'রাউজান ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, '15', '688', 'SANDWIP', 'সন্দ্বীপ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, '15', '689', 'SATKANIA', 'সাতকানিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, '15', '690', 'SITAKUNDA', 'সীতাকুন্ড', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, '15', '691', 'CHITTAGONG CITY CORPORATIO', 'চট্টগ্রাম সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, '19', '694', 'COMILLA CITY CORPORATIO', 'কুমিল্লা সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, '19', '695', 'BARURA', 'বরুড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, '19', '696', 'BRAHMAN PARA', 'ব্রাহ্মণপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, '19', '697', 'BURICHANG', 'বুড়ীচং', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, '19', '698', 'CHANDINA', 'চান্দিনা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, '19', '699', 'CHAUDDAGRAM', 'চৌদ্দগ্রাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, '19', '700', 'COMILLA SADAR DAKSHI', 'সদর দক্ষিণ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, '19', '701', 'DAUDKANDI', 'দাউদকান্দি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, '19', '702', 'DEBIDWAR', 'দেবীদ্বার ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, '19', '703', 'HOMNA', 'হোমনা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, '19', '704', 'COMILLA ADARSHA SADAR', ' আদর্শ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, '19', '705', 'LAKSAM', 'লাকসাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, '19', '706', 'MANOHARGANJ', 'মনোহরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, '19', '707', 'MEGHNA', 'মেঘনা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, '19', '708', 'MURADNAGAR', 'মুরাদনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, '19', '709', 'NANGALKOT', 'লাংগলকোট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, '19', '710', 'TITAS', 'তিতাস', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, '22', '711', 'CHAKARIA', 'চকরিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, '22', '712', 'COX\'S BAZAR SADAR', 'কক্সবাজার সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, '22', '713', 'COX\'S BAZAR PAURASHAVA', 'কক্সবাজার পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, '22', '714', 'KUTUBDIA', 'কুতুবদিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, '22', '715', 'MAHESHKHALI', 'মহেশখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, '22', '716', 'PEKUA', 'পেকুয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, '22', '717', 'RAMU', 'রামু', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, '22', '718', 'TEKNAF', 'টেকনাফ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, '22', '719', 'UKHIA', 'উখিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, '30', '720', 'CHHAGALNAIYA', 'ছাগলনাইয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, '30', '721', 'DAGANBHUIYA', 'দাগনভূঞা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, '30', '722', 'FENI SADAR', 'ফেনী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, '30', '723', 'FENI PAURASAVA', 'ফেনী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, '30', '724', 'FULGAZI', 'ফুলগাজী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, '30', '725', 'PARSHURAM', 'পরশুরাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, '30', '726', 'SONAGAZI', 'সোনাগাজী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, '46', '727', 'DIGHINALA', 'দীঘিনালা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, '46', '728', 'KHAGRACHHARI SADAR', 'খাগড়াছড়ি সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, '46', '729', 'KHAGRACHHARI PAURASAVA', 'খাগড়াছড়ি পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, '46', '730', 'LAKSHMICHHARI', 'লক্ষীছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, '46', '731', 'MAHALCHHARI', 'মহালছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, '46', '732', 'MANIKCHHARI', 'মানিকছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, '46', '733', 'MATIRANGA', 'মাটিরাংগা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, '46', '734', 'PANCHHARI', 'পানছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, '46', '735', 'RAMGARH', 'রামগড়', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, '46', '736', 'RAMGARH PAURASHAVA', 'রামগড় পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, '51', '737', 'KAMALNAGAR', 'কমলনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, '51', '738', 'LAKSHMIPUR SADAR', 'লক্ষ্মীপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, '51', '739', 'LAKSHMIPUR PAURASAVA', 'লক্ষ্মীপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, '51', '740', 'ROYPUR', 'রায়পুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, '51', '741', 'RAMGANJ', 'রামগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, '51', '742', 'RAMGATI', 'রামগতি ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, '75', '743', 'BEGUMGANJ', 'বেগমগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, '75', '744', 'CHATKHIL', 'চাটখিল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, '75', '745', 'COMPANIGANJ', 'কোম্পানীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, '75', '746', 'HATIYA', 'হাতিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, '75', '747', 'KABIRHAT', 'কবিরহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, '75', '748', 'SENBAGH', 'সেনবাগ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, '75', '749', 'SONAIMURI', 'সোনাইমুড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, '75', '750', 'SUBARNACHAR', 'সুবর্ণচর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, '75', '751', 'NOAKHALI SADAR', 'নোয়াখালী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, '75', '752', 'NOAKHALI PAURASAVA', 'নোয়াখালী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, '84', '753', 'BAGHAICHHARI', 'বাঘাইছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, '84', '754', 'BARKAL ', 'বরকল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, '84', '755', 'KAWKHALI (BETBUNIA)', 'কাউখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, '84', '756', 'BELAI CHHARI  ', 'বিলাইছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, '84', '757', 'KAPTAI ', 'কাপ্তাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, '84', '758', 'JURAI CHHARI', 'জুড়াছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, '84', '759', 'LANGADU', 'লংগদু', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, '84', '760', 'NANIARCHAR ', 'নানিয়ারচর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, '84', '761', 'RAJASTHALI', 'রাজস্থলী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, '84', '762', 'RANGAMATI SADAR', 'রাঙ্গামাটি সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, '84', '763', 'RANGAMATI PAURASHAVA', 'রাঙ্গামাটি পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, '01', '764', 'BAGERHAT SADAR', 'বাগেরহাট সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, '01', '765', 'BAGERHAT PAURASAVA', 'বাগেরহাট পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, '01', '766', 'CHITALMARI', 'চিতলমারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, '01', '767', 'FAKIRHAT', 'ফকিরহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, '01', '768', 'KACHUA', 'কচুয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, '01', '769', 'MOLLAHAT', 'মোল্লাহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, '01', '770', 'MONGLA', 'মংলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, '01', '771', 'MORRELGANJ', 'মোড়েলগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, '01', '772', 'RAMPAL', 'রামপাল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, '01', '773', 'SARANKHOLA', 'শরণখোলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, '18', '774', 'ALAMDANGA', 'আলমডাংগা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, '18', '775', 'CHUADANGA SADAR', 'চুয়াডাংগা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, '18', '776', 'CHUADANGA PAURASAVA', 'চুয়াডাংগা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, '18', '777', 'DAMURHUDA', 'দামুড়হুদা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, '18', '778', 'JIBAN NAGAR', 'জীবননগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, '41', '779', 'ABHAYNAGAR', 'অভয়নগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, '41', '780', 'BAGHER PARA', 'বাঘার পাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, '41', '781', 'CHAUGACHHA', 'চৌগাছা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, '41', '782', 'JHIKARGACHHA', 'ঝিকরগাছা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, '41', '783', 'KESHABPUR', 'কেশবপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, '41', '784', 'JESSORE SADAR', 'যশোর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, '41', '785', 'JESSORE PAURASAVA', 'যশোর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, '41', '786', 'MANIRAMPUR', 'মনিরামপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, '41', '787', 'SHARSHA', 'শার্শা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, '44', '788', 'HARINAKUNDA', 'হরিণাকুন্ডু', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, '44', '789', 'JHENAIDAH SADAR', 'ঝিনাইদহ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, '44', '790', 'JHENAIDAH PAURASAVA', 'ঝিনাইদহ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, '44', '791', 'KALIGANJ', 'কালীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, '44', '792', 'KOTCHANDPUR', 'কোটচাঁদপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, '44', '793', 'MOHESHPUR', 'মহেশপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, '44', '794', 'SHAILKUPA', 'শৈলকুপা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, '47', '795', 'BATIAGHATA', 'বটিয়াঘাটা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, '47', '796', 'DACOPE', 'দাকোপ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, '47', '797', 'DIGHALIA', 'দিঘলিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, '47', '798', 'DUMURIA', 'ডুমুরিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, '47', '799', 'KOYRA', 'কয়রা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, '47', '800', 'PAIKGACHHA', 'পাইকগাছা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, '47', '801', 'PHULTALA', 'ফুলতলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, '47', '802', 'RUPSA', 'রূপসা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, '47', '803', 'TEROKHADA', 'তেরখাদা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, '47', '804', 'KHULNA CITY CORPORATIO', 'খুলনা সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, '50', '807', 'BHERAMARA', 'ভেড়ামারা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, '50', '808', 'DAULATPUR', 'দৌলতপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, '50', '809', 'KHOKSA', 'খোকসা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, '50', '810', 'KUMARKHALI', 'কুমারখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, '50', '811', 'KUSHTIA SADAR', 'কুষ্টিয়া সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, '50', '812', 'KUSHTIA PAURASHAVA', 'কুষ্টিয়া পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, '50', '813', 'MIRPUR', 'মিরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, '55', '814', 'MAGURA SADAR', 'মাগুরা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, '55', '815', 'MAGURA PAURASAVA', 'মাগুরা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, '55', '816', 'MOHAMMADPUR', 'মোহাম্মদপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, '55', '817', 'SHALIKHA', 'শালিখা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, '55', '818', 'SREEPUR', 'শ্রীপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, '57', '819', 'GANGNI', 'গাংনী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, '57', '820', 'MUJIB NAGAR', 'মুজিবনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, '57', '821', 'MEHERPUR SADAR', 'মেহেরপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, '57', '822', 'MEHERPUR PAURASHAVA', 'মেহেরপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, '65', '823', 'KALIA', 'কালিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, '65', '824', 'LOHAGARA', 'লোহাগড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, '65', '825', 'NARAIL SADAR', 'নড়াইল সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, '65', '826', 'NARAIL PAURASAVA', 'নড়াইল পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, '87', '827', 'ASSASUNI', 'আশাশুনি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, '87', '828', 'DEBHATA', 'দেবহাটা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, '87', '829', 'KALAROA', 'কলারোয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, '87', '830', 'KALIGANJ', 'কালীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, '87', '831', 'SATKHIRA SADAR', 'সাতক্ষীরা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, '87', '832', 'SATKHIRA PAURASAVA', 'সাতক্ষীরা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, '87', '833', 'SHYAMNAGAR', 'শ্যামনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, '87', '834', 'TALA', 'তালা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, '10', '1', 'BOGRA PAURASAVA', 'বগুড়া পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, '10', '1', 'BOGRA SADAR', 'বগুড়া সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, '10', '1', 'ADAMDIGHI', 'আদমদিঘী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, '10', '1', 'DHUNAT', 'ধুনট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, '10', '1', 'DHUPCHANCHIA', 'দুপচাঁচিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, '10', '1', 'GABTALI', 'গাবতলী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, '10', '2', 'KAHALOO', 'কাহালু', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, '10', '1263', 'NANDIGRAM', 'নন্দিগ্রাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, '10', '1264', 'SARIAKANDI', 'সারিয়াকান্দি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, '10', '1265', 'SHAJAHANPUR', 'শাহজাহানপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, '10', '1266', 'SHERPUR', 'শেরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, '10', '1267', 'SHIBGANJ', 'শিবগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, '10', '1268', 'SONATOLA', 'সোনাতলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, '38', '1269', 'JOYPURHAT PAURASAVA', 'জয়পুরহাট পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, '38', '1270', 'JOYPURHAT SADAR', 'জয়পুরহাট সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, '38', '1271', 'AKKELPUR', 'আক্কেলপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, '38', '1272', 'KALAI', 'কালাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, '38', '1273', 'KHETLAL', 'ক্ষেতলাল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, '38', '1274', 'PANCHBIBI', 'পাঁচবিবি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, '64', '1275', 'NAOGAON PAURASAVA', 'নওগাঁ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, '64', '1276', 'NAOGAON SADAR', 'নওগাঁ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, '64', '1277', 'ATRAI', 'আত্রাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, '64', '1278', 'BADALGACHHI', 'বদলগাছী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, '64', '1279', 'DHAMOIRHAT', 'ধামুইরহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, '64', '1280', 'MANDA', 'মান্দা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, '64', '1281', 'MAHADEBPUR', 'মহাদেবপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, '64', '1282', 'NIAMATPUR', 'নিয়ামতপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, '64', '1283', 'PATNITALA', 'পত্নীতলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, '64', '1284', 'PORSHA', 'পোরশা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, '64', '1285', 'RANINAGAR', 'রানীনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, '64', '1286', 'SAPAHAR', 'সাপাহার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, '69', '1287', 'NATORE PAURASAVA', 'নাটোর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, '69', '1288', 'NATORE SADAR', 'নাটোর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, '69', '1289', 'BAGATIPARA', 'বাগাতিপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, '69', '1290', 'BARAIGRAM', 'বড়াইগ্রাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, '69', '1291', 'GURUDASPUR', 'গুরুদাসপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, '69', '1292', 'LALPUR', 'লালপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, '69', '1293', 'SINGRA', 'সিংড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, '70', '1294', 'CHAPAINAWABGANJ PAURASHAVA', 'চাঁপাইনবাবগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, '70', '1295', 'CHAPAINAWABGANJ SADAR', 'চাঁপাইনবাবগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, '70', '1296', 'BHOLAHAT', 'ভোলাহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, '70', '1297', 'GOMASTAPUR', 'গোমস্তাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, '70', '1298', 'NACHOLE', 'নাচোল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, '70', '1299', 'SHIBGANJ', 'শিবগঞ্জ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, '76', '1300', 'PABNA PAURASAVA', 'পাবনা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, '76', '1301', 'PABNA SADAR', 'পাবনা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, '76', '1302', 'ATGHARIA', 'আটঘরিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, '76', '1303', 'BERA', 'বেড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, '76', '1304', 'BHANGURA', 'ভাঙ্গুরা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, '76', '1305', 'CHATMOHAR', 'চাটমোহর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, '76', '1306', 'FARIDPUR', 'ফরিদপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, '76', '1307', 'ISHWARDI', 'ঈশ্বরদী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, '76', '1308', 'SANTHIA', 'সাথিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, '76', '1309', 'SUJANAGAR', 'সুজানগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, '88', '1310', 'SIRAJGANJ PAURASAVA', 'সিরাজগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, '88', '1311', 'SIRAJGANJ SADAR', 'সিরাজগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, '88', '1312', 'BELKUCHI', 'বেলকুচি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, '88', '1313', 'CHAUHALI', 'চৌহালি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, '88', '1314', 'KAMARKHANDA', 'কামারখন্দ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, '88', '1315', 'KAZIPUR', 'কাজীপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, '88', '1316', 'ROYGANJ', 'রায়গঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, '88', '1317', 'SHAHJADPUR', 'শাহজাদপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, '88', '1318', 'TARASH', 'তাড়াশ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, '88', '1319', 'ULLAH PARA', 'উল্লাপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, '81', '1320', 'RAJSHAHI CITY CORPORATIO', 'রাজশাহী সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, '81', '1321', 'BAGHA', 'বাঘা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, '81', '1322', 'BAGHMARA', 'বাগমারা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, '81', '1323', 'CHARGHAT', 'চারঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, '81', '1324', 'DURGAPUR', 'দুর্গাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, '81', '1325', 'GODAGARI', 'গোদাগাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, '81', '1326', 'MOHANPUR', 'মোহনপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, '81', '1327', 'PABA', 'পবা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, '81', '1328', 'PUTHIA', 'পুঠিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, '81', '1329', 'TANORE', 'তানোর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, '36', '1532', 'HABIGANJ PAURASHAVA', 'হবিগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, '36', '1533', 'AJMIRIGANJ', 'আজমেরীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, '36', '1534', 'BAHUBAL', 'বাহুবল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, '36', '1535', 'BANIACHONG', 'বানিয়াচং', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, '36', '1536', 'CHUNARUGHAT', 'চুনারুঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, '36', '1537', 'HABIGANJ SADAR', 'হবিগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, '36', '1538', 'LAKHAI', 'লাখাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, '36', '1539', 'MADHABPUR', 'মাধবপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, '36', '1540', 'NABIGANJ', 'নবীগঞ্জ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, '58', '1541', 'MAULVIBAZAR PAURASHAVA', 'মৌলভীবাজার পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, '58', '1542', 'BARLEKHA', 'বড়লেখা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, '58', '1543', 'JURI', 'জুড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, '58', '1544', 'KAMALGANJ', 'কমলগঞ্জ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, '58', '1545', 'KULAURA', 'কুলাউড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, '58', '1546', 'MAULVIBAZAR SADAR', 'মৌলভীবাজার সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, '58', '1547', 'RAJNAGAR', 'রাজনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, '58', '1548', 'SREEMANGAL', 'শ্রীমঙ্গল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, '90', '1549', 'SUNAMGANJ PAURASHAVA', 'সুনামগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, '90', '1550', 'BISHWAMBARPUR', 'বিশম্ভরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, '90', '1551', 'CHHATAK', 'ছাতক', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, '90', '1552', 'DAKSHIN SUNAMGANJ', 'দক্ষিণ সুনামগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, '90', '1553', 'DERAI', 'দিরাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, '90', '1554', 'DHARAMPASHA', 'ধর্মপাশা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, '90', '1555', 'DOWARABAZAR', 'দোয়ারাবাজার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, '90', '1556', 'JAGANNATHPUR', 'জগন্নাথপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, '90', '1557', 'JAMALGANJ', 'জামালগঞ্জ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, '90', '1558', 'SULLA', 'শাল্লা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, '90', '1559', 'SUNAMGANJ SADAR', 'সুনামগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, '90', '1560', 'TAHIRPUR', 'তাহিরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, '91', '1561', 'SYLHET CITY CORPORATIO', 'সিলেট সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, '91', '1562', 'BALAGANJ', 'বালাগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, '91', '1563', 'BEANI BAZAR', 'বিয়ানীবাজার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, '91', '1564', 'BISHWANATH', 'বিশ্বনাথ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, '91', '1565', 'COMPANIGANJ', 'কোম্পানীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, '91', '1566', 'DAKSHIN SURMA', 'দক্ষিণ সুরমা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, '91', '1567', 'FENCHUGANJ', 'ফেঞ্চুগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, '91', '1568', 'GOLAPGANJ', 'গোলাপগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, '91', '1569', 'GOWAINGHAT', 'গোয়াইনঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, '91', '1570', 'JAINTIAPUR', 'জৈন্তাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, '91', '1571', 'KANAIGHAT', 'কানাইঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, '91', '1572', 'SYLHET SADAR', 'সিলেট সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, '91', '1573', 'ZAKIGANJ', 'জকিগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, '26', '1574', 'DHAKA UTTAR CITY CORPORATIO', 'ঢাকা উত্তর সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, '26', '1575', 'DHAKA DAKSHIN CITY CORPORATIO', 'ঢাকা দক্ষিণ সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, '26', '1576', 'Tejgaon Circle', 'তেজগাঁও সার্কেল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, '26', '1577', 'DHAMRAI', 'ধামরাই', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, '26', '1578', 'DOHAR', 'দোহার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, '26', '1579', 'KERANIGANJ', 'কেরানীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, '26', '1580', 'NAWABGANJ', 'নবাবগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, '26', '1581', 'SAVAR', 'সাভার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, '29', '1582', 'FARIDPUR PAURASHAVA', 'ফরিদপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, '29', '1583', 'ALFADANGA', 'আলফাডাঙ্গা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, '29', '1584', 'BHANGA', 'ভাঙ্গা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, '29', '1585', 'BOALMARI', 'বোয়ালমারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, '29', '1586', 'CHAR BHADRASA', 'চরভদ্রাসন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, '29', '1587', 'FARIDPUR SADAR', 'ফরিদপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, '29', '1588', 'MADHUKHALI', 'মধুখালী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, '29', '1589', 'NAGARKANDA', 'নগরকান্দা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, '29', '1590', 'SADARPUR', 'সদরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, '29', '1591', 'SALTHA', 'সালথা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, '33', '1592', 'GAZIPUR CITY CORPORATIO', 'গাজীপুর সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, '33', '1593', 'GAZIPUR SADAR', 'গাজীপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, '33', '1594', 'KALIAKAIR', 'কালিয়াকৈর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, '33', '1595', 'KALIGANJ', 'কালীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, '33', '1596', 'KAPASIA', 'কাপাসিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, '33', '1597', 'SREEPUR', 'শ্রীপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, '35', '1598', 'GOPALGANJ PAURASHAVA', 'গোপালগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, '35', '1599', 'GOPALGANJ SADAR', 'গোপালগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, '35', '1600', 'KASHIANI', 'কাশিয়ানী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, '35', '1601', 'KOTALIPARA', 'কোটালীপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, '35', '1602', 'MUKSUDPUR', 'মুকসুদপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, '35', '1603', 'TUNGIPARA', 'টুঙ্গীপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, '39', '1604', 'JAMALPUR PAURASHAVA', 'জামালপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, '39', '1605', 'BAKSHIGANJ', 'বক্সীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, '39', '1606', 'DEWANGANJ', 'দেওয়ানগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, '39', '1607', 'ISLAMPUR', 'ইসলামপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, '39', '1608', 'JAMALPUR SADAR', 'জামালপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, '39', '1609', 'MADARGANJ', 'মাদারগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, '39', '1610', 'MELANDAHA', 'মেলান্দহ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, '39', '1611', 'SARISHABARI', 'সরিষাবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, '48', '1612', 'KISHOREGANJ PAURASHAVA', 'কিশোরগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, '48', '1613', 'AUSTAGRAM', 'অষ্টগ্রাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, '48', '1614', 'BAJITPUR', 'বাজিতপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, '48', '1615', 'BHAIRAB', 'ভৈরব', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, '48', '1616', 'HOSSAINPUR', 'হোসেনপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, '48', '1617', 'ITNA', 'ইটনা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, '48', '1618', 'KARIMGANJ', 'করিমগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, '48', '1619', 'KATIADI', 'কটিয়াদি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, '48', '1620', 'KISHOREGANJ SADAR', 'কিশোরগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, '48', '1621', 'KULIAR CHAR', 'কুলিয়ারচর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, '48', '1622', 'MITHAMAI', 'মিঠামঈন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, '48', '1623', 'NIKLI', 'নিকলী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, '48', '1624', 'PAKUNDIA', 'পাকুন্দিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, '48', '1625', 'TARAIL', 'তাড়াইল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, '54', '1626', 'MADARIPUR PAURASHAVA', 'মাদারীপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, '54', '1627', 'KALKINI', 'কালকিনি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, '54', '1628', 'MADARIPUR SADAR', 'মাদারীপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, '54', '1629', 'RAJOIR', 'রাজৈর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, '54', '1630', 'SHIBCHAR', 'শিবচর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, '56', '1631', 'MANIKGANJ PAURASHAVA', 'মানিকগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, '56', '1632', 'DAULATPUR', 'দৌলতপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, '56', '1633', 'GHIOR', 'ঘিওর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, '56', '1634', 'HARIRAMPUR', 'হরিরামপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, '56', '1635', 'MANIKGANJ SADAR', 'মানিকগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, '56', '1636', 'SATURIA', 'সাটুরিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, '56', '1637', 'SHIBALAYA', 'শিবালয়', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, '56', '1638', 'SINGAIR', 'সিংগাইর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, '59', '1639', 'MUNSHIGANJ PAURASHAVA', 'মুন্সিগঞ্জ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, '59', '1640', 'GAZARIA', 'গজারিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, '59', '1641', 'LOHAJANG', 'লৌহজং', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, '59', '1642', 'MUNSHIGANJ SADAR', 'মুন্সিগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, '59', '1643', 'SERAJDIKHA', 'সিরাজদিখান', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, '59', '1644', 'SREENAGAR', 'শ্রীনগর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, '59', '1645', 'TONGIBARI', 'টংগীবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, '61', '1646', 'MYMENSINGH PAURASHAVA', 'ময়মনসিংহ পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, '61', '1647', 'BHALUKA', 'ভালুকা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, '61', '1648', 'DHOBAURA', 'ধোবাউড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, '61', '1649', 'FULBARIA', 'ফুলবাড়ীয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, '61', '1650', 'GAFFARGAO', 'গফরগাঁও', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, '61', '1651', 'GAURIPUR', 'গৌরীপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, '61', '1652', 'HALUAGHAT', 'হালুয়াঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, '61', '1653', 'ISHWARGANJ', 'ঈশ্বরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, '61', '1654', 'MYMENSINGH SADAR', 'ময়মনসিংহ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, '61', '1655', 'MUKTAGACHHA', 'মুক্তাগাছা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, '61', '1656', 'NANDAIL', 'নান্দাইল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, '61', '1657', 'PHULPUR', 'ফুলপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, '61', '1658', 'TRISHAL', 'ত্রিশাল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, '67', '1659', 'NARAYANGANJ CITY CORPORATIO', 'নারায়ণগঞ্জ সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, '67', '1660', 'ARAIHAZAR', 'আড়াইহাজার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, '67', '1661', 'SONARGAO', 'সোনারগাঁও', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, '67', '1662', 'BANDAR', 'বন্দর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, '67', '1663', 'NARAYANGANJ SADAR', 'নারায়ণগঞ্জ সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, '67', '1664', 'RUPGANJ', 'রূপগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, '68', '1665', 'NARSINGDI PAURASHAVA', 'নরসিংদী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, '68', '1666', 'BELABO', 'বেলাবো', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, '68', '1667', 'MANOHARDI', 'মনোহরদী ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, '68', '1668', 'NARSINGDI SADAR', 'নরসিংদী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, '68', '1669', 'PALASH', 'পলাশ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, '68', '1670', 'ROYPURA', 'রায়পুরা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, '68', '1671', 'SHIBPUR', 'শিবপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, '72', '1672', 'NETROKONA PAURASAVA', 'নেত্রকোনা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, '72', '1673', 'ATPARA', 'আটপাড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, '72', '1674', 'BARHATTA', 'বারহাট্টা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, '72', '1675', 'DURGAPUR', 'দূর্গাপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, '72', '1676', 'KHALIAJURI', 'খালিয়াজুরী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, '72', '1677', 'KALMAKANDA', 'কলমাকান্দা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, '72', '1678', 'KENDUA', 'কেন্দুয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, '72', '1679', 'MADA', 'মদন', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, '72', '1680', 'MOHANGANJ', 'মোহনগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, '72', '1681', 'NETROKONA SADAR', 'নেত্রকোনা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, '72', '1682', 'PURBADHALA', 'পূর্বধলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, '82', '1683', 'RAJBARI PAURASHAVA', 'রাজবাড়ী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, '82', '1684', 'BALIAKANDI', 'বালিয়াকান্দি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, '82', '1685', 'GOALANDA', 'গোয়ালন্দ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, '82', '1686', 'KALUKHALI', 'কালুখালি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, '82', '1687', 'PANGSHA', 'পাংশা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, '82', '1688', 'RAJBARI SADAR', 'রাজবাড়ী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, '86', '1689', 'SHARIATPUR PAURASHAVA', 'শরীয়তপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, '86', '1690', 'BHEDARGANJ', 'ভেদরগঞ্জ ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, '86', '1691', 'DAMUDYA', 'ডামুড্যা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, '86', '1692', 'GOSAIRHAT', 'গোসাইরহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, '86', '1693', 'NARIA', 'নড়িয়া ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, '86', '1694', 'SHARIATPUR SADAR', 'শরীয়তপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, '86', '1695', 'ZANJIRA', 'জাজিরা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, '89', '1696', 'SHERPUR PAURASHAVA', 'শেরপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, '89', '1697', 'JHENAIGATI', 'ঝিনাইগাতি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, '89', '1698', 'NAKLA', 'নকলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, '89', '1699', 'NALITABARI', 'নলিতাবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, '89', '1700', 'SHERPUR SADAR', 'শেরপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, '89', '1701', 'SREEBARDI', 'শ্রীবর্দী ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, '93', '1702', 'TANGAIL PAURASHAVA', 'টাঙ্গাইল পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, '93', '1703', 'BASAIL', 'বাসাইল ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, '93', '1704', 'BHUAPUR', 'ভুঞাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, '93', '1705', 'DELDUAR', 'দেলদুয়ার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, '93', '1706', 'DHANBARI', 'ধনবাড়ী ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, '93', '1707', 'GHATAIL', 'ঘাটাইল ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, '93', '1708', 'GOPALPUR', 'গোপালপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(483, '93', '1709', 'KALIHATI', 'কালীহাতি ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `upozila` (`id`, `dist_id`, `div_id`, `name_en`, `name_bd`, `mcu_type`, `created_at`, `updated_at`) VALUES
(484, '93', '1710', 'MADHUPUR', 'মধুপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(485, '93', '1711', 'MIRZAPUR', 'মির্জাপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, '93', '1712', 'NAGARPUR', 'নাগরপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, '93', '1713', 'SAKHIPUR', 'সখিপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, '93', '1714', 'TANGAIL SADAR', 'টাঙ্গাইল সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, '27', '1715', 'DINAJPUR PAURASAVA', 'দিনাজপুর পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, '27', '1716', 'BIRAMPUR', 'বিরামপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, '27', '1717', 'BIRGANJ', 'বীরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, '27', '1718', 'BIRAL', 'বিরল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(493, '27', '1719', 'BOCHAGANJ', 'বোচাগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(494, '27', '1720', 'CHIRIRBANDAR', 'চিরিরবন্দর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(495, '27', '1721', 'FULBARI', 'ফুলবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(496, '27', '1722', 'GHORAGHAT', 'ঘোড়াঘাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(497, '27', '1723', 'HAKIMPUR', 'হাকিমপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(498, '27', '1724', 'KAHAROLE', 'কাহারোল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(499, '27', '1725', 'KHANSAMA', 'খানসামা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(500, '27', '1726', 'DINAJPUR SADAR', 'দিনাজপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(501, '27', '1727', 'NAWABGANJ', 'নবাবগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(502, '27', '1728', 'PARBATIPUR', 'পার্বতীপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(503, '32', '1729', 'GAIBANDHA PAURASAVA', 'গাইবান্ধা পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(504, '32', '1730', 'FULCHHARI', 'ফুলছড়ি', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(505, '32', '1731', 'GAIBANDHA SADAR', 'গাইবান্ধা সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(506, '32', '1732', 'GOBINDAGANJ', 'গোবিন্দগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(507, '32', '1733', 'PALASHBARI', 'পলাশবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(508, '32', '1734', 'SADULLAPUR', 'সাদুল্যাপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(509, '32', '1735', 'SAGHATA', 'সাঘাটা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(510, '32', '1736', 'SUNDARGANJ', 'সুন্দরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(511, '49', '1737', 'KURIGRAM PAURASAVA', 'কুড়িগ্রাম পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(512, '49', '1738', 'BHURUNGAMARI', 'ভূরুঙ্গামারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(513, '49', '1739', 'CHAR RAJIBPUR', 'রাজিবপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(514, '49', '1740', 'CHILMARI', 'চিলমারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(515, '49', '1741', 'PHULBARI', 'ফুলবাড়ী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(516, '49', '1742', 'KURIGRAM SADAR', 'কুড়িগ্রাম সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(517, '49', '1743', 'NAGESHWARI', 'নাগেশ্বরী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(518, '49', '1744', 'RAJARHAT', 'রাজারহাট', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(519, '49', '1745', 'RAUMARI', 'রৌমারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(520, '49', '1746', 'ULIPUR', 'উলিপুর ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(521, '52', '1747', 'LALMONIRHAT PAURASAVA', 'লালমনিরহাট পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(522, '52', '1748', 'ADITMARI', 'আদিতমারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(523, '52', '1749', 'HATIBANDHA', 'হাতীবান্ধা ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(524, '52', '1750', 'KALIGANJ', 'কালীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(525, '52', '1751', 'LALMONIRHAT SADAR', 'লালমনিরহাট সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(526, '52', '1752', 'PATGRAM', 'পাটগ্রাম', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(527, '73', '1753', 'NILPHAMARI PAURASAVA', 'নীলফামারী পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(528, '73', '1754', 'DIMLA ', 'ডিমলা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(529, '73', '1755', 'DOMAR ', 'ডোমার', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(530, '73', '1756', 'JALDHAKA ', 'জলঢাকা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(531, '73', '1757', 'KISHOREGANJ', 'কিশোরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(532, '73', '1758', 'NILPHAMARI SADAR ', 'নীলফামারী সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(533, '73', '1759', 'SAIDPUR', 'সৈয়দপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(534, '77', '1760', 'PANCHAGARH PAURASAVA', 'পঞ্চগড় পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(535, '77', '1761', 'ATWARI', 'আটোয়ারী', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(536, '77', '1762', 'BODA', 'বোদা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(537, '77', '1763', 'DEBIGANJ', 'দেবীগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(538, '77', '1764', 'PANCHAGARH SADAR', 'পঞ্চগড় সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(539, '77', '1765', 'TENTULIA', 'তেঁতুলিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(540, '85', '1766', 'RANGPUR CITY CORPORATIO', 'রংপুর সিটি কর্পোরেশন', 'CityCorp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(541, '85', '1767', 'BADARGANJ', 'বদরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(542, '85', '1768', 'GANGACHARA', 'গঙ্গাচড়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(543, '85', '1769', 'KAUNIA', 'কাউনিয়া', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(544, '85', '1770', 'RANGPUR SADAR', 'রংপুর সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(545, '85', '1771', 'PIRGACHHA', 'পীরগাছা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(546, '85', '1772', 'TARAGANJ', 'তারাগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(547, '85', '1773', 'MITHA PUKUR', 'মিঠাপুকুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(548, '85', '1774', 'PIRGANJ', 'পীরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(549, '94', '1775', 'THAKURGAON PAURASAVA', 'ঠাকুরগাঁও পৌরসভা', 'Municipality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(550, '94', '1776', 'BALIADANGI', 'বালিয়াডাংগী ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(551, '94', '1777', 'HARIPUR', 'হরিপুর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(552, '94', '1778', 'PIRGANJ', 'পীরগঞ্জ', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(553, '94', '1779', 'RANISANKAIL', 'রানীশংকৈল', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(554, '94', '1780', 'THAKURGAON SADAR', 'ঠাকুরগাঁও সদর', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(555, '69', '2', 'Naldanga', 'নলডাঙ্গা', 'Upazila', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(556, '12', '1', 'বরগুনা', 'BARGUNA', 'FFFF', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `group_id` tinyint(4) DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` tinyint(4) NOT NULL,
  `division` tinyint(4) NOT NULL,
  `district` tinyint(4) NOT NULL,
  `thana` tinyint(4) NOT NULL,
  `profile_picture` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `status`, `group_id`, `name`, `office`, `email`, `mobile`, `job_title`, `division`, `district`, `thana`, `profile_picture`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'SM Farhad Hossain', 9, 'farhad1556@gmail.com', '01729963312', 5, 1, 8, 10, 'f97a717485d86c23fa79ec2ff09cf042.jpg', '2018-12-23 18:00:00', '$2y$10$Nl6DNoz/cDSRJWQ0m1yiG.Pv7w5IbE/fkdeaseLGtx1.SU.3enMoy', '', 'o088eJoulQ8siKUcTDHkZtvBqwOGDuCzTpeosobmeZmtSQrhkZyTIv65Z9T0', '2018-12-12 12:45:35', '2019-09-04 03:55:12'),
(2, 1, 3, 'kalam', 9, 'kalam@gmail.com', '0', 5, 0, 0, 0, '', '2018-12-23 18:00:00', '$2y$10$pFKAkiH5RgpRcBWovaW.ZemXUiEPCk9taURV0ZPRhICbbII5x5S2K', '', 'e09EIwpX0UP0t7305M4MiICZEr3gmNEYxlihYKoiOfNj9HNILQfoHRCBf5N1', '2018-12-12 12:45:35', '2018-12-19 06:40:19'),
(3, 1, NULL, 'hossain', 9, 'hossain@gmail.com', '0', 5, 0, 0, 0, '', NULL, '$2y$10$V/6sN.IRophcBI.g36TkUentAnfIwE8m/L6ncBdbTX6lPEiy/B4n6', '1p4ns9dEIA6aETcZkkJP7LwkRreWZ5I1S6squaYaFdzB911m4S95z8M4Il7o', NULL, '2019-01-13 09:00:10', '2019-01-13 09:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_activations`
--

CREATE TABLE `user_activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_activities`
--

CREATE TABLE `user_activities` (
  `id` int(11) NOT NULL,
  `name` varchar(11) DEFAULT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-12-09 18:40:53', '2018-12-09 12:41:58'),
(2, 'Publisher', '2018-11-28 16:21:55', '2018-12-21 10:18:53'),
(3, 'Editor', '2018-12-21 16:19:08', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flags`
--
ALTER TABLE `flags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_access_permision`
--
ALTER TABLE `module_access_permision`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upozila`
--
ALTER TABLE `upozila`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_activations`
--
ALTER TABLE `user_activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_activations_id_user_foreign` (`id_user`);

--
-- Indexes for table `user_activities`
--
ALTER TABLE `user_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `flags`
--
ALTER TABLE `flags`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `module_access_permision`
--
ALTER TABLE `module_access_permision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=494;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `upozila`
--
ALTER TABLE `upozila`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=557;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_activations`
--
ALTER TABLE `user_activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_activities`
--
ALTER TABLE `user_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
