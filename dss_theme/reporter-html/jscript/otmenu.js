// Responsive menu logo (defaut = '<img src="/images/logo.png" alt="OrangeThemes" />')
var _otmLogo = (typeof _otmLogo === "undefined") ? '<img src="images/logo.png" alt="OrangeThemes" />' :_otmLogo;
// Responsive menu search link (defaut = '/s')
var _otmSearch = (typeof _otmSearch === "undefined") ? '/s' :_otmSearch;

(function () {
    "use strict";

	Array.prototype.forEach2=function(a){ var l=this.length; for(var i=0;i<l;i++)a(this[i],i) };

	function ot_class(matchClass) {
		var elems = document.getElementsByTagName('*'),i,ret = [];
		for (i in elems) {
			if((" "+elems[i].className+" ").indexOf(" "+matchClass+" ") > -1) {
				ret.push(elems[i]);
			}
		}
		return ret;
	}

	if(typeof document.getElementById("boxed") != "undefined") {

		document.getElementById("boxed").insertAdjacentHTML('beforeend', '<div class="ot-responsive-menu-header"><a href="#" id="ot-menu-burger-1" class="ot-responsive-menu-header-burger"></a><a href="/" class="ot-responsive-menu-header-logo">'+_otmLogo+'</a></div>');
		
		document.body.insertAdjacentHTML('beforeend', '<div class="ot-responsive-menu-content-c-header"><a href="#" id="ot-menu-burger-2" class="ot-responsive-menu-header-burger"></a></div><div class="ot-responsive-menu-content"><div class="ot-responsive-menu-content-inner has-search"><form action="'+_otmSearch+'" method="get"><input type="text" name="s" value="" placeholder="Search" /><button type="submit"></button></form><ul id="responsive-menu-holder"></ul></div></div><div class="ot-responsive-menu-background"></div>');

		ot_class("load-responsive").forEach2(function(key){
			var e = document.getElementById("responsive-menu-holder").innerHTML;
			document.getElementById("responsive-menu-holder").innerHTML = e+key.innerHTML;
		});

		document.getElementById("ot-menu-burger-1").addEventListener('click', function(e){
			e.preventDefault();

			var mex_top = "-"+parseInt(window.pageYOffset, 10)+"px";
			if(!document.body.classList.contains("nomorefixd")){
				document.body.classList.add("ot-responsive-menu-show");
				document.body.classList.add("nomorefixd");
				document.getElementById("boxed").style.marginTop = mex_top;
			}
			window.scrollTo( 0, mex_top );
			return false;
		});

		document.getElementById("ot-menu-burger-2").addEventListener('click', function(e){
			e.preventDefault();
			document.getElementById("boxed").click();
			return false;
		});

		document.getElementById("boxed").addEventListener('click', function(e){
			if(!e.toElement.classList.contains("ot-responsive-menu-header-burger") && document.body.classList.contains("nomorefixd") && document.body.classList.contains("ot-responsive-menu-show")){
				document.body.classList.remove("ot-responsive-menu-show");
				setTimeout(function(){
					var mex_top = Math.abs(parseInt(document.getElementById("boxed").style.marginTop, 10));
					document.getElementById("boxed").removeAttribute("style");
					document.body.classList.remove("nomorefixd");
					window.scrollTo( 0, mex_top );
				}, 200);
			}
		});
	}


	// Main menu follow on scroll
	var menuisfollowing = false,
		allmenus = [];

	ot_class("otm-follow").forEach2(function(key){
		allmenus.push(key);
	});

	window.onscroll = function(){
		var scrollBarPosition = window.pageYOffset | document.body.scrollTop;
		allmenus.forEach2(function(key){
			if(typeof key == "undefined" || key.offsetParent == null) return false;
			
			var pos = key.offsetTop + key.offsetParent.offsetTop;
			if(scrollBarPosition >= pos) {
				if(!key.classList.contains("is-now-following")){
					key.style.height = key.offsetHeight+"px";
					key.style.width = key.offsetWidth+"px";
					key.classList.add("is-now-following");
				}
			} else {
				if(key.classList.contains("is-now-following")){
					key.classList.remove("is-now-following");
					key.removeAttribute("style");
				}
			}
		});
	};

})();