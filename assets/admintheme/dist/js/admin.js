(function () {
    var headlineList = $('div#bangla_post_id_group');
    headlineList.hide();
    $('select#language').on('change', function (e) {
        e.preventDefault();
        var lang = $(this).val();
        if (lang == 1) {
            headlineList.show();
            headlineList.find('select').attr('required', true);
        } else {
            headlineList.hide();
            headlineList.find('select').attr('required', false);
        }
    });

    $('select#bangla_post_id').on('change', function (e) {
        e.preventDefault();
        var news_url, video, description;
        var news_url = $(this).find(':selected').data('newsurl');
        var video;
        //alert($(this).find(':selected').data('category'));
        $.ajax({
            type: "GET",
            url: news_url,
            data: "news_url",
            success: function (data) {
                $('#video').text(data.video);
                $('#image_p img').attr('src', '/storage/images/news_thumb/' + data.image);
                $('#imageUploadEn').attr('value', data.image);
                $('#descriptionp').text(data.description);
                //$('.textarea').setValue(data.description);
                //$('.textarea').html(data.description);
                var category_list = $('select#category option');
                var featured_list = $('select#featured option');
                var tags_list = $('select#tags option');
                category_list.each(function () {
                    if ($(this).val() == data.category) {
                        $(this).attr('selected', true);
                    } else {
                        $(this).attr('selected', false);
                    }
                });
                featured_list.each(function () {
                    if ($(this).val() == data.featured) {
                        $(this).attr('selected', true);
                    } else {
                        $(this).attr('selected', false);
                    }
                });
                tags_list.each(function () {
                    if ($(this).val() == data.tag) {
                        $(this).attr('selected', true);
                    } else {
                        $(this).attr('selected', false);
                    }
                });
            }
        });
    });

    $('button#draft').on('click', function () {
        var status = $(this).data('status');
        $('input#status').val(status);
    });
    $('button#save').on('click', function () {
        var status = $(this).data('status');
        $('input#status').val(status);
    });
    
    var multiFIleUpload = "<div  class='form-group'>" +
            "<label for='galleryImageUpload' class='col-sm-2 control-label'>গ্যালারি চিত্র আপলোড</label>" +
            "<div class='col-sm-5'>" +
            "<input type='file' name='gallery_image_upload[]' id='galleryImageUpload' class='galleryImageUpload'>" +
            " <p class='help-block'>Please upload an image here</p>" +
            "</div>"+
            "<div class='col-sm-5'>"+
            "<p id='gallery_p' class='gallery_p help-block'></p>"+
            "</div>";
     var i =0; 
    $('button.addMoreGallary').on('click', function () {
        i++;
        if(i <5 ){
        $('div#multiImagediv').append(multiFIleUpload);
        
        // test ga
        $(".galleryImageUpload").on('change', function () {
        if (typeof (FileReader) != "undefined") {
            var image_holder = $(this).closest( ".form-group" ).find(".gallery_p");
            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
                    "style": "width:200px;height:100px;"
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });  
    }
    });

    $('input#name').on('change', (function () {
        var form = $(this).closest("form");
        var formData = new FormData(form);
        //var formData = form.serialize() ;


        var url = form.attr('action');
        console.log(formData.get());
        return;
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log("success");
                console.log(data);
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    }));
 
    $("input[name='addblock']").on('change', function () {
        if ($(this).val() == 1) {
            $(this).val()
            $('#embedCode').show();
            $('#imageAdd').hide();
        } else {
            $('#imageAdd').show();
            $('#embedCode').hide();
        }
    });
    
//     
  CKEDITOR.replace('description');
  CKEDITOR.replace('description_en');
  
// on browse upload image 
$("#imageUpload").on('change', function () {

        if (typeof (FileReader) != "undefined") {

            var image_holder = $("#image_p");
            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
                    "style": "width:200px;height:100px;"
                }).appendTo(image_holder);

            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });
    
// on gallery image
$(".galleryImageUpload").on('change', function () {
        if (typeof (FileReader) != "undefined") {
            var image_holder = $(this).closest( ".form-group" ).find(".gallery_p");
            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
                    "style": "width:200px;height:100px;"
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });  
}());